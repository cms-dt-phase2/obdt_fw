
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;

entity gbt is
  generic(
    NUM_LINKS           : positive := 1
  );
  port (
    txFrameClk40        : in std_logic;  -- 40 MHz GBT Frame clock
    tdcMgtClk120        : in std_logic;
    drpClk              : in std_logic;

    QSFP_TX_P           : out std_logic_vector(1 to 2);
    QSFP_TX_N           : out std_logic_vector(1 to 2);
    QSFP_RX_P           : in  std_logic_vector(1 to 2);
    QSFP_RX_N           : in  std_logic_vector(1 to 2);

    generalReset        : in std_logic;
    manualResetTx       : in std_logic;
    manualResetRx       : in std_logic;

    txData              : in gbtframe_A(1 to 2);
    txIsDataSel         : in std_logic_vector(1 to 2);
    rxData              : out gbtframe_A(1 to 2);
    rxIsData            : out std_logic_vector(1 to 2)
  );
end entity;

architecture structural of gbt is

  signal testPatterSel              : std_logic_vector(1 downto 0);
  signal loopBack                   : std_logic_vector(2 downto 0) :=(others =>'0');
  signal resetDataErrorSeenFlag     : std_logic := '0';
  signal resetGbtRxReadyLostFlag    : std_logic := '0';

  signal mgtReady                   : std_logic_vector(1 to NUM_LINKS);
  signal rxBitSlipNbr               : std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0);
  signal rxWordClkReady             : std_logic_vector(1 to NUM_LINKS);
  signal rxFrameClkReady            : std_logic_vector(1 to NUM_LINKS);
  signal gbtRxReady                 : std_logic_vector(1 to NUM_LINKS);
  signal gbtRxReadyLostFlag         : std_logic_vector(1 to NUM_LINKS);
  signal rxDataErrorSeen            : std_logic_vector(1 to NUM_LINKS);
  signal rxExtrDataWidebusErSeen    : std_logic_vector(1 to NUM_LINKS);

  signal txExtraDataWidebus         : wbframe_extra_A(1 to NUM_LINKS);
  signal rxExtraDataWidebus         : wbframe_extra_A(1 to NUM_LINKS);

  signal txFrameClk                 : std_logic_vector(1 to NUM_LINKS);
  signal txWordClk                  : std_logic_vector(1 to NUM_LINKS);
  signal rxFrameClk                 : std_logic_vector(1 to NUM_LINKS);
  signal rxWordClk                  : std_logic_vector(1 to NUM_LINKS);

  signal txMatchFlag                : std_logic;
  signal rxMatchFlag                : std_logic_vector(1 to NUM_LINKS);

  signal errCnter                   : std_logic_vector(63 downto 0);
  signal wordCnter                  : std_logic_vector(63 downto 0);

  component msemi_pf_gbt
  generic (
    GBT_BANK_ID                     : integer := 0;
    NUM_LINKS                       : integer := 1;
    TX_OPTIMIZATION                 : integer range 0 to 1 := STANDARD;
    RX_OPTIMIZATION                 : integer range 0 to 1 := STANDARD;
    TX_ENCODING                     : integer range 0 to 1 := GBT_FRAME;
    RX_ENCODING                     : integer range 0 to 1 := GBT_FRAME;

    -- Extended configuration --
    DATA_GENERATOR_ENABLE           : integer range 0 to 1 := 1;
    DATA_CHECKER_ENABLE             : integer range 0 to 1 := 1;
    MATCH_FLAG_ENABLE               : integer range 0 to 1 := 1
    );
  port (
    --==============--
    -- Clocks       --
    --==============--
    FRAMECLK_40MHZ                                : in  std_logic;
    XCVRCLK                                       : in  std_logic;
    XCVRCLK_FAB                                   : in  std_logic;
    RX_FRAMECLK_O                                 : out std_logic_vector(1 to NUM_LINKS);
    RX_WORDCLK_O                                  : out std_logic_vector(1 to NUM_LINKS);
    TX_FRAMECLK_O                                 : out std_logic_vector(1 to NUM_LINKS);
    TX_WORDCLK_O                                  : out std_logic_vector(1 to NUM_LINKS);

    RX_WORDCLK_RDY_O                              : out std_logic_vector(1 to NUM_LINKS);
    RX_FRAMECLK_RDY_O                             : out std_logic_vector(1 to NUM_LINKS);

    --==============--
    -- Reset        --
    --==============--
    GBTBANK_GENERAL_RESET_I                       : in  std_logic;
    GBTBANK_MANUAL_RESET_TX_I                     : in  std_logic;
    GBTBANK_MANUAL_RESET_RX_I                     : in  std_logic;

    --==============--
    -- Serial lanes --
    --==============--
    GBTBANK_MGT_RX_P                              : in  std_logic_vector(1 to NUM_LINKS);
    GBTBANK_MGT_RX_N                              : in  std_logic_vector(1 to NUM_LINKS);
    GBTBANK_MGT_TX_P                              : out std_logic_vector(1 to NUM_LINKS);
    GBTBANK_MGT_TX_N                              : out std_logic_vector(1 to NUM_LINKS);

    --==============--
    -- Data       --
    --==============--
    GBTBANK_GBT_DATA_I                            : in  gbtframe_A(1 to NUM_LINKS);
    GBTBANK_WB_DATA_I                             : in  wbframe_A(1 to NUM_LINKS);
    TX_DATA_O                                     : out gbtframe_A(1 to NUM_LINKS);
    WB_DATA_O                                     : out wbframe_extra_A(1 to NUM_LINKS);
    GBTBANK_GBT_DATA_O                            : out gbtframe_A(1 to NUM_LINKS);
    GBTBANK_WB_DATA_O                             : out wbframe_extra_A(1 to NUM_LINKS);

    --==============--
    -- Reconf.     --
    --==============--
    GBTBANK_MGT_DRP_RST                           : in  std_logic;
    GBTBANK_MGT_DRP_CLK                           : in  std_logic;

    --==============--
    -- TX ctrl      --
    --==============--
    GBTBANK_TX_ISDATA_SEL_I                       : in  std_logic_vector(1 to NUM_LINKS);
    GBTBANK_TEST_PATTERN_SEL_I                    : in  std_logic_vector(1 downto 0);

    --==============--
    -- RX ctrl      --
    --==============--
    GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I          : in  std_logic_vector(1 to NUM_LINKS);
    GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I           : in  std_logic_vector(1 to NUM_LINKS);

    --==============--
    -- TX Status    --
    --==============--
    GBTBANK_GBTTX_READY_O                         : out std_logic_vector(1 to NUM_LINKS);
    GBTBANK_GBTRX_READY_O                         : out std_logic_vector(1 to NUM_LINKS);
    GBTBANK_LINK_READY_O                          : out std_logic_vector(1 to NUM_LINKS);
    GBTBANK_LINK1_BITSLIP_O                       : out std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0);
    GBTBANK_TX_MATCHFLAG_O                        : out std_logic;

    --==============--
    -- RX Status    --
    --==============--
    GBTBANK_GBTRXREADY_LOST_FLAG_O                : out std_logic_vector(1 to NUM_LINKS);
    GBTBANK_RXDATA_ERRORSEEN_FLAG_O               : out std_logic_vector(1 to NUM_LINKS);
    GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O  : out std_logic_vector(1 to NUM_LINKS);
    GBTBANK_RX_MATCHFLAG_O                        : out std_logic_vector(1 to NUM_LINKS);
    GBTBANK_RX_ISDATA_SEL_O                       : out std_logic_vector(1 to NUM_LINKS);
    RXDATA_ERROR_CNT                              : out std_logic_vector(63 downto 0);
    RXDATA_WORD_CNT                               : out std_logic_vector(63 downto 0);
    GBTBANK_RX_ERRORDETECTED_O                    : out std_logic_vector(1 to NUM_LINKS);

    --==============--
    -- XCVR ctrl    --
    --==============--
    GBTBANK_LOOPBACK_I                            : in  std_logic_vector(2 downto 0);
    GBTBANK_TX_POL                                : in  std_logic_vector(1 to NUM_LINKS);
    GBTBANK_RX_POL                                : in  std_logic_vector(1 to NUM_LINKS)
  );
  end component;

begin


  loopBack <= (others =>'0');
  testPatterSel <= "11";        --"11": data, "01": counter


  pf_gbt_inst: entity work.msemi_pf_gbt
  generic map(
    GBT_BANK_ID                                   => 1,
    NUM_LINKS                                     => GBT_BANKS_USER_SETUP(1).NUM_LINKS,
    TX_OPTIMIZATION                               => GBT_BANKS_USER_SETUP(1).TX_OPTIMIZATION,
    RX_OPTIMIZATION                               => GBT_BANKS_USER_SETUP(1).RX_OPTIMIZATION,
    TX_ENCODING                                   => GBT_BANKS_USER_SETUP(1).TX_ENCODING,
    RX_ENCODING                                   => GBT_BANKS_USER_SETUP(1).RX_ENCODING,
    DATA_GENERATOR_ENABLE                         => 0,
    DATA_CHECKER_ENABLE                           => 0,
    MATCH_FLAG_ENABLE                             => 0
  )
  port map (
    --==============--
    -- Clocks       --
    --==============--

    FRAMECLK_40MHZ                                => txFrameClk40,
    XCVRCLK                                       => tdcMgtClk120,
    XCVRCLK_FAB                                   => tdcMgtClk120,

    TX_FRAMECLK_O                                 => txFrameClk,
    TX_WORDCLK_O                                  => txWordClk,
    RX_FRAMECLK_O                                 => rxFrameClk,
    RX_WORDCLK_O                                  => rxWordClk,
    RX_WORDCLK_RDY_O                              => rxWordClkReady,
    RX_FRAMECLK_RDY_O                             => rxFrameClkReady,


    --==============--
    -- Reset        --
    --==============--
    GBTBANK_GENERAL_RESET_I                       => generalReset,
    GBTBANK_MANUAL_RESET_TX_I                     => manualResetTx,
    GBTBANK_MANUAL_RESET_RX_I                     => manualResetRx,

    --==============--
    -- Serial lanes --
    --==============--
    GBTBANK_MGT_RX_P                              => QSFP_RX_P(1 to NUM_LINKS),
    GBTBANK_MGT_RX_N                              => QSFP_RX_N(1 to NUM_LINKS),
    GBTBANK_MGT_TX_P                              => QSFP_TX_P(1 to NUM_LINKS),
    GBTBANK_MGT_TX_N                              => QSFP_TX_N(1 to NUM_LINKS),


    --==============--
    -- Data             --
    --==============--
    GBTBANK_GBT_DATA_I                            => txData(1 to NUM_LINKS),
    GBTBANK_WB_DATA_I                             => (others => (others =>'0')),

    TX_DATA_O                                     => open,
    WB_DATA_O                                     => txExtraDataWidebus,


    GBTBANK_GBT_DATA_O                            => rxData(1 to NUM_LINKS),
    GBTBANK_WB_DATA_O                             => rxExtraDataWidebus,

    --==============--
    -- Reconf.         --
    --==============--
    GBTBANK_MGT_DRP_RST                           => '0',
    GBTBANK_MGT_DRP_CLK                           => drpclk,

    --==============--
    -- TX ctrl        --
    --==============--
    GBTBANK_TX_ISDATA_SEL_I                       => txIsDataSel(1 to NUM_LINKS),
    GBTBANK_TEST_PATTERN_SEL_I                    => testPatterSel,

    --==============--
    -- RX ctrl      --
    --==============--
    GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I          => (others => resetGbtRxReadyLostFlag ),
    GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I           => (others => resetDataErrorSeenFlag ),

    --==============--
    -- TX Status    --
    --==============--

    GBTBANK_LINK_READY_O                          => mgtReady,
    GBTBANK_TX_MATCHFLAG_O                        => txMatchFlag,

    --==============--
    -- RX Status    --
    --==============--

    GBTBANK_GBTRX_READY_O                         => gbtRxReady,
    GBTBANK_LINK1_BITSLIP_O                       => rxBitSlipNbr,
    GBTBANK_GBTRXREADY_LOST_FLAG_O                => gbtRxReadyLostFlag,
    GBTBANK_RXDATA_ERRORSEEN_FLAG_O               => rxDataErrorSeen,
    GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O  => rxExtrDataWidebusErSeen,

    GBTBANK_RX_MATCHFLAG_O                        => rxMatchFlag,
    GBTBANK_RX_ISDATA_SEL_O                       => rxIsData(1 to NUM_LINKS),

    RXDATA_WORD_CNT                               => wordCnter,
    RXDATA_ERROR_CNT                              => errCnter,
    GBTBANK_RX_ERRORDETECTED_O                    => open,

    --==============--
    -- XCVR ctrl    --
    --==============--
    GBTBANK_LOOPBACK_I                            => loopBack,
    GBTBANK_TX_POL                                => (others => '0'),
    GBTBANK_RX_POL                                => (others => '0')
  );

end structural;
