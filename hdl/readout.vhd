
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.obdt_pkg.all;

entity readout is
  Port (
    clk           : in STD_LOGIC;
    rst           : in STD_LOGIC;
    
    data_in       : in tdc_enc_a;
    bx_cnt        : in STD_LOGIC_VECTOR(11 downto 0);
    
    rdclk_0       : in std_logic;
    data_out_0    : out STD_LOGIC_VECTOR(24 downto 0);
    strobe_out_0  : out STD_LOGIC;
    empty_out_0   : out STD_LOGIC;

    rdclk_1       : in std_logic;
    data_out_1    : out STD_LOGIC_VECTOR(24 downto 0);
    strobe_out_1  : out STD_LOGIC;
    empty_out_1   : out STD_LOGIC
  );
end readout;

architecture Behavioral of readout is


signal bx_cnt_s, bx_cnt_s1 : std_logic_vector(11 downto 0);
signal data_in_s, data_in_s1 : tdc_enc_a;
signal cnt_1 : natural range 5 downto 0 := 0;
signal cnt_2 : natural range 9 downto 0 := 0;
signal cnt_out : natural range 3 downto 0 := 0;
type din_t is array (0 to 39) of std_logic_vector(24 downto 0);
signal din_1, dout_1 : din_t := (others=>(others=>'0'));
type rd_en_1_t is array (0 to 3) of std_logic_vector(9 downto 0);
signal rd_en_1 : rd_en_1_t := (others=>(others=>'0'));
signal wr_en_1, empty_1, rd_en_1_i : std_logic_vector(39 downto 0) :=(others=>'0');
signal din_2, dout_2 : din_t := (others=>(others=>'0'));
signal wr_en_2, rd_en_2, empty_2 : std_logic_vector(39 downto 0) :=(others=>'0');
signal din_out : std_logic_vector(24 downto 0) :=(others=>'0');
signal wr_en_out, rd_en_out_0, rd_en_out_1, empty_out_0_s, empty_out_1_s : std_logic :='0';

begin

pipeline_proc: process(clk, rst)
begin
  if rst = '1' then
    data_in_s1 <= (others => (others => '0'));
    data_in_s <= (others => (others => '0'));
    bx_cnt_s1 <= (others => '0');
    bx_cnt_s <= (others => '0');
  elsif rising_edge(clk) then
    data_in_s1 <= data_in;
    data_in_s <= data_in_s1;
    bx_cnt_s1 <= bx_cnt;
    bx_cnt_s <= bx_cnt_s1;
  end if;
end process;

--data_in_s <= data_in;
--bx_cnt_s <= bx_cnt;

fifo_1_g: for i in 0 to 39 generate
    fifo_1 : entity work.ro_fifo_1
    PORT MAP (
        CLK   => clk,
        DATA  => din_1(i),
        RE    => rd_en_1_i(i),
        RESET => rst,
        WE    => wr_en_1(i),
        -- Outputs
        EMPTY => empty_1(i),
        FULL  => open,
        Q     => dout_1(i)
    );
end generate;

fifo_2_g: for i in 0 to 3 generate
    fifo_1 : entity work.ro_fifo_2
    PORT MAP (
        CLK   => clk,
        DATA  => din_2(i),
        RE    => rd_en_2(i),
        RESET => rst,
        WE    => wr_en_2(i),
        -- Outputs
        EMPTY => empty_2(i),
        FULL  => open,
        Q     => dout_2(i)
    );
end generate;

fifo_out_0: entity work.ro_fifo_out
    PORT MAP (
        WCLOCK   => clk,
        RCLOCK  => rdclk_0,
        DATA  => din_out,
        RE    => rd_en_out_0,
        RESET => rst,
        WE    => wr_en_out,
        -- Outputs
        EMPTY => empty_out_0_s,
        FULL  => open,
        Q     => data_out_0
    );
    
fifo_out_1: entity work.ro_fifo_out
    PORT MAP (
        WCLOCK   => clk,
        RCLOCK  => rdclk_1,
        DATA  => din_out,
        RE    => rd_en_out_1,
        RESET => rst,
        WE    => wr_en_out,
        -- Outputs
        EMPTY => empty_out_1_s,
        FULL  => open,
        Q     => data_out_1
    );
    
process(clk, rst)
begin
  if rst = '1' then
    cnt_1 <= 0;
    cnt_2 <= 0;
    cnt_out <= 0;
  elsif rising_edge(clk) then
    if cnt_1 >=5 then cnt_1 <= 0;
    else cnt_1 <= cnt_1 + 1;
    end if;

    if cnt_2 >= 9 then cnt_2 <= 0;
    else cnt_2 <= cnt_2 + 1;
    end if;

    if cnt_out >= 3 then cnt_out <= 0;
    else cnt_out <= cnt_out + 1;
    end if;
  end if;
end process;

write_1_g : for i in 0 to 39 generate
    write_1_p : process(clk, rst)
    begin
        if rst = '1' then
            din_1(i) <= (others=>'0');
            wr_en_1(i) <= '0';
        elsif clk'event and clk = '1' then
            if (data_in_s(i*6+cnt_1) /= "00000")then
                din_1(i) <= data_in_s(i*6+cnt_1) & bx_cnt_s & std_logic_vector(to_unsigned(i*6+cnt_1,8));
                wr_en_1(i) <= '1';
            else
                din_1(i) <= din_1(i);
                wr_en_1(i) <= '0';
            end if;
        end if;
    end process;
end generate;

write_2_g : for i in 0 to 3 generate
    write_2_p : process(clk, rst)
    begin
        if rst = '1' then
            wr_en_2(i) <= '0';
            din_2(i) <= (others=>'0');
            rd_en_1(i) <= (others=>'0');
        elsif clk'event and clk = '1' then
            if cnt_2 = 0 then
                rd_en_1(i)(9) <= '0';
            else
                rd_en_1(i)(cnt_2-1) <= '0';
            end if;
            if empty_1(i*10+cnt_2) = '0' then
                din_2(i) <= dout_1(i*10+cnt_2);
                wr_en_2(i) <= '1';
                rd_en_1(i)(cnt_2) <= '1';
            else
                din_2(i) <= din_2(i);
                wr_en_2(i) <= '0';
                rd_en_1(i)(cnt_2) <= '0';
            end if;
        end if;
    end process;
end generate;

rd_en_1_g : for i in 0 to 39 generate
    rd_en_1_i(i) <= rd_en_1(i/10)(i mod 10);
end generate;

write_out_p : process(clk, rst)
begin
    if rst = '1' then
        wr_en_out <= '0';
        din_out <= (others=>'0');
        rd_en_2 <= (others=>'0');
    elsif clk'event and clk = '1' then
        if cnt_out = 0 then
            rd_en_2(3) <= '0';
        else
            rd_en_2(cnt_out-1) <= '0';
        end if;
        if empty_2(cnt_out) = '0' then
            din_out <= dout_2(cnt_out);
            wr_en_out <= '1';
            rd_en_2(cnt_out) <= '1';
        else
            din_out <= din_out;
            wr_en_out <= '0';
            rd_en_2(cnt_out) <= '0';
        end if;
    end if;
end process;

rd_en_out_p_0 : process(rdclk_0)
begin
    if rdclk_0'event and rdclk_0 = '1' then
        rd_en_out_0 <= not empty_out_0_s;
    end if;
end process;

strobe_out_0 <= rd_en_out_0;
empty_out_0 <= empty_out_0_s;

rd_en_out_p_1 : process(rdclk_1)
begin
    if rdclk_1'event and rdclk_1 = '1' then
        rd_en_out_1 <= not empty_out_1_s;
    end if;
end process;

strobe_out_1 <= rd_en_out_1;
empty_out_1 <= empty_out_1_s;

end Behavioral;