library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;



entity ttc is
  port (
    clk40         : in std_logic;
    bc0           : in std_logic;
    oc0           : in std_logic;
    bunch_ctr     : out std_logic_vector(11 downto 0);
    orbit_ctr     : out std_logic_vector(31 downto 0)
  );
end entity;

architecture Behavioral of ttc is

  
begin
  
  process(clk40)
    variable bctr : unsigned(11 downto 0);
    variable octr : unsigned(31 downto 0);
  begin
    if rising_edge(clk40) then
      bunch_ctr <= std_logic_vector(bctr);
      orbit_ctr <= std_logic_vector(octr);

      if bc0 = '1' then 
        bctr := (others => '0');
        octr := octr + 1;
      else
        bctr := ( bctr + 1 ) mod 3564;
      end if;
      
      if oc0 = '1' then  octr := (others => '0');
      end if;
    end if;
  end process;


end architecture;