library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library polarfire;
use polarfire.all;

library work;
use work.obdt_pkg.all;


entity elink is
  PORT (
    rst                 : in std_logic;

    --  E-LINK
    E_IN_P, E_IN_N      : in std_logic;
    E_OUT_P, E_OUT_N    : out std_logic;
    
    clk40               : in std_logic;
    clk320              : in std_logic;
    
    -- DATA
    elink_data          : out std_logic_vector(7 downto 0);
    conf                : out conf_t := (others => (others => '0'));
    pulses              : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioral of elink is

  component INBUF_DIFF
  port(
    PADP  : in std_logic;
    PADN  : in std_logic;
    Y     : out std_logic
    );
  end component;
  
  component OUTBUF_DIFF
  port(
    D     : in std_logic;
    PADP  : out std_logic;
    PADN  : out std_logic
    );
  end component;
    
  signal e_in, e_out: std_logic;
  
  signal bitslip : std_logic;

  signal word8 : std_logic_vector(7 downto 0);
  signal sc_enable : std_logic := '0';
  signal eout_cal : std_logic ;
  signal eout_txword, sc_pipein : std_logic_vector(32 downto 0);
  
  signal conf_i : conf_t;
  attribute syn_ramstyle : string;
  attribute syn_ramstyle of conf_i : signal is "registers";
  
  signal bit0, bit1, bit2, bit3, bit4, bit5, bit6, bit7 : std_logic;  -- for debug
  
begin

  din_buf_ds : INBUF_DIFF
  PORT MAP ( PADP => E_IN_P, PADN => E_IN_N, Y => e_in );

  dout_buf_ds : OUTBUF_DIFF
  PORT MAP ( D => e_out, PADP => E_OUT_P, PADN => E_OUT_N );

  process(clk320)
    variable word8_v : std_logic_vector(7 downto 0);
    variable bitslip_pipe : std_logic_vector(1 downto 0);
    variable elinkdeser : natural range 7 downto 0;
  begin
    if rising_edge(clk320) then
      if bitslip_pipe(bitslip_pipe'high) = bitslip_pipe(bitslip_pipe'high-1) then
        if elinkdeser > 0 then 
          elinkdeser := elinkdeser - 1;
        else
          elinkdeser := 7;
          word8 <= word8_v;
        end if;
      end if;
      
      bitslip_pipe := bitslip_pipe(bitslip_pipe'high - 1 downto 0) & bitslip;
      word8_v := word8_v(6 downto 0) & e_in;

    end if;
  end process;
  
  e_out <= eout_txword(32);
  conf <= conf_i;

  process (rst, clk40)
    type cntdwn_t is array(15 downto 0) of unsigned(15 downto 0);
    variable cntdwn : cntdwn_t;
    variable bunch_ctr : unsigned(11 downto 0);
    variable unlocked : unsigned(3 downto 0);
    variable locked : boolean;
  begin
    if rst = '1' then
      sc_pipein <= (others => '0');
      cntdwn := (others => (others => '0') );
      eout_txword <= (others => '0');
      sc_enable <= '0';
      conf_i <=  (others => (others => '0') );
      eout_cal <= '0';
      pulses <= (others => '0');
      unlocked := (others => '1');
      locked := false;
      
    elsif rising_edge(clk40) then
      
      -- e-link byte alignment
      if unlocked = (unlocked'range => '0') then  locked := true;
      else                                        locked := false;
      end if;

      if word8(0) = '1' or bunch_ctr = x"DEB" then
        -- the alignment of bc0 controls the alignment of the elink deserialization.
        if bunch_ctr = x"DEB" and word8(0) = '1' then
          if unlocked /= (unlocked'range => '0') then unlocked := unlocked - 1;
          end if;
        else
          if unlocked /= (unlocked'range => '1') then
            unlocked := unlocked + 1;
          else
            bitslip <= not bitslip; -- produce an e-link deserialization bit slip
            unlocked(unlocked'high) := '0'; -- start bit-slipped evaluation in the middle of unlockedness
          end if;
        end if;

        bunch_ctr := (others => '0');
      else
        bunch_ctr := bunch_ctr + 1;
      end if;

      if locked then  elink_data <= word8;
      else            elink_data <= (others => '0');
      end if;

      -- advance output word
      eout_txword <= eout_txword(31 downto 0) & '0';
      if eout_cal = '1' then eout_txword(32) <= not eout_txword(32);
      end if;
      
      -- 32-bit slow control words
      if sc_pipein(32) = '1' and locked then
        if sc_pipein(31 downto 0) = x"5C15600D" then
          sc_enable <= '1';
        elsif sc_pipein(31 downto 0) = x"5C15BAAD" then
          sc_enable <= '0';
        elsif sc_enable = '1' then
          if sc_pipein(31 downto 24) = x"57" then 
            conf_i(to_integer(unsigned(sc_pipein(16+CONF_ADDR_WIDTH-1 downto 16)))) <= sc_pipein(15 downto 0);
          elsif sc_pipein(31 downto 24) = x"52" then 
            cntdwn(to_integer(unsigned(sc_pipein(19 downto 16)))) := unsigned(sc_pipein(15 downto 0));
          elsif sc_pipein(31 downto 0) = x"5ECA600D" then 
            eout_cal <= '1';
          elsif sc_pipein(31 downto 0) = x"5ECABAAD" then 
            eout_cal <= '0';
          elsif sc_pipein(31 downto 16) = x"5EFB" then
            eout_txword <= '1' & x"FB0000" & FW_VERS_SLV;
          elsif sc_pipein(31 downto 24) = x"8C" then
            eout_txword <= '1' & sc_pipein(31 downto 16) & conf_i(to_integer(unsigned(sc_pipein(16+CONF_ADDR_WIDTH-1 downto 16))));
          end if;
        end if;
        
        sc_pipein <= (others => '0');
      else
        sc_pipein <= sc_pipein(31 downto 0) & elink_data(6);
      end if;
      
      -- pulses output
      for i in 15 downto 0 loop
        if cntdwn(i) > 0 then
          cntdwn(i) := cntdwn(i) - 1;
          pulses(i) <= '1';
        else
          pulses(i) <= '0';
        end if;
      end loop;
    
    end if;
  end process;

  bit0 <= elink_data(0); -- for debug
  bit1 <= elink_data(1); -- for debug
  bit2 <= elink_data(2); -- for debug
  bit3 <= elink_data(3); -- for debug
  bit4 <= elink_data(4); -- for debug
  bit5 <= elink_data(5); -- for debug
  bit6 <= elink_data(6); -- for debug
  bit7 <= elink_data(7); -- for debug
      


end architecture;