library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;



entity testpulse is
  port (
    clk           : in std_logic;
    rst           : in std_logic;
    -- configuration
    enable        : in std_logic;
    intgen        : in std_logic;
    delay         : in unsigned(15 downto 0);
    duration      : in unsigned(5 downto 0);
    -- signals
    bgo           : in std_logic;
    TestPulse     : out std_logic
  );
end entity;

architecture Behavioral of testpulse is

  
begin
  
  process(clk)
    variable counter : unsigned(15 downto 0);
    variable tp : std_logic := '0';
    variable bgo_pipe : std_logic_vector(3 downto 0) := (others => '0');
    variable doing_bgo: boolean := false;
  begin
    if rising_edge(clk) then
      if rst = '1' or enable = '0' then
        tp := '0';
        bgo_pipe := (others => '0');
        counter := (others => '0');
        TestPulse <= '0';
        doing_bgo := false;
      else
        TestPulse <= tp; -- at top of process to add pipe level to tp.

        if intgen = '1' then -- Periodic TPs
          if counter = (counter'range => '0') then
            tp := not tp;
            if tp = '0' then  counter(delay'range) := delay;
            else              counter(duration'range) := duration;
            end if;
          else
            counter := counter - 1;
          end if;
          doing_bgo := false; -- for good state when changing from internal to bgo
        else -- BGo-triggered TPs
          if not doing_bgo then
            tp := '0';
            counter(delay'range) := delay;
            doing_bgo := bgo_pipe(bgo_pipe'high downto bgo_pipe'high - 1) = "01";
          else
            if counter = (counter'range => '0') then
              tp := not tp;
              if tp = '0' then  doing_bgo := false;
              else              counter(duration'range) := duration;
              end if;
            else
              counter := counter - 1;
            end if;
          end if;
        end if;
        
        bgo_pipe := bgo_pipe(bgo_pipe'high - 1 downto 0) & bgo;
      end if;
    end if;
  end process;


end architecture;