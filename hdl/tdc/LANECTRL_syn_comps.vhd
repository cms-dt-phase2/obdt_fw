-- Version: 
-- VHDL Black Box file 
-- 

library ieee;
use ieee.std_logic_1164.all;
library polarfire;
use polarfire.all;

entity LANECTRL is
	generic (
		DATA_RATE:real := 0.0;
		FORMAL_NAME:string := "";
		INTERFACE_NAME:string := "";
		DELAY_LINE_SIMULATION_MODE:string := "";
		INTERFACE_LEVEL:std_logic_vector := x"0";
		RESERVED_0:std_logic_vector := x"0";
		RESERVED_1:std_logic_vector := x"0";
		RESERVED_2:std_logic_vector := x"0";
		SOFTRESET_EN:std_logic_vector := x"0";
		SOFTRESET:std_logic_vector := x"0";
		RX_DQS_DELAY_LINE_EN:std_logic_vector := x"0";
		TX_DQS_DELAY_LINE_EN:std_logic_vector := x"0";
		RX_DQS_DELAY_LINE_DIRECTION:std_logic_vector := x"0";
		TX_DQS_DELAY_LINE_DIRECTION:std_logic_vector := x"0";
		RX_DQS_DELAY_VAL:std_logic_vector := x"0";
		TX_DQS_DELAY_VAL:std_logic_vector := x"0";
		FIFO_EN:std_logic_vector := x"0";
		FIFO_MODE:std_logic_vector := x"0";
		FIFO_RD_PTR_MODE:std_logic_vector := x"0";
		DQS_MODE:std_logic_vector := x"0";
		CDR_EN:std_logic_vector := x"0";
		HS_IO_CLK_SEL:std_logic_vector := x"0";
		DLL_CODE_SEL:std_logic_vector := x"0";
		CDR_CLK_SEL:std_logic_vector := x"0";
		READ_MARGIN_TEST_EN:std_logic_vector := x"0";
		WRITE_MARGIN_TEST_EN:std_logic_vector := x"0";
		CDR_CLK_DIV:std_logic_vector := x"0";
		DIV_CLK_SEL:std_logic_vector := x"0";
		HS_IO_CLK_PAUSE_EN:std_logic_vector := x"0";
		QDR_EN:std_logic_vector := x"0";
		DYN_ODT_MODE:std_logic_vector := x"0";
		DIV_CLK_EN_SRC:std_logic_vector := x"0";
		RANK_2_MODE:std_logic_vector := x"0"	);
   port( 
       FAB_CLK : in std_logic;
       RESET : in std_logic;
       DDR_READ : in std_logic;
       READ_CLK_SEL : in std_logic_vector(2 downto 0);
       DELAY_LINE_SEL : in std_logic;
       DELAY_LINE_LOAD : in std_logic;
       DELAY_LINE_DIRECTION : in std_logic;
       DELAY_LINE_MOVE : in std_logic;
       HS_IO_CLK_PAUSE : in std_logic;
       DIV_CLK_EN_N : in std_logic;
       RX_BIT_SLIP : in std_logic;
       CDR_CLK_A_SEL : in std_logic_vector(7 downto 0);
       EYE_MONITOR_WIDTH_IN : in std_logic_vector(2 downto 0);
       ODT_EN : in std_logic;
       CODE_UPDATE : in std_logic;
       RX_DATA_VALID : out std_logic;
       RX_BURST_DETECT : out std_logic;
       RX_DELAY_LINE_OUT_OF_RANGE : out std_logic;
       TX_DELAY_LINE_OUT_OF_RANGE : out std_logic;
       CLK_OUT_R : out std_logic;
       A_OUT_RST_N : out std_logic;
       DQS : in std_logic;
       DQS_N : in std_logic;
       HS_IO_CLK : in std_logic_vector(5 downto 0);
       DLL_CODE : in std_logic_vector(7 downto 0);
       EYE_MONITOR_WIDTH_OUT : out std_logic_vector(2 downto 0);
       ODT_EN_SEL : out std_logic;
       RX_DQS_90 : out std_logic_vector(1 downto 0);
       TX_DQS : out std_logic;
       TX_DQS_270 : out std_logic;
       FIFO_WR_PTR : out std_logic_vector(2 downto 0);
       FIFO_RD_PTR : out std_logic_vector(2 downto 0);
       CDR_CLK : out std_logic;
       CDR_NEXT_CLK : out std_logic;
       ARST_N : out std_logic;
       RX_SYNC_RST : out std_logic;
       TX_SYNC_RST : out std_logic;
       ODT_EN_OUT : out std_logic;
       DDR_DO_READ : in std_logic;
       CDR_CLK_A_SEL_8 : in std_logic;
       CDR_CLK_A_SEL_9 : in std_logic;
       CDR_CLK_A_SEL_10 : in std_logic;
       CDR_CLK_B_SEL : in std_logic_vector(10 downto 0);
       SWITCH : in std_logic;
       CDR_CLR_NEXT_CLK_N : in std_logic
   );
end LANECTRL;
architecture DEF_ARCH of LANECTRL is 

   attribute syn_black_box : boolean;
   attribute syn_black_box of DEF_ARCH : architecture is true;
   attribute black_box_pad_pin : string;
   attribute black_box_pad_pin of DEF_ARCH : architecture is "";

begin

end DEF_ARCH;
