//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Fri Apr 19 13:50:27 2019
// Version: v12.0 12.500.0.22
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// ides_43
module ides_43(
    // Inputs
    ARST_N,
    HS_IO_CLK_PAUSE,
    RXD,
    RXD_N,
    RX_CLK,
    // Outputs
    L0_RXD_DATA,
    L10_RXD_DATA,
    L11_RXD_DATA,
    L12_RXD_DATA,
    L13_RXD_DATA,
    L14_RXD_DATA,
    L15_RXD_DATA,
    L16_RXD_DATA,
    L17_RXD_DATA,
    L18_RXD_DATA,
    L19_RXD_DATA,
    L1_RXD_DATA,
    L20_RXD_DATA,
    L21_RXD_DATA,
    L22_RXD_DATA,
    L23_RXD_DATA,
    L24_RXD_DATA,
    L25_RXD_DATA,
    L26_RXD_DATA,
    L27_RXD_DATA,
    L28_RXD_DATA,
    L29_RXD_DATA,
    L2_RXD_DATA,
    L30_RXD_DATA,
    L31_RXD_DATA,
    L32_RXD_DATA,
    L33_RXD_DATA,
    L34_RXD_DATA,
    L35_RXD_DATA,
    L36_RXD_DATA,
    L37_RXD_DATA,
    L38_RXD_DATA,
    L39_RXD_DATA,
    L40_RXD_DATA,
    L41_RXD_DATA,
    L42_RXD_DATA,
    L3_RXD_DATA,
    L4_RXD_DATA,
    L5_RXD_DATA,
    L6_RXD_DATA,
    L7_RXD_DATA,
    L8_RXD_DATA,
    L9_RXD_DATA,
    RX_CLK_G
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input         ARST_N;
input         HS_IO_CLK_PAUSE;
input  [42:0] RXD;
input  [42:0] RXD_N;
input         RX_CLK;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output [9:0]  L0_RXD_DATA;
output [9:0]  L10_RXD_DATA;
output [9:0]  L11_RXD_DATA;
output [9:0]  L12_RXD_DATA;
output [9:0]  L13_RXD_DATA;
output [9:0]  L14_RXD_DATA;
output [9:0]  L15_RXD_DATA;
output [9:0]  L16_RXD_DATA;
output [9:0]  L17_RXD_DATA;
output [9:0]  L18_RXD_DATA;
output [9:0]  L19_RXD_DATA;
output [9:0]  L1_RXD_DATA;
output [9:0]  L20_RXD_DATA;
output [9:0]  L21_RXD_DATA;
output [9:0]  L22_RXD_DATA;
output [9:0]  L23_RXD_DATA;
output [9:0]  L24_RXD_DATA;
output [9:0]  L25_RXD_DATA;
output [9:0]  L26_RXD_DATA;
output [9:0]  L27_RXD_DATA;
output [9:0]  L28_RXD_DATA;
output [9:0]  L29_RXD_DATA;
output [9:0]  L2_RXD_DATA;
output [9:0]  L30_RXD_DATA;
output [9:0]  L31_RXD_DATA;
output [9:0]  L32_RXD_DATA;
output [9:0]  L33_RXD_DATA;
output [9:0]  L34_RXD_DATA;
output [9:0]  L35_RXD_DATA;
output [9:0]  L36_RXD_DATA;
output [9:0]  L37_RXD_DATA;
output [9:0]  L38_RXD_DATA;
output [9:0]  L39_RXD_DATA;
output [9:0]  L40_RXD_DATA;
output [9:0]  L41_RXD_DATA;
output [9:0]  L42_RXD_DATA;
output [9:0]  L3_RXD_DATA;
output [9:0]  L4_RXD_DATA;
output [9:0]  L5_RXD_DATA;
output [9:0]  L6_RXD_DATA;
output [9:0]  L7_RXD_DATA;
output [9:0]  L8_RXD_DATA;
output [9:0]  L9_RXD_DATA;
output        RX_CLK_G;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire          ARST_N;
wire          CLK_0_Y;
wire          HS_IO_CLK_CASCADED_Y;
wire          HS_IO_CLK_FIFO_Y;
wire          HS_IO_CLK_PAUSE;
wire          HS_IO_CLK_RX_Y;
wire   [9:0]  L0_RXD_DATA_net_0;
wire   [9:0]  L1_RXD_DATA_net_0;
wire   [9:0]  L2_RXD_DATA_net_0;
wire   [9:0]  L3_RXD_DATA_net_0;
wire   [9:0]  L4_RXD_DATA_net_0;
wire   [9:0]  L5_RXD_DATA_net_0;
wire   [9:0]  L6_RXD_DATA_net_0;
wire   [9:0]  L7_RXD_DATA_net_0;
wire   [9:0]  L8_RXD_DATA_net_0;
wire   [9:0]  L9_RXD_DATA_net_0;
wire   [9:0]  L10_RXD_DATA_net_0;
wire   [9:0]  L11_RXD_DATA_net_0;
wire   [9:0]  L12_RXD_DATA_net_0;
wire   [9:0]  L13_RXD_DATA_net_0;
wire   [9:0]  L14_RXD_DATA_net_0;
wire   [9:0]  L15_RXD_DATA_net_0;
wire   [9:0]  L16_RXD_DATA_net_0;
wire   [9:0]  L17_RXD_DATA_net_0;
wire   [9:0]  L18_RXD_DATA_net_0;
wire   [9:0]  L19_RXD_DATA_net_0;
wire   [9:0]  L20_RXD_DATA_net_0;
wire   [9:0]  L21_RXD_DATA_net_0;
wire   [9:0]  L22_RXD_DATA_net_0;
wire   [9:0]  L23_RXD_DATA_net_0;
wire   [9:0]  L24_RXD_DATA_net_0;
wire   [9:0]  L25_RXD_DATA_net_0;
wire   [9:0]  L26_RXD_DATA_net_0;
wire   [9:0]  L27_RXD_DATA_net_0;
wire   [9:0]  L28_RXD_DATA_net_0;
wire   [9:0]  L29_RXD_DATA_net_0;
wire   [9:0]  L30_RXD_DATA_net_0;
wire   [9:0]  L31_RXD_DATA_net_0;
wire   [9:0]  L32_RXD_DATA_net_0;
wire   [9:0]  L33_RXD_DATA_net_0;
wire   [9:0]  L34_RXD_DATA_net_0;
wire   [9:0]  L35_RXD_DATA_net_0;
wire   [9:0]  L36_RXD_DATA_net_0;
wire   [9:0]  L37_RXD_DATA_net_0;
wire   [9:0]  L38_RXD_DATA_net_0;
wire   [9:0]  L39_RXD_DATA_net_0;
wire   [9:0]  L40_RXD_DATA_net_0;
wire   [9:0]  L41_RXD_DATA_net_0;
wire   [9:0]  L42_RXD_DATA_net_0;

wire          PF_CLK_DIV_FIFO_CLK_DIV_OUT;
wire          PF_CLK_DIV_FIFO_CLK_OUT_HS_IO_CLK;
wire          PF_CLK_DIV_RXCLK_CLK_OUT_HS_IO_CLK;
wire          PF_LANECTRL_0_ARST_N;
wire   [2:0]  PF_LANECTRL_0_FIFO_RD_PTR;
wire   [2:0]  PF_LANECTRL_0_FIFO_WR_PTR;
wire   [0:0]  PF_LANECTRL_0_RX_DQS_90;
wire          PF_LANECTRL_0_RX_SYNC_RST;
wire          PF_LANECTRL_0_TX_SYNC_RST;
wire          RX_CLK;
wire          RX_CLK_G_net_0;
wire   [42:0] RXD;
wire   [42:0] RXD_N;
wire   [9:0]  L0_RXD_DATA_net_1;
wire   [9:0]  L1_RXD_DATA_net_1;
wire   [9:0]  L2_RXD_DATA_net_1;
wire   [9:0]  L3_RXD_DATA_net_1;
wire   [9:0]  L4_RXD_DATA_net_1;
wire   [9:0]  L5_RXD_DATA_net_1;
wire   [9:0]  L6_RXD_DATA_net_1;
wire   [9:0]  L7_RXD_DATA_net_1;
wire   [9:0]  L8_RXD_DATA_net_1;
wire   [9:0]  L9_RXD_DATA_net_1;
wire   [9:0]  L10_RXD_DATA_net_1;
wire   [9:0]  L11_RXD_DATA_net_1;
wire   [9:0]  L12_RXD_DATA_net_1;
wire   [9:0]  L13_RXD_DATA_net_1;
wire   [9:0]  L14_RXD_DATA_net_1;
wire   [9:0]  L15_RXD_DATA_net_1;
wire   [9:0]  L16_RXD_DATA_net_1;
wire   [9:0]  L17_RXD_DATA_net_1;
wire   [9:0]  L18_RXD_DATA_net_1;
wire   [9:0]  L19_RXD_DATA_net_1;
wire   [9:0]  L20_RXD_DATA_net_1;
wire   [9:0]  L21_RXD_DATA_net_1;
wire   [9:0]  L22_RXD_DATA_net_1;
wire   [9:0]  L23_RXD_DATA_net_1;
wire   [9:0]  L24_RXD_DATA_net_1;
wire   [9:0]  L25_RXD_DATA_net_1;
wire   [9:0]  L26_RXD_DATA_net_1;
wire   [9:0]  L27_RXD_DATA_net_1;
wire   [9:0]  L28_RXD_DATA_net_1;
wire   [9:0]  L29_RXD_DATA_net_1;
wire   [9:0]  L30_RXD_DATA_net_1;
wire   [9:0]  L31_RXD_DATA_net_1;
wire   [9:0]  L32_RXD_DATA_net_1;
wire   [9:0]  L33_RXD_DATA_net_1;
wire   [9:0]  L34_RXD_DATA_net_1;
wire   [9:0]  L35_RXD_DATA_net_1;
wire   [9:0]  L36_RXD_DATA_net_1;
wire   [9:0]  L37_RXD_DATA_net_1;
wire   [9:0]  L38_RXD_DATA_net_1;
wire   [9:0]  L39_RXD_DATA_net_1;
wire   [9:0]  L40_RXD_DATA_net_1;
wire   [9:0]  L41_RXD_DATA_net_1;
wire   [9:0]  L42_RXD_DATA_net_1;

wire          RX_CLK_G_net_1;
wire   [1:0]  HS_IO_CLK_net_0;
//--------------------------------------------------------------------
// TiedOff Nets
//--------------------------------------------------------------------
wire          GND_net;
wire   [2:0]  EYE_MONITOR_LANE_WIDTH_const_net_0;
wire   [9:0]  TX_DATA_0_const_net_0;
wire   [9:0]  TX_DATA_1_const_net_0;
wire   [9:0]  TX_DATA_2_const_net_0;
wire   [9:0]  TX_DATA_3_const_net_0;
wire   [9:0]  TX_DATA_4_const_net_0;
wire   [9:0]  TX_DATA_5_const_net_0;
wire   [9:0]  TX_DATA_6_const_net_0;
wire   [9:0]  TX_DATA_7_const_net_0;
wire   [9:0]  TX_DATA_8_const_net_0;
wire   [9:0]  TX_DATA_9_const_net_0;
wire   [9:0]  TX_DATA_10_const_net_0;
wire   [9:0]  TX_DATA_11_const_net_0;
wire   [9:0]  TX_DATA_12_const_net_0;
wire   [9:0]  TX_DATA_13_const_net_0;
wire   [9:0]  TX_DATA_14_const_net_0;
wire   [9:0]  TX_DATA_15_const_net_0;
wire   [9:0]  TX_DATA_16_const_net_0;
wire   [9:0]  TX_DATA_17_const_net_0;
wire   [9:0]  TX_DATA_18_const_net_0;
wire   [9:0]  TX_DATA_19_const_net_0;
wire   [9:0]  TX_DATA_20_const_net_0;
wire   [9:0]  TX_DATA_21_const_net_0;
wire   [9:0]  TX_DATA_22_const_net_0;
wire   [9:0]  TX_DATA_23_const_net_0;
wire   [9:0]  TX_DATA_24_const_net_0;
wire   [9:0]  TX_DATA_25_const_net_0;
wire   [9:0]  TX_DATA_26_const_net_0;
wire   [9:0]  TX_DATA_27_const_net_0;
wire   [9:0]  TX_DATA_28_const_net_0;
wire   [9:0]  TX_DATA_29_const_net_0;
wire   [9:0]  TX_DATA_30_const_net_0;
wire   [9:0]  TX_DATA_31_const_net_0;
wire   [9:0]  TX_DATA_32_const_net_0;
wire   [9:0]  TX_DATA_33_const_net_0;
wire   [9:0]  TX_DATA_34_const_net_0;
wire   [9:0]  TX_DATA_35_const_net_0;
wire   [9:0]  TX_DATA_36_const_net_0;
wire   [9:0]  TX_DATA_37_const_net_0;
wire   [9:0]  TX_DATA_38_const_net_0;
wire   [9:0]  TX_DATA_39_const_net_0;
wire   [9:0]  TX_DATA_40_const_net_0;
wire   [9:0]  TX_DATA_41_const_net_0;
wire   [9:0]  TX_DATA_42_const_net_0;
wire   [9:0]  TX_DATA_43_const_net_0;
wire   [9:0]  TX_DATA_44_const_net_0;
wire   [9:0]  TX_DATA_45_const_net_0;
wire   [9:0]  TX_DATA_46_const_net_0;
wire   [9:0]  TX_DATA_47_const_net_0;
wire   [9:0]  TX_DATA_48_const_net_0;
wire   [9:0]  TX_DATA_49_const_net_0;
wire   [9:0]  TX_DATA_50_const_net_0;
wire   [9:0]  TX_DATA_51_const_net_0;
wire   [9:0]  TX_DATA_52_const_net_0;
wire   [9:0]  TX_DATA_53_const_net_0;
wire   [9:0]  TX_DATA_54_const_net_0;
wire   [9:0]  TX_DATA_55_const_net_0;
wire   [9:0]  TX_DATA_56_const_net_0;
wire   [9:0]  TX_DATA_57_const_net_0;
wire   [9:0]  TX_DATA_58_const_net_0;
wire   [9:0]  TX_DATA_59_const_net_0;
wire   [9:0]  TX_DATA_60_const_net_0;
wire   [9:0]  TX_DATA_61_const_net_0;
wire   [9:0]  TX_DATA_62_const_net_0;
wire   [9:0]  TX_DATA_63_const_net_0;
wire   [3:0]  OE_DATA_0_const_net_0;
wire   [3:0]  OE_DATA_1_const_net_0;
wire   [3:0]  OE_DATA_2_const_net_0;
wire   [3:0]  OE_DATA_3_const_net_0;
wire   [3:0]  OE_DATA_4_const_net_0;
wire   [3:0]  OE_DATA_5_const_net_0;
wire   [3:0]  OE_DATA_6_const_net_0;
wire   [3:0]  OE_DATA_7_const_net_0;
wire   [3:0]  OE_DATA_8_const_net_0;
wire   [3:0]  OE_DATA_9_const_net_0;
wire   [3:0]  OE_DATA_10_const_net_0;
wire   [3:0]  OE_DATA_11_const_net_0;
wire   [3:0]  OE_DATA_12_const_net_0;
wire   [3:0]  OE_DATA_13_const_net_0;
wire   [3:0]  OE_DATA_14_const_net_0;
wire   [3:0]  OE_DATA_15_const_net_0;
wire   [3:0]  OE_DATA_16_const_net_0;
wire   [3:0]  OE_DATA_17_const_net_0;
wire   [3:0]  OE_DATA_18_const_net_0;
wire   [3:0]  OE_DATA_19_const_net_0;
wire   [3:0]  OE_DATA_20_const_net_0;
wire   [3:0]  OE_DATA_21_const_net_0;
wire   [3:0]  OE_DATA_22_const_net_0;
wire   [3:0]  OE_DATA_23_const_net_0;
wire   [3:0]  OE_DATA_24_const_net_0;
wire   [3:0]  OE_DATA_25_const_net_0;
wire   [3:0]  OE_DATA_26_const_net_0;
wire   [3:0]  OE_DATA_27_const_net_0;
wire   [3:0]  OE_DATA_28_const_net_0;
wire   [3:0]  OE_DATA_29_const_net_0;
wire   [3:0]  OE_DATA_30_const_net_0;
wire   [3:0]  OE_DATA_31_const_net_0;
wire   [3:0]  OE_DATA_32_const_net_0;
wire   [3:0]  OE_DATA_33_const_net_0;
wire   [3:0]  OE_DATA_34_const_net_0;
wire   [3:0]  OE_DATA_35_const_net_0;
wire   [3:0]  OE_DATA_36_const_net_0;
wire   [3:0]  OE_DATA_37_const_net_0;
wire   [3:0]  OE_DATA_38_const_net_0;
wire   [3:0]  OE_DATA_39_const_net_0;
wire   [3:0]  OE_DATA_40_const_net_0;
wire   [3:0]  OE_DATA_41_const_net_0;
wire   [3:0]  OE_DATA_42_const_net_0;
wire   [3:0]  OE_DATA_43_const_net_0;
wire   [3:0]  OE_DATA_44_const_net_0;
wire   [3:0]  OE_DATA_45_const_net_0;
wire   [3:0]  OE_DATA_46_const_net_0;
wire   [3:0]  OE_DATA_47_const_net_0;
wire   [3:0]  OE_DATA_48_const_net_0;
wire   [3:0]  OE_DATA_49_const_net_0;
wire   [3:0]  OE_DATA_50_const_net_0;
wire   [3:0]  OE_DATA_51_const_net_0;
wire   [3:0]  OE_DATA_52_const_net_0;
wire   [3:0]  OE_DATA_53_const_net_0;
wire   [3:0]  OE_DATA_54_const_net_0;
wire   [3:0]  OE_DATA_55_const_net_0;
wire   [3:0]  OE_DATA_56_const_net_0;
wire   [3:0]  OE_DATA_57_const_net_0;
wire   [3:0]  OE_DATA_58_const_net_0;
wire   [3:0]  OE_DATA_59_const_net_0;
wire   [3:0]  OE_DATA_60_const_net_0;
wire   [3:0]  OE_DATA_61_const_net_0;
wire   [3:0]  OE_DATA_62_const_net_0;
wire   [3:0]  OE_DATA_63_const_net_0;
wire   [42:0] PAD_const_net_0;
wire   [42:0] PAD_N_const_net_0;
wire   [7:0]  DLL_CODE_const_net_0;
wire   [2:0]  READ_CLK_SEL_const_net_0;
wire   [2:0]  EYE_MONITOR_WIDTH_IN_const_net_0;
wire   [7:0]  CDR_CLK_A_SEL_const_net_0;
wire          VCC_net;
wire   [10:0] CDR_CLK_B_SEL_const_net_0;
//--------------------------------------------------------------------
// Inverted Nets
//--------------------------------------------------------------------
wire          RESET_IN_POST_INV0_0;
//--------------------------------------------------------------------
// Constant assignments
//--------------------------------------------------------------------
assign GND_net                            = 1'b0;
assign EYE_MONITOR_LANE_WIDTH_const_net_0 = 3'h0;
assign TX_DATA_0_const_net_0              = 10'h000;
assign TX_DATA_1_const_net_0              = 10'h000;
assign TX_DATA_2_const_net_0              = 10'h000;
assign TX_DATA_3_const_net_0              = 10'h000;
assign TX_DATA_4_const_net_0              = 10'h000;
assign TX_DATA_5_const_net_0              = 10'h000;
assign TX_DATA_6_const_net_0              = 10'h000;
assign TX_DATA_7_const_net_0              = 10'h000;
assign TX_DATA_8_const_net_0              = 10'h000;
assign TX_DATA_9_const_net_0              = 10'h000;
assign TX_DATA_10_const_net_0             = 10'h000;
assign TX_DATA_11_const_net_0             = 10'h000;
assign TX_DATA_12_const_net_0             = 10'h000;
assign TX_DATA_13_const_net_0             = 10'h000;
assign TX_DATA_14_const_net_0             = 10'h000;
assign TX_DATA_15_const_net_0             = 10'h000;
assign TX_DATA_16_const_net_0             = 10'h000;
assign TX_DATA_17_const_net_0             = 10'h000;
assign TX_DATA_18_const_net_0             = 10'h000;
assign TX_DATA_19_const_net_0             = 10'h000;
assign TX_DATA_20_const_net_0             = 10'h000;
assign TX_DATA_21_const_net_0             = 10'h000;
assign TX_DATA_22_const_net_0             = 10'h000;
assign TX_DATA_23_const_net_0             = 10'h000;
assign TX_DATA_24_const_net_0             = 10'h000;
assign TX_DATA_25_const_net_0             = 10'h000;
assign TX_DATA_26_const_net_0             = 10'h000;
assign TX_DATA_27_const_net_0             = 10'h000;
assign TX_DATA_28_const_net_0             = 10'h000;
assign TX_DATA_29_const_net_0             = 10'h000;
assign TX_DATA_30_const_net_0             = 10'h000;
assign TX_DATA_31_const_net_0             = 10'h000;
assign TX_DATA_32_const_net_0             = 10'h000;
assign TX_DATA_33_const_net_0             = 10'h000;
assign TX_DATA_34_const_net_0             = 10'h000;
assign TX_DATA_35_const_net_0             = 10'h000;
assign TX_DATA_36_const_net_0             = 10'h000;
assign TX_DATA_37_const_net_0             = 10'h000;
assign TX_DATA_38_const_net_0             = 10'h000;
assign TX_DATA_39_const_net_0             = 10'h000;
assign TX_DATA_40_const_net_0             = 10'h000;
assign TX_DATA_41_const_net_0             = 10'h000;
assign TX_DATA_42_const_net_0             = 10'h000;
assign TX_DATA_43_const_net_0             = 10'h000;
assign TX_DATA_44_const_net_0             = 10'h000;
assign TX_DATA_45_const_net_0             = 10'h000;
assign TX_DATA_46_const_net_0             = 10'h000;
assign TX_DATA_47_const_net_0             = 10'h000;
assign TX_DATA_48_const_net_0             = 10'h000;
assign TX_DATA_49_const_net_0             = 10'h000;
assign TX_DATA_50_const_net_0             = 10'h000;
assign TX_DATA_51_const_net_0             = 10'h000;
assign TX_DATA_52_const_net_0             = 10'h000;
assign TX_DATA_53_const_net_0             = 10'h000;
assign TX_DATA_54_const_net_0             = 10'h000;
assign TX_DATA_55_const_net_0             = 10'h000;
assign TX_DATA_56_const_net_0             = 10'h000;
assign TX_DATA_57_const_net_0             = 10'h000;
assign TX_DATA_58_const_net_0             = 10'h000;
assign TX_DATA_59_const_net_0             = 10'h000;
assign TX_DATA_60_const_net_0             = 10'h000;
assign TX_DATA_61_const_net_0             = 10'h000;
assign TX_DATA_62_const_net_0             = 10'h000;
assign TX_DATA_63_const_net_0             = 10'h000;
assign OE_DATA_0_const_net_0              = 4'h0;
assign OE_DATA_1_const_net_0              = 4'h0;
assign OE_DATA_2_const_net_0              = 4'h0;
assign OE_DATA_3_const_net_0              = 4'h0;
assign OE_DATA_4_const_net_0              = 4'h0;
assign OE_DATA_5_const_net_0              = 4'h0;
assign OE_DATA_6_const_net_0              = 4'h0;
assign OE_DATA_7_const_net_0              = 4'h0;
assign OE_DATA_8_const_net_0              = 4'h0;
assign OE_DATA_9_const_net_0              = 4'h0;
assign OE_DATA_10_const_net_0             = 4'h0;
assign OE_DATA_11_const_net_0             = 4'h0;
assign OE_DATA_12_const_net_0             = 4'h0;
assign OE_DATA_13_const_net_0             = 4'h0;
assign OE_DATA_14_const_net_0             = 4'h0;
assign OE_DATA_15_const_net_0             = 4'h0;
assign OE_DATA_16_const_net_0             = 4'h0;
assign OE_DATA_17_const_net_0             = 4'h0;
assign OE_DATA_18_const_net_0             = 4'h0;
assign OE_DATA_19_const_net_0             = 4'h0;
assign OE_DATA_20_const_net_0             = 4'h0;
assign OE_DATA_21_const_net_0             = 4'h0;
assign OE_DATA_22_const_net_0             = 4'h0;
assign OE_DATA_23_const_net_0             = 4'h0;
assign OE_DATA_24_const_net_0             = 4'h0;
assign OE_DATA_25_const_net_0             = 4'h0;
assign OE_DATA_26_const_net_0             = 4'h0;
assign OE_DATA_27_const_net_0             = 4'h0;
assign OE_DATA_28_const_net_0             = 4'h0;
assign OE_DATA_29_const_net_0             = 4'h0;
assign OE_DATA_30_const_net_0             = 4'h0;
assign OE_DATA_31_const_net_0             = 4'h0;
assign OE_DATA_32_const_net_0             = 4'h0;
assign OE_DATA_33_const_net_0             = 4'h0;
assign OE_DATA_34_const_net_0             = 4'h0;
assign OE_DATA_35_const_net_0             = 4'h0;
assign OE_DATA_36_const_net_0             = 4'h0;
assign OE_DATA_37_const_net_0             = 4'h0;
assign OE_DATA_38_const_net_0             = 4'h0;
assign OE_DATA_39_const_net_0             = 4'h0;
assign OE_DATA_40_const_net_0             = 4'h0;
assign OE_DATA_41_const_net_0             = 4'h0;
assign OE_DATA_42_const_net_0             = 4'h0;
assign OE_DATA_43_const_net_0             = 4'h0;
assign OE_DATA_44_const_net_0             = 4'h0;
assign OE_DATA_45_const_net_0             = 4'h0;
assign OE_DATA_46_const_net_0             = 4'h0;
assign OE_DATA_47_const_net_0             = 4'h0;
assign OE_DATA_48_const_net_0             = 4'h0;
assign OE_DATA_49_const_net_0             = 4'h0;
assign OE_DATA_50_const_net_0             = 4'h0;
assign OE_DATA_51_const_net_0             = 4'h0;
assign OE_DATA_52_const_net_0             = 4'h0;
assign OE_DATA_53_const_net_0             = 4'h0;
assign OE_DATA_54_const_net_0             = 4'h0;
assign OE_DATA_55_const_net_0             = 4'h0;
assign OE_DATA_56_const_net_0             = 4'h0;
assign OE_DATA_57_const_net_0             = 4'h0;
assign OE_DATA_58_const_net_0             = 4'h0;
assign OE_DATA_59_const_net_0             = 4'h0;
assign OE_DATA_60_const_net_0             = 4'h0;
assign OE_DATA_61_const_net_0             = 4'h0;
assign OE_DATA_62_const_net_0             = 4'h0;
assign OE_DATA_63_const_net_0             = 4'h0;
assign PAD_const_net_0                    = 32'h00000000;
assign PAD_N_const_net_0                  = 32'h00000000;
assign DLL_CODE_const_net_0               = 8'h00;
assign READ_CLK_SEL_const_net_0           = 3'h0;
assign EYE_MONITOR_WIDTH_IN_const_net_0   = 3'h0;
assign CDR_CLK_A_SEL_const_net_0          = 8'h00;
assign VCC_net                            = 1'b1;
assign CDR_CLK_B_SEL_const_net_0          = 11'h000;
//--------------------------------------------------------------------
// Inversions
//--------------------------------------------------------------------
assign RESET_IN_POST_INV0_0 = ~ ARST_N;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign L0_RXD_DATA_net_1  = L0_RXD_DATA_net_0;
assign L0_RXD_DATA[9:0]   = L0_RXD_DATA_net_1;
assign L1_RXD_DATA_net_1  = L1_RXD_DATA_net_0;
assign L1_RXD_DATA[9:0]   = L1_RXD_DATA_net_1;
assign L2_RXD_DATA_net_1  = L2_RXD_DATA_net_0;
assign L2_RXD_DATA[9:0]   = L2_RXD_DATA_net_1;
assign L3_RXD_DATA_net_1  = L3_RXD_DATA_net_0;
assign L3_RXD_DATA[9:0]   = L3_RXD_DATA_net_1;
assign L4_RXD_DATA_net_1  = L4_RXD_DATA_net_0;
assign L4_RXD_DATA[9:0]   = L4_RXD_DATA_net_1;
assign L5_RXD_DATA_net_1  = L5_RXD_DATA_net_0;
assign L5_RXD_DATA[9:0]   = L5_RXD_DATA_net_1;
assign L6_RXD_DATA_net_1  = L6_RXD_DATA_net_0;
assign L6_RXD_DATA[9:0]   = L6_RXD_DATA_net_1;
assign L7_RXD_DATA_net_1  = L7_RXD_DATA_net_0;
assign L7_RXD_DATA[9:0]   = L7_RXD_DATA_net_1;
assign L8_RXD_DATA_net_1  = L8_RXD_DATA_net_0;
assign L8_RXD_DATA[9:0]   = L8_RXD_DATA_net_1;
assign L9_RXD_DATA_net_1  = L9_RXD_DATA_net_0;
assign L9_RXD_DATA[9:0]   = L9_RXD_DATA_net_1;
assign L10_RXD_DATA_net_1 = L10_RXD_DATA_net_0;
assign L10_RXD_DATA[9:0]  = L10_RXD_DATA_net_1;
assign L11_RXD_DATA_net_1 = L11_RXD_DATA_net_0;
assign L11_RXD_DATA[9:0]  = L11_RXD_DATA_net_1;
assign L12_RXD_DATA_net_1 = L12_RXD_DATA_net_0;
assign L12_RXD_DATA[9:0]  = L12_RXD_DATA_net_1;
assign L13_RXD_DATA_net_1 = L13_RXD_DATA_net_0;
assign L13_RXD_DATA[9:0]  = L13_RXD_DATA_net_1;
assign L14_RXD_DATA_net_1 = L14_RXD_DATA_net_0;
assign L14_RXD_DATA[9:0]  = L14_RXD_DATA_net_1;
assign L15_RXD_DATA_net_1 = L15_RXD_DATA_net_0;
assign L15_RXD_DATA[9:0]  = L15_RXD_DATA_net_1;
assign L16_RXD_DATA_net_1 = L16_RXD_DATA_net_0;
assign L16_RXD_DATA[9:0]  = L16_RXD_DATA_net_1;
assign L17_RXD_DATA_net_1 = L17_RXD_DATA_net_0;
assign L17_RXD_DATA[9:0]  = L17_RXD_DATA_net_1;
assign L18_RXD_DATA_net_1 = L18_RXD_DATA_net_0;
assign L18_RXD_DATA[9:0]  = L18_RXD_DATA_net_1;
assign L19_RXD_DATA_net_1 = L19_RXD_DATA_net_0;
assign L19_RXD_DATA[9:0]  = L19_RXD_DATA_net_1;
assign L20_RXD_DATA_net_1 = L20_RXD_DATA_net_0;
assign L20_RXD_DATA[9:0]  = L20_RXD_DATA_net_1;
assign L21_RXD_DATA_net_1 = L21_RXD_DATA_net_0;
assign L21_RXD_DATA[9:0]  = L21_RXD_DATA_net_1;
assign L22_RXD_DATA_net_1 = L22_RXD_DATA_net_0;
assign L22_RXD_DATA[9:0]  = L22_RXD_DATA_net_1;
assign L23_RXD_DATA_net_1 = L23_RXD_DATA_net_0;
assign L23_RXD_DATA[9:0]  = L23_RXD_DATA_net_1;
assign L24_RXD_DATA_net_1 = L24_RXD_DATA_net_0;
assign L24_RXD_DATA[9:0]  = L24_RXD_DATA_net_1;
assign L25_RXD_DATA_net_1 = L25_RXD_DATA_net_0;
assign L25_RXD_DATA[9:0]  = L25_RXD_DATA_net_1;
assign L26_RXD_DATA_net_1 = L26_RXD_DATA_net_0;
assign L26_RXD_DATA[9:0]  = L26_RXD_DATA_net_1;
assign L27_RXD_DATA_net_1 = L27_RXD_DATA_net_0;
assign L27_RXD_DATA[9:0]  = L27_RXD_DATA_net_1;
assign L28_RXD_DATA_net_1 = L28_RXD_DATA_net_0;
assign L28_RXD_DATA[9:0]  = L28_RXD_DATA_net_1;
assign L29_RXD_DATA_net_1 = L29_RXD_DATA_net_0;
assign L29_RXD_DATA[9:0]  = L29_RXD_DATA_net_1;
assign L30_RXD_DATA_net_1 = L30_RXD_DATA_net_0;
assign L30_RXD_DATA[9:0]  = L30_RXD_DATA_net_1;
assign L31_RXD_DATA_net_1 = L31_RXD_DATA_net_0;
assign L31_RXD_DATA[9:0]  = L31_RXD_DATA_net_1;

assign L32_RXD_DATA_net_1 = L32_RXD_DATA_net_0;
assign L32_RXD_DATA[9:0]  = L32_RXD_DATA_net_1;
assign L33_RXD_DATA_net_1 = L33_RXD_DATA_net_0;
assign L33_RXD_DATA[9:0]  = L33_RXD_DATA_net_1;

assign L34_RXD_DATA_net_1 = L34_RXD_DATA_net_0;
assign L34_RXD_DATA[9:0]  = L34_RXD_DATA_net_1;
assign L35_RXD_DATA_net_1 = L35_RXD_DATA_net_0;
assign L35_RXD_DATA[9:0]  = L35_RXD_DATA_net_1;
assign L36_RXD_DATA_net_1 = L36_RXD_DATA_net_0;
assign L36_RXD_DATA[9:0]  = L36_RXD_DATA_net_1;
assign L37_RXD_DATA_net_1 = L37_RXD_DATA_net_0;
assign L37_RXD_DATA[9:0]  = L37_RXD_DATA_net_1;
assign L38_RXD_DATA_net_1 = L38_RXD_DATA_net_0;
assign L38_RXD_DATA[9:0]  = L38_RXD_DATA_net_1;
assign L39_RXD_DATA_net_1 = L39_RXD_DATA_net_0;
assign L39_RXD_DATA[9:0]  = L39_RXD_DATA_net_1;

assign L40_RXD_DATA_net_1 = L40_RXD_DATA_net_0;
assign L40_RXD_DATA[9:0]  = L40_RXD_DATA_net_1;
assign L41_RXD_DATA_net_1 = L41_RXD_DATA_net_0;
assign L41_RXD_DATA[9:0]  = L41_RXD_DATA_net_1;
assign L42_RXD_DATA_net_1 = L42_RXD_DATA_net_0;
assign L42_RXD_DATA[9:0]  = L42_RXD_DATA_net_1;

assign RX_CLK_G_net_1     = RX_CLK_G_net_0;
assign RX_CLK_G           = RX_CLK_G_net_1;
//--------------------------------------------------------------------
// Concatenation assignments
//--------------------------------------------------------------------
assign HS_IO_CLK_net_0 = { HS_IO_CLK_RX_Y , HS_IO_CLK_FIFO_Y };
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------INBUF
//INBUF CLK_0(
        //// Inputs
        //.PAD ( RX_CLK ),
        //// Outputs
        //.Y   ( CLK_0_Y ) 
        //);

//--------CLKINT
CLKINT CLKINT_0(
        // Inputs
        .A ( PF_CLK_DIV_FIFO_CLK_DIV_OUT ),
        // Outputs
        .Y ( RX_CLK_G_net_0 ) 
        );

assign HS_IO_CLK_CASCADED_Y = RX_CLK;

//--------HS_IO_CLK
//HS_IO_CLK HS_IO_CLK_CASCADED(
        //// Inputs
        //.A ( CLK_0_Y ),
        //// Outputs
        //.Y ( HS_IO_CLK_CASCADED_Y ) 
        //);

//--------HS_IO_CLK
HS_IO_CLK HS_IO_CLK_FIFO(
        // Inputs
        .A ( PF_CLK_DIV_FIFO_CLK_OUT_HS_IO_CLK ),
        // Outputs
        .Y ( HS_IO_CLK_FIFO_Y ) 
        );

//--------HS_IO_CLK
HS_IO_CLK HS_IO_CLK_RX(
        // Inputs
        .A ( PF_CLK_DIV_RXCLK_CLK_OUT_HS_IO_CLK ),
        // Outputs
        .Y ( HS_IO_CLK_RX_Y ) 
        );

//--------ides_32_PF_CLK_DIV_FIFO_PF_CLK_DIV_DELAY   -   Actel:SgCore:PF_CLK_DIV_DELAY:1.1.101
ides_43_PF_CLK_DIV_FIFO_PF_CLK_DIV_DELAY PF_CLK_DIV_FIFO(
        // Inputs
        .CLK_IN            ( HS_IO_CLK_CASCADED_Y ),
        // Outputs
        .CLK_OUT_HS_IO_CLK ( PF_CLK_DIV_FIFO_CLK_OUT_HS_IO_CLK ),
        .CLK_DIV_OUT       ( PF_CLK_DIV_FIFO_CLK_DIV_OUT ) 
        );

//--------ides_32_PF_CLK_DIV_RXCLK_PF_CLK_DIV_DELAY   -   Actel:SgCore:PF_CLK_DIV_DELAY:1.1.101
ides_43_PF_CLK_DIV_RXCLK_PF_CLK_DIV_DELAY PF_CLK_DIV_RXCLK(
        // Inputs
        .CLK_IN            ( HS_IO_CLK_CASCADED_Y ),
        // Outputs
        .CLK_OUT_HS_IO_CLK ( PF_CLK_DIV_RXCLK_CLK_OUT_HS_IO_CLK ),
        .CLK_DIV_OUT       (  ) 
        );

//--------ides_32_PF_IOD_RX_PF_IOD   -   Actel:SgCore:PF_IOD:1.0.214
ides_43_PF_IOD_RX_PF_IOD PF_IOD_RX(
        // Inputs
        .ARST_N                     ( PF_LANECTRL_0_ARST_N ),
        .RX_SYNC_RST                ( PF_LANECTRL_0_RX_SYNC_RST ),
        .TX_SYNC_RST                ( PF_LANECTRL_0_TX_SYNC_RST ),
        .HS_IO_CLK                  ( HS_IO_CLK_FIFO_Y ),
        .RX_DQS_90                  ( PF_LANECTRL_0_RX_DQS_90 ),
        .FIFO_WR_PTR                ( PF_LANECTRL_0_FIFO_WR_PTR ),
        .FIFO_RD_PTR                ( PF_LANECTRL_0_FIFO_RD_PTR ),
        .EYE_MONITOR_LANE_WIDTH     ( EYE_MONITOR_LANE_WIDTH_const_net_0 ), // tied to 3'h0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_0  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_1  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_2  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_3  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_4  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_5  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_6  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_7  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_8  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_9  ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_10 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_11 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_12 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_13 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_14 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_15 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_16 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_17 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_18 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_19 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_20 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_21 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_22 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_23 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_24 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_25 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_26 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_27 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_28 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_29 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_30 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_31 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_32 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_33 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_34 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_35 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_36 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_37 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_38 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_39 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_40 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_41 ( GND_net ), // tied to 1'b0 from definition
        .EYE_MONITOR_CLEAR_FLAGS_42 ( GND_net ), // tied to 1'b0 from definition

        .FAB_CLK                    ( RX_CLK_G_net_0 ),
        .PAD_I                      ( RXD ),
        .PAD_I_N                    ( RXD_N ),
        .ODT_EN_0                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_1                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_2                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_3                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_4                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_5                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_6                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_7                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_8                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_9                   ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_10                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_11                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_12                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_13                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_14                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_15                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_16                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_17                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_18                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_19                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_20                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_21                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_22                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_23                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_24                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_25                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_26                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_27                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_28                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_29                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_30                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_31                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_32                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_33                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_34                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_35                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_36                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_37                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_38                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_39                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_40                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_41                  ( GND_net ), // tied to 1'b0 from definition
        .ODT_EN_42                  ( GND_net ), // tied to 1'b0 from definition
        // Outputs
        .EYE_MONITOR_EARLY_0        (  ),
        .EYE_MONITOR_EARLY_1        (  ),
        .EYE_MONITOR_EARLY_2        (  ),
        .EYE_MONITOR_EARLY_3        (  ),
        .EYE_MONITOR_EARLY_4        (  ),
        .EYE_MONITOR_EARLY_5        (  ),
        .EYE_MONITOR_EARLY_6        (  ),
        .EYE_MONITOR_EARLY_7        (  ),
        .EYE_MONITOR_EARLY_8        (  ),
        .EYE_MONITOR_EARLY_9        (  ),
        .EYE_MONITOR_EARLY_10       (  ),
        .EYE_MONITOR_EARLY_11       (  ),
        .EYE_MONITOR_EARLY_12       (  ),
        .EYE_MONITOR_EARLY_13       (  ),
        .EYE_MONITOR_EARLY_14       (  ),
        .EYE_MONITOR_EARLY_15       (  ),
        .EYE_MONITOR_EARLY_16       (  ),
        .EYE_MONITOR_EARLY_17       (  ),
        .EYE_MONITOR_EARLY_18       (  ),
        .EYE_MONITOR_EARLY_19       (  ),
        .EYE_MONITOR_EARLY_20       (  ),
        .EYE_MONITOR_EARLY_21       (  ),
        .EYE_MONITOR_EARLY_22       (  ),
        .EYE_MONITOR_EARLY_23       (  ),
        .EYE_MONITOR_EARLY_24       (  ),
        .EYE_MONITOR_EARLY_25       (  ),
        .EYE_MONITOR_EARLY_26       (  ),
        .EYE_MONITOR_EARLY_27       (  ),
        .EYE_MONITOR_EARLY_28       (  ),
        .EYE_MONITOR_EARLY_29       (  ),
        .EYE_MONITOR_EARLY_30       (  ),
        .EYE_MONITOR_EARLY_31       (  ),
        .EYE_MONITOR_EARLY_32       (  ),
        .EYE_MONITOR_EARLY_33       (  ),
        .EYE_MONITOR_EARLY_34       (  ),
        .EYE_MONITOR_EARLY_35       (  ),
        .EYE_MONITOR_EARLY_36       (  ),
        .EYE_MONITOR_EARLY_37       (  ),
        .EYE_MONITOR_EARLY_38       (  ),
        .EYE_MONITOR_EARLY_39       (  ),
        .EYE_MONITOR_EARLY_40       (  ),
        .EYE_MONITOR_EARLY_41       (  ),
        .EYE_MONITOR_EARLY_42       (  ),

        .EYE_MONITOR_LATE_0         (  ),
        .EYE_MONITOR_LATE_1         (  ),
        .EYE_MONITOR_LATE_2         (  ),
        .EYE_MONITOR_LATE_3         (  ),
        .EYE_MONITOR_LATE_4         (  ),
        .EYE_MONITOR_LATE_5         (  ),
        .EYE_MONITOR_LATE_6         (  ),
        .EYE_MONITOR_LATE_7         (  ),
        .EYE_MONITOR_LATE_8         (  ),
        .EYE_MONITOR_LATE_9         (  ),
        .EYE_MONITOR_LATE_10        (  ),
        .EYE_MONITOR_LATE_11        (  ),
        .EYE_MONITOR_LATE_12        (  ),
        .EYE_MONITOR_LATE_13        (  ),
        .EYE_MONITOR_LATE_14        (  ),
        .EYE_MONITOR_LATE_15        (  ),
        .EYE_MONITOR_LATE_16        (  ),
        .EYE_MONITOR_LATE_17        (  ),
        .EYE_MONITOR_LATE_18        (  ),
        .EYE_MONITOR_LATE_19        (  ),
        .EYE_MONITOR_LATE_20        (  ),
        .EYE_MONITOR_LATE_21        (  ),
        .EYE_MONITOR_LATE_22        (  ),
        .EYE_MONITOR_LATE_23        (  ),
        .EYE_MONITOR_LATE_24        (  ),
        .EYE_MONITOR_LATE_25        (  ),
        .EYE_MONITOR_LATE_26        (  ),
        .EYE_MONITOR_LATE_27        (  ),
        .EYE_MONITOR_LATE_28        (  ),
        .EYE_MONITOR_LATE_29        (  ),
        .EYE_MONITOR_LATE_30        (  ),
        .EYE_MONITOR_LATE_31        (  ),
        .EYE_MONITOR_LATE_32        (  ),
        .EYE_MONITOR_LATE_33        (  ),
        .EYE_MONITOR_LATE_34        (  ),
        .EYE_MONITOR_LATE_35        (  ),
        .EYE_MONITOR_LATE_36        (  ),
        .EYE_MONITOR_LATE_37        (  ),
        .EYE_MONITOR_LATE_38        (  ),
        .EYE_MONITOR_LATE_39        (  ),
        .EYE_MONITOR_LATE_40        (  ),
        .EYE_MONITOR_LATE_41        (  ),
        .EYE_MONITOR_LATE_42        (  ),

        .RX_DATA_0                  ( L0_RXD_DATA_net_0 ),
        .RX_DATA_1                  ( L1_RXD_DATA_net_0 ),
        .RX_DATA_2                  ( L2_RXD_DATA_net_0 ),
        .RX_DATA_3                  ( L3_RXD_DATA_net_0 ),
        .RX_DATA_4                  ( L4_RXD_DATA_net_0 ),
        .RX_DATA_5                  ( L5_RXD_DATA_net_0 ),
        .RX_DATA_6                  ( L6_RXD_DATA_net_0 ),
        .RX_DATA_7                  ( L7_RXD_DATA_net_0 ),
        .RX_DATA_8                  ( L8_RXD_DATA_net_0 ),
        .RX_DATA_9                  ( L9_RXD_DATA_net_0 ),
        .RX_DATA_10                 ( L10_RXD_DATA_net_0 ),
        .RX_DATA_11                 ( L11_RXD_DATA_net_0 ),
        .RX_DATA_12                 ( L12_RXD_DATA_net_0 ),
        .RX_DATA_13                 ( L13_RXD_DATA_net_0 ),
        .RX_DATA_14                 ( L14_RXD_DATA_net_0 ),
        .RX_DATA_15                 ( L15_RXD_DATA_net_0 ),
        .RX_DATA_16                 ( L16_RXD_DATA_net_0 ),
        .RX_DATA_17                 ( L17_RXD_DATA_net_0 ),
        .RX_DATA_18                 ( L18_RXD_DATA_net_0 ),
        .RX_DATA_19                 ( L19_RXD_DATA_net_0 ),
        .RX_DATA_20                 ( L20_RXD_DATA_net_0 ),
        .RX_DATA_21                 ( L21_RXD_DATA_net_0 ),
        .RX_DATA_22                 ( L22_RXD_DATA_net_0 ),
        .RX_DATA_23                 ( L23_RXD_DATA_net_0 ),
        .RX_DATA_24                 ( L24_RXD_DATA_net_0 ),
        .RX_DATA_25                 ( L25_RXD_DATA_net_0 ),
        .RX_DATA_26                 ( L26_RXD_DATA_net_0 ),
        .RX_DATA_27                 ( L27_RXD_DATA_net_0 ),
        .RX_DATA_28                 ( L28_RXD_DATA_net_0 ),
        .RX_DATA_29                 ( L29_RXD_DATA_net_0 ),
        .RX_DATA_30                 ( L30_RXD_DATA_net_0 ),
        .RX_DATA_31                 ( L31_RXD_DATA_net_0 ),
        .RX_DATA_32                 ( L32_RXD_DATA_net_0 ),
        .RX_DATA_33                 ( L33_RXD_DATA_net_0 ),
        .RX_DATA_34                 ( L34_RXD_DATA_net_0 ),
        .RX_DATA_35                 ( L35_RXD_DATA_net_0 ),
        .RX_DATA_36                 ( L36_RXD_DATA_net_0 ),
        .RX_DATA_37                 ( L37_RXD_DATA_net_0 ),
        .RX_DATA_38                 ( L38_RXD_DATA_net_0 ),
        .RX_DATA_39                 ( L39_RXD_DATA_net_0 ),
        .RX_DATA_40                 ( L40_RXD_DATA_net_0 ),
        .RX_DATA_41                 ( L41_RXD_DATA_net_0 ),
        .RX_DATA_42                 ( L42_RXD_DATA_net_0 )
        );

//--------ides_46_PF_LANECTRL_0_PF_LANECTRL   -   Actel:SgCore:PF_LANECTRL:1.0.204
ides_43_PF_LANECTRL_0_PF_LANECTRL PF_LANECTRL_0(
        // Inputs
        .HS_IO_CLK                  ( HS_IO_CLK_net_0 ),
        .DLL_CODE                   ( DLL_CODE_const_net_0 ), // tied to 8'h00 from definition
        .FAB_CLK                    ( RX_CLK_G_net_0 ),
        .RESET                      ( RESET_IN_POST_INV0_0 ),
        .DELAY_LINE_SEL             ( GND_net ), // tied to 1'b0 from definition
        .DELAY_LINE_LOAD            ( GND_net ), // tied to 1'b0 from definition
        .DELAY_LINE_DIRECTION       ( GND_net ), // tied to 1'b0 from definition
        .DELAY_LINE_MOVE            ( GND_net ), // tied to 1'b0 from definition
        .HS_IO_CLK_PAUSE            ( HS_IO_CLK_PAUSE ),
        .EYE_MONITOR_WIDTH_IN       ( EYE_MONITOR_WIDTH_IN_const_net_0 ), // tied to 3'h0 from definition
        // Outputs
        .EYE_MONITOR_WIDTH_OUT      (  ),
        .RX_DQS_90                  ( PF_LANECTRL_0_RX_DQS_90 ),
        .TX_DQS                     (  ),
        .TX_DQS_270                 (  ),
        .FIFO_WR_PTR                ( PF_LANECTRL_0_FIFO_WR_PTR ),
        .FIFO_RD_PTR                ( PF_LANECTRL_0_FIFO_RD_PTR ),
        .ARST_N                     ( PF_LANECTRL_0_ARST_N ),
        .RX_SYNC_RST                ( PF_LANECTRL_0_RX_SYNC_RST ),
        .TX_SYNC_RST                ( PF_LANECTRL_0_TX_SYNC_RST ),
        .RX_DELAY_LINE_OUT_OF_RANGE (  ),
        .TX_DELAY_LINE_OUT_OF_RANGE (  ),
        .A_OUT_RST_N                (  ) 
        );


endmodule
