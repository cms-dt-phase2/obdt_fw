library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library polarfire;
use polarfire.all;

use work.obdt_pkg.all;

entity tdc is
  PORT (
    -- TDC inputs
    data_in_from_pins_p : IN std_logic_vector(TDC_N-1 downto 0);
    data_in_from_pins_n : IN std_logic_vector(TDC_N-1 downto 0);
    -- Other inputs
    bx_cnt              : in std_logic_vector(11 downto 0);
    deadtime            : in std_logic_vector(3 downto 0);
    -- clocks, resets
    rst                 : IN STD_LOGIC;
    clk40               : IN std_logic;
    clk120              : IN std_logic;
    clk_bank0           : IN std_logic;
    clk_bank1           : IN std_logic;
    clk_bank2           : IN std_logic;
    clk_bank4           : IN std_logic;
    clk_bank5           : IN std_logic;
    clk_bank6           : IN std_logic;
    clk_bank7           : IN std_logic;
    -- TDC output
    tdc_enc             : OUT tdc_enc_a;
    enable_mask         : IN std_logic_vector(TDC_N-1 downto 0)
  );
end tdc;

architecture Behavioral of tdc is

  type des_out_t    is array (TDC_N-1 downto 0) of std_logic_vector(9 downto 0);
  signal des_out  : des_out_t   :=(others=>(others=>'0'));
  signal tictoc20 : std_logic;
  signal tdc_enc_120 : tdc_enc_a ;
  
  signal rst_io_auto : std_logic :='0';
  signal locked : std_logic :='0';
  signal cnt : integer range 0 to 15 :=0;

component ides_46
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(45 downto 0);
    RXD_N           : in  std_logic_vector(45 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    L19_RXD_DATA    : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L20_RXD_DATA    : out std_logic_vector(9 downto 0);
    L21_RXD_DATA    : out std_logic_vector(9 downto 0);
    L22_RXD_DATA    : out std_logic_vector(9 downto 0);
    L23_RXD_DATA    : out std_logic_vector(9 downto 0);
    L24_RXD_DATA    : out std_logic_vector(9 downto 0);
    L25_RXD_DATA    : out std_logic_vector(9 downto 0);
    L26_RXD_DATA    : out std_logic_vector(9 downto 0);
    L27_RXD_DATA    : out std_logic_vector(9 downto 0);
    L28_RXD_DATA    : out std_logic_vector(9 downto 0);
    L29_RXD_DATA    : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L30_RXD_DATA    : out std_logic_vector(9 downto 0);
    L31_RXD_DATA    : out std_logic_vector(9 downto 0);
    L32_RXD_DATA    : out std_logic_vector(9 downto 0);
    L33_RXD_DATA    : out std_logic_vector(9 downto 0);
    L34_RXD_DATA    : out std_logic_vector(9 downto 0);
    L35_RXD_DATA    : out std_logic_vector(9 downto 0);
    L36_RXD_DATA    : out std_logic_vector(9 downto 0);
    L37_RXD_DATA    : out std_logic_vector(9 downto 0);
    L38_RXD_DATA    : out std_logic_vector(9 downto 0);
    L39_RXD_DATA    : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L40_RXD_DATA    : out std_logic_vector(9 downto 0);
    L41_RXD_DATA    : out std_logic_vector(9 downto 0);
    L42_RXD_DATA    : out std_logic_vector(9 downto 0);
    L43_RXD_DATA    : out std_logic_vector(9 downto 0);
    L44_RXD_DATA    : out std_logic_vector(9 downto 0);
    L45_RXD_DATA    : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;

component ides_43
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(42 downto 0);
    RXD_N           : in  std_logic_vector(42 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    L19_RXD_DATA    : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L20_RXD_DATA    : out std_logic_vector(9 downto 0);
    L21_RXD_DATA    : out std_logic_vector(9 downto 0);
    L22_RXD_DATA    : out std_logic_vector(9 downto 0);
    L23_RXD_DATA    : out std_logic_vector(9 downto 0);
    L24_RXD_DATA    : out std_logic_vector(9 downto 0);
    L25_RXD_DATA    : out std_logic_vector(9 downto 0);
    L26_RXD_DATA    : out std_logic_vector(9 downto 0);
    L27_RXD_DATA    : out std_logic_vector(9 downto 0);
    L28_RXD_DATA    : out std_logic_vector(9 downto 0);
    L29_RXD_DATA    : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L30_RXD_DATA    : out std_logic_vector(9 downto 0);
    L31_RXD_DATA    : out std_logic_vector(9 downto 0);
    L32_RXD_DATA    : out std_logic_vector(9 downto 0);
    L33_RXD_DATA    : out std_logic_vector(9 downto 0);
    L34_RXD_DATA    : out std_logic_vector(9 downto 0);
    L35_RXD_DATA    : out std_logic_vector(9 downto 0);
    L36_RXD_DATA    : out std_logic_vector(9 downto 0);
    L37_RXD_DATA    : out std_logic_vector(9 downto 0);
    L38_RXD_DATA    : out std_logic_vector(9 downto 0);
    L39_RXD_DATA    : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L40_RXD_DATA    : out std_logic_vector(9 downto 0);
    L41_RXD_DATA    : out std_logic_vector(9 downto 0);
    L42_RXD_DATA    : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;

component ides_36
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(35 downto 0);
    RXD_N           : in  std_logic_vector(35 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    L19_RXD_DATA    : out std_logic_vector(9 downto 0);
    L20_RXD_DATA    : out std_logic_vector(9 downto 0);
    L21_RXD_DATA    : out std_logic_vector(9 downto 0);
    L22_RXD_DATA    : out std_logic_vector(9 downto 0);
    L23_RXD_DATA    : out std_logic_vector(9 downto 0);
    L24_RXD_DATA    : out std_logic_vector(9 downto 0);
    L25_RXD_DATA    : out std_logic_vector(9 downto 0);
    L26_RXD_DATA    : out std_logic_vector(9 downto 0);
    L27_RXD_DATA    : out std_logic_vector(9 downto 0);
    L28_RXD_DATA    : out std_logic_vector(9 downto 0);
    L29_RXD_DATA    : out std_logic_vector(9 downto 0);
    L30_RXD_DATA    : out std_logic_vector(9 downto 0);
    L31_RXD_DATA    : out std_logic_vector(9 downto 0);
    L32_RXD_DATA    : out std_logic_vector(9 downto 0);
    L33_RXD_DATA    : out std_logic_vector(9 downto 0);
    L34_RXD_DATA    : out std_logic_vector(9 downto 0);
    L35_RXD_DATA    : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;

component ides_24
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(23 downto 0);
    RXD_N           : in  std_logic_vector(23 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    L19_RXD_DATA    : out std_logic_vector(9 downto 0);
    L20_RXD_DATA    : out std_logic_vector(9 downto 0);
    L21_RXD_DATA    : out std_logic_vector(9 downto 0);
    L22_RXD_DATA    : out std_logic_vector(9 downto 0);
    L23_RXD_DATA    : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;

component ides_19
    -- Port list
    port(
    -- Inputs
    ARST_N          : in  std_logic;
    HS_IO_CLK_PAUSE : in  std_logic;
    RXD             : in  std_logic_vector(18 downto 0);
    RXD_N           : in  std_logic_vector(18 downto 0);
    RX_CLK          : in  std_logic;
    -- Outputs
    L0_RXD_DATA     : out std_logic_vector(9 downto 0);
    L1_RXD_DATA     : out std_logic_vector(9 downto 0);
    L2_RXD_DATA     : out std_logic_vector(9 downto 0);
    L3_RXD_DATA     : out std_logic_vector(9 downto 0);
    L4_RXD_DATA     : out std_logic_vector(9 downto 0);
    L5_RXD_DATA     : out std_logic_vector(9 downto 0);
    L6_RXD_DATA     : out std_logic_vector(9 downto 0);
    L7_RXD_DATA     : out std_logic_vector(9 downto 0);
    L8_RXD_DATA     : out std_logic_vector(9 downto 0);
    L9_RXD_DATA     : out std_logic_vector(9 downto 0);
    L10_RXD_DATA    : out std_logic_vector(9 downto 0);
    L11_RXD_DATA    : out std_logic_vector(9 downto 0);
    L12_RXD_DATA    : out std_logic_vector(9 downto 0);
    L13_RXD_DATA    : out std_logic_vector(9 downto 0);
    L14_RXD_DATA    : out std_logic_vector(9 downto 0);
    L15_RXD_DATA    : out std_logic_vector(9 downto 0);
    L16_RXD_DATA    : out std_logic_vector(9 downto 0);
    L17_RXD_DATA    : out std_logic_vector(9 downto 0);
    L18_RXD_DATA    : out std_logic_vector(9 downto 0);
    RX_CLK_G        : out std_logic
);
end component;

begin


--------- Reset SerDes -------------------------------------------------------------


--
-- fix "locked" signal it is not connected
--

--rst_des_p: process(rst, clk_in)
rst_des_p: process(rst, clk120)
begin
	if rst = '1' then
		rst_io_auto <= '0';
		cnt <= 0;
	elsif (clk120='1' and clk120'event) then
		if locked = '1' and cnt < 10 then
			rst_io_auto <= '1';
			cnt <= cnt + 1;
		else
			rst_io_auto <= '0';
			cnt <= cnt;
		end if;
	end if;
end process;

------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
bank0 : ides_36
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(35 downto 0),
    RXD_N  => data_in_from_pins_n(35 downto 0),
    RX_CLK          => clk_bank0,
    -- Outputs
    L0_RXD_DATA     => des_out(0),
    L1_RXD_DATA     => des_out(1),
    L2_RXD_DATA     => des_out(2),
    L3_RXD_DATA     => des_out(3),
    L4_RXD_DATA     => des_out(4),
    L5_RXD_DATA     => des_out(5),
    L6_RXD_DATA     => des_out(6),
    L7_RXD_DATA     => des_out(7),
    L8_RXD_DATA     => des_out(8),
    L9_RXD_DATA     => des_out(9),
    L10_RXD_DATA    => des_out(10),
    L11_RXD_DATA    => des_out(11),
    L12_RXD_DATA    => des_out(12),
    L13_RXD_DATA    => des_out(13),
    L14_RXD_DATA    => des_out(14),
    L15_RXD_DATA    => des_out(15),
    L16_RXD_DATA    => des_out(16),
    L17_RXD_DATA    => des_out(17),
    L18_RXD_DATA    => des_out(18),
    L19_RXD_DATA    => des_out(19),
    L20_RXD_DATA    => des_out(20),
    L21_RXD_DATA    => des_out(21),
    L22_RXD_DATA    => des_out(22),
    L23_RXD_DATA    => des_out(23),
    L24_RXD_DATA    => des_out(24),
    L25_RXD_DATA    => des_out(25),
    L26_RXD_DATA    => des_out(26),
    L27_RXD_DATA    => des_out(27),
    L28_RXD_DATA    => des_out(28),
    L29_RXD_DATA    => des_out(29),
    L30_RXD_DATA    => des_out(30),
    L31_RXD_DATA    => des_out(31),
    L32_RXD_DATA    => des_out(32),
    L33_RXD_DATA    => des_out(33),
    L34_RXD_DATA    => des_out(34),
    L35_RXD_DATA    => des_out(35),
    RX_CLK_G        => open
);  

bank1 : ides_19
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(54 downto 36),
    RXD_N  => data_in_from_pins_n(54 downto 36),
    RX_CLK          => clk_bank1,
    -- Outputs
    L0_RXD_DATA     => des_out(36),
    L1_RXD_DATA     => des_out(37),
    L2_RXD_DATA     => des_out(38),
    L3_RXD_DATA     => des_out(39),
    L4_RXD_DATA     => des_out(40),
    L5_RXD_DATA     => des_out(41),
    L6_RXD_DATA     => des_out(42),
    L7_RXD_DATA     => des_out(43),
    L8_RXD_DATA     => des_out(44),
    L9_RXD_DATA     => des_out(45),
    L10_RXD_DATA    => des_out(46),
    L11_RXD_DATA    => des_out(47),
    L12_RXD_DATA    => des_out(48),
    L13_RXD_DATA    => des_out(49),
    L14_RXD_DATA    => des_out(50),
    L15_RXD_DATA    => des_out(51),
    L16_RXD_DATA    => des_out(52),
    L17_RXD_DATA    => des_out(53),
    L18_RXD_DATA    => des_out(54),
    RX_CLK_G        => open
);  

bank2: ides_43
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(97 downto 55),
    RXD_N  => data_in_from_pins_n(97 downto 55),
    RX_CLK          => clk_bank2,
    -- Outputs
    L0_RXD_DATA     => des_out(55),
    L1_RXD_DATA     => des_out(56),
    L2_RXD_DATA     => des_out(57),
    L3_RXD_DATA     => des_out(58),
    L4_RXD_DATA     => des_out(59),
    L5_RXD_DATA     => des_out(60),
    L6_RXD_DATA     => des_out(61),
    L7_RXD_DATA     => des_out(62),
    L8_RXD_DATA     => des_out(63),
    L9_RXD_DATA     => des_out(64),
    L10_RXD_DATA    => des_out(65),
    L11_RXD_DATA    => des_out(66),
    L12_RXD_DATA    => des_out(67),
    L13_RXD_DATA    => des_out(68),
    L14_RXD_DATA    => des_out(69),
    L15_RXD_DATA    => des_out(70),
    L16_RXD_DATA    => des_out(71),
    L17_RXD_DATA    => des_out(72),
    L18_RXD_DATA    => des_out(73),
    L19_RXD_DATA    => des_out(74),
    L20_RXD_DATA    => des_out(75),
    L21_RXD_DATA    => des_out(76),
    L22_RXD_DATA    => des_out(77),
    L23_RXD_DATA    => des_out(78),
    L24_RXD_DATA    => des_out(79),
    L25_RXD_DATA    => des_out(80),
    L26_RXD_DATA    => des_out(81),
    L27_RXD_DATA    => des_out(82),
    L28_RXD_DATA    => des_out(83),
    L29_RXD_DATA    => des_out(84),
    L30_RXD_DATA    => des_out(85),
    L31_RXD_DATA    => des_out(86),
    L32_RXD_DATA    => des_out(87),
    L33_RXD_DATA    => des_out(88),
    L34_RXD_DATA    => des_out(89),
    L35_RXD_DATA    => des_out(90),
    L36_RXD_DATA    => des_out(91),
    L37_RXD_DATA    => des_out(92),
    L38_RXD_DATA    => des_out(93),
    L39_RXD_DATA    => des_out(94),
    L40_RXD_DATA    => des_out(95),
    L41_RXD_DATA    => des_out(96),
    L42_RXD_DATA    => des_out(97),
    RX_CLK_G        => open
);  

bank4: ides_46
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(143 downto 98),
    RXD_N  => data_in_from_pins_n(143 downto 98),
    RX_CLK          => clk_bank4,
    -- Outputs
    L0_RXD_DATA     => des_out(98),
    L1_RXD_DATA     => des_out(99),
    L2_RXD_DATA     => des_out(100),
    L3_RXD_DATA     => des_out(101),
    L4_RXD_DATA     => des_out(102),
    L5_RXD_DATA     => des_out(103),
    L6_RXD_DATA     => des_out(104),
    L7_RXD_DATA     => des_out(105),
    L8_RXD_DATA     => des_out(106),
    L9_RXD_DATA     => des_out(107),
    L10_RXD_DATA    => des_out(108),
    L11_RXD_DATA    => des_out(109),
    L12_RXD_DATA    => des_out(110),
    L13_RXD_DATA    => des_out(111),
    L14_RXD_DATA    => des_out(112),
    L15_RXD_DATA    => des_out(113),
    L16_RXD_DATA    => des_out(114),
    L17_RXD_DATA    => des_out(115),
    L18_RXD_DATA    => des_out(116),
    L19_RXD_DATA    => des_out(117),
    L20_RXD_DATA    => des_out(118),
    L21_RXD_DATA    => des_out(119),
    L22_RXD_DATA    => des_out(120),
    L23_RXD_DATA    => des_out(121),
    L24_RXD_DATA    => des_out(122),
    L25_RXD_DATA    => des_out(123),
    L26_RXD_DATA    => des_out(124),
    L27_RXD_DATA    => des_out(125),
    L28_RXD_DATA    => des_out(126),
    L29_RXD_DATA    => des_out(127),
    L30_RXD_DATA    => des_out(128),
    L31_RXD_DATA    => des_out(129),
    L32_RXD_DATA    => des_out(130),
    L33_RXD_DATA    => des_out(131),
    L34_RXD_DATA    => des_out(132),
    L35_RXD_DATA    => des_out(133),
    L36_RXD_DATA    => des_out(134),
    L37_RXD_DATA    => des_out(135),
    L38_RXD_DATA    => des_out(136),
    L39_RXD_DATA    => des_out(137),
    L40_RXD_DATA    => des_out(138),
    L41_RXD_DATA    => des_out(139),
    L42_RXD_DATA    => des_out(140),
    L43_RXD_DATA    => des_out(141),
    L44_RXD_DATA    => des_out(142),
    L45_RXD_DATA    => des_out(143),
    RX_CLK_G        => open
);  

bank5: ides_24
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(167 downto 144),
    RXD_N  => data_in_from_pins_n(167 downto 144),
    RX_CLK          => clk_bank5,
    -- Outputs
    L0_RXD_DATA     => des_out(144),
    L1_RXD_DATA     => des_out(145),
    L2_RXD_DATA     => des_out(146),
    L3_RXD_DATA     => des_out(147),
    L4_RXD_DATA     => des_out(148),
    L5_RXD_DATA     => des_out(149),
    L6_RXD_DATA     => des_out(150),
    L7_RXD_DATA     => des_out(151),
    L8_RXD_DATA     => des_out(152),
    L9_RXD_DATA     => des_out(153),
    L10_RXD_DATA    => des_out(154),
    L11_RXD_DATA    => des_out(155),
    L12_RXD_DATA    => des_out(156),
    L13_RXD_DATA    => des_out(157),
    L14_RXD_DATA    => des_out(158),
    L15_RXD_DATA    => des_out(159),
    L16_RXD_DATA    => des_out(160),
    L17_RXD_DATA    => des_out(161),
    L18_RXD_DATA    => des_out(162),
    L19_RXD_DATA    => des_out(163),
    L20_RXD_DATA    => des_out(164),
    L21_RXD_DATA    => des_out(165),
    L22_RXD_DATA    => des_out(166),
    L23_RXD_DATA    => des_out(167),

    RX_CLK_G        => open
);  

bank6: ides_36
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(203 downto 168),
    RXD_N  => data_in_from_pins_n(203 downto 168),
    RX_CLK          => clk_bank6,
    -- Outputs
    L0_RXD_DATA     => des_out(168),
    L1_RXD_DATA     => des_out(169),
    L2_RXD_DATA     => des_out(170),
    L3_RXD_DATA     => des_out(171),
    L4_RXD_DATA     => des_out(172),
    L5_RXD_DATA     => des_out(173),
    L6_RXD_DATA     => des_out(174),
    L7_RXD_DATA     => des_out(175),
    L8_RXD_DATA     => des_out(176),
    L9_RXD_DATA     => des_out(177),
    L10_RXD_DATA    => des_out(178),
    L11_RXD_DATA    => des_out(179),
    L12_RXD_DATA    => des_out(180),
    L13_RXD_DATA    => des_out(181),
    L14_RXD_DATA    => des_out(182),
    L15_RXD_DATA    => des_out(183),
    L16_RXD_DATA    => des_out(184),
    L17_RXD_DATA    => des_out(185),
    L18_RXD_DATA    => des_out(186),
    L19_RXD_DATA    => des_out(187),
    L20_RXD_DATA    => des_out(188),
    L21_RXD_DATA    => des_out(189),
    L22_RXD_DATA    => des_out(190),
    L23_RXD_DATA    => des_out(191),
    L24_RXD_DATA    => des_out(192),
    L25_RXD_DATA    => des_out(193),
    L26_RXD_DATA    => des_out(194),
    L27_RXD_DATA    => des_out(195),
    L28_RXD_DATA    => des_out(196),
    L29_RXD_DATA    => des_out(197),
    L30_RXD_DATA    => des_out(198),
    L31_RXD_DATA    => des_out(199),
    L32_RXD_DATA    => des_out(200),
    L33_RXD_DATA    => des_out(201),
    L34_RXD_DATA    => des_out(202),
    L35_RXD_DATA    => des_out(203),

    RX_CLK_G        => open
);  

bank7: ides_36
   port map(
    -- Inputs
    ARST_N          => not rst_io_auto,
    HS_IO_CLK_PAUSE => '0',
    RXD  => data_in_from_pins_p(239 downto 204),
    RXD_N  => data_in_from_pins_n(239 downto 204),
    RX_CLK          => clk_bank7,
    -- Outputs
    L0_RXD_DATA     => des_out(204),
    L1_RXD_DATA     => des_out(205),
    L2_RXD_DATA     => des_out(206),
    L3_RXD_DATA     => des_out(207),
    L4_RXD_DATA     => des_out(208),
    L5_RXD_DATA     => des_out(209),
    L6_RXD_DATA     => des_out(210),
    L7_RXD_DATA     => des_out(211),
    L8_RXD_DATA     => des_out(212),
    L9_RXD_DATA     => des_out(213),
    L10_RXD_DATA    => des_out(214),
    L11_RXD_DATA    => des_out(215),
    L12_RXD_DATA    => des_out(216),
    L13_RXD_DATA    => des_out(217),
    L14_RXD_DATA    => des_out(218),
    L15_RXD_DATA    => des_out(219),
    L16_RXD_DATA    => des_out(220),
    L17_RXD_DATA    => des_out(221),
    L18_RXD_DATA    => des_out(222),
    L19_RXD_DATA    => des_out(223),
    L20_RXD_DATA    => des_out(224),
    L21_RXD_DATA    => des_out(225),
    L22_RXD_DATA    => des_out(226),
    L23_RXD_DATA    => des_out(227),
    L24_RXD_DATA    => des_out(228),
    L25_RXD_DATA    => des_out(229),
    L26_RXD_DATA    => des_out(230),
    L27_RXD_DATA    => des_out(231),
    L28_RXD_DATA    => des_out(232),
    L29_RXD_DATA    => des_out(233),
    L30_RXD_DATA    => des_out(234),
    L31_RXD_DATA    => des_out(235),
    L32_RXD_DATA    => des_out(236),
    L33_RXD_DATA    => des_out(237),
    L34_RXD_DATA    => des_out(238),
    L35_RXD_DATA    => des_out(239),
    RX_CLK_G        => open
);  

tictoc20 <= not tictoc20 when rising_edge(clk40);

process(clk120, rst)
  type des_out_11_t    is array (TDC_N-1 downto 0) of std_logic_vector(10 downto 0);
  variable des_out_11  : des_out_11_t;
  type deadtime_ctrs_t is array(TDC_N-1 downto 0) of unsigned(deadtime'range);
  variable deadtime_ctrs : deadtime_ctrs_t;
  variable valid : std_logic_vector(TDC_N-1 downto 0);
  type tdc_enc_v_t is array (TDC_N-1 downto 0) of unsigned(4 downto 0);
  variable tdc_enc_v : tdc_enc_v_t :=(others=>0);
  variable tictoc20_pipe : std_logic_vector(3 downto 0);
begin
  if rst='1' then
    des_out_11    := (others => (others => '1'));
    deadtime_ctrs := (others => (others => '0'));
    valid         := (others => '0');
    tdc_enc_v    := (others => (others => '0'));
    tdc_enc_120 <= (others => (others=>'0'));
    tictoc20_pipe := (others => tictoc20);
  elsif rising_edge(clk120) then
    tictoc20_pipe := tictoc20_pipe(tictoc20_pipe'high-1 downto 0) & tictoc20;
    
    for i in TDC_N-1 downto 0 loop
      des_out_11(i) := des_out(i) & des_out_11(i)(10);
      
      if valid(i)= '0' then
        tdc_enc_v(i) := (others => '0');
        for j in 0 to 9 loop
          if des_out_11(i)(10-j downto 9-j) = "10" then
            tdc_enc_v(i) := to_unsigned(30-j,5);
            valid(i) := '1';
          end if;
        end loop;
      else
        tdc_enc_v(i) := tdc_enc_v(i) - 10;
      end if;
      
      if tictoc20_pipe(1) /= tictoc20_pipe(2) then
        if valid(i) = '1' and enable_mask(i) = '1' and deadtime_ctrs(i) = (deadtime'range => '0') then
          tdc_enc_120(i) <= std_logic_vector(tdc_enc_v(i));
          deadtime_ctrs(i) := unsigned(deadtime);
        else
          tdc_enc_120(i) <= (others=>'0');
          if deadtime_ctrs(i) > 0 then
            deadtime_ctrs(i) := deadtime_ctrs(i) - 1;
          end if;
        end if;
        valid(i) := '0';
      end if;
    end loop;
  end if;
end process;

tdc_enc <= tdc_enc_120 when rising_edge(clk40);

end Behavioral;