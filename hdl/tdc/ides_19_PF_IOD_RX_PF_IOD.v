`timescale 1 ns/100 ps
// Version: v12.0 12.500.0.22


module ides_19_PF_IOD_RX_PF_IOD(
       ARST_N,
       RX_SYNC_RST,
       TX_SYNC_RST,
       HS_IO_CLK,
       RX_DQS_90,
       FIFO_WR_PTR,
       FIFO_RD_PTR,
       EYE_MONITOR_LANE_WIDTH,
       EYE_MONITOR_CLEAR_FLAGS_0,
       EYE_MONITOR_CLEAR_FLAGS_1,
       EYE_MONITOR_CLEAR_FLAGS_2,
       EYE_MONITOR_CLEAR_FLAGS_3,
       EYE_MONITOR_CLEAR_FLAGS_4,
       EYE_MONITOR_CLEAR_FLAGS_5,
       EYE_MONITOR_CLEAR_FLAGS_6,
       EYE_MONITOR_CLEAR_FLAGS_7,
       EYE_MONITOR_CLEAR_FLAGS_8,
       EYE_MONITOR_CLEAR_FLAGS_9,
       EYE_MONITOR_CLEAR_FLAGS_10,
       EYE_MONITOR_CLEAR_FLAGS_11,
       EYE_MONITOR_CLEAR_FLAGS_12,
       EYE_MONITOR_CLEAR_FLAGS_13,
       EYE_MONITOR_CLEAR_FLAGS_14,
       EYE_MONITOR_CLEAR_FLAGS_15,
       EYE_MONITOR_CLEAR_FLAGS_16,
       EYE_MONITOR_CLEAR_FLAGS_17,
       EYE_MONITOR_CLEAR_FLAGS_18,
       FAB_CLK,
       EYE_MONITOR_EARLY_0,
       EYE_MONITOR_EARLY_1,
       EYE_MONITOR_EARLY_2,
       EYE_MONITOR_EARLY_3,
       EYE_MONITOR_EARLY_4,
       EYE_MONITOR_EARLY_5,
       EYE_MONITOR_EARLY_6,
       EYE_MONITOR_EARLY_7,
       EYE_MONITOR_EARLY_8,
       EYE_MONITOR_EARLY_9,
       EYE_MONITOR_EARLY_10,
       EYE_MONITOR_EARLY_11,
       EYE_MONITOR_EARLY_12,
       EYE_MONITOR_EARLY_13,
       EYE_MONITOR_EARLY_14,
       EYE_MONITOR_EARLY_15,
       EYE_MONITOR_EARLY_16,
       EYE_MONITOR_EARLY_17,
       EYE_MONITOR_EARLY_18,
       EYE_MONITOR_LATE_0,
       EYE_MONITOR_LATE_1,
       EYE_MONITOR_LATE_2,
       EYE_MONITOR_LATE_3,
       EYE_MONITOR_LATE_4,
       EYE_MONITOR_LATE_5,
       EYE_MONITOR_LATE_6,
       EYE_MONITOR_LATE_7,
       EYE_MONITOR_LATE_8,
       EYE_MONITOR_LATE_9,
       EYE_MONITOR_LATE_10,
       EYE_MONITOR_LATE_11,
       EYE_MONITOR_LATE_12,
       EYE_MONITOR_LATE_13,
       EYE_MONITOR_LATE_14,
       EYE_MONITOR_LATE_15,
       EYE_MONITOR_LATE_16,
       EYE_MONITOR_LATE_17,
       EYE_MONITOR_LATE_18,
       RX_DATA_0,
       RX_DATA_1,
       RX_DATA_2,
       RX_DATA_3,
       RX_DATA_4,
       RX_DATA_5,
       RX_DATA_6,
       RX_DATA_7,
       RX_DATA_8,
       RX_DATA_9,
       RX_DATA_10,
       RX_DATA_11,
       RX_DATA_12,
       RX_DATA_13,
       RX_DATA_14,
       RX_DATA_15,
       RX_DATA_16,
       RX_DATA_17,
       RX_DATA_18,
       PAD_I,
       PAD_I_N,
       ODT_EN_0,
       ODT_EN_1,
       ODT_EN_2,
       ODT_EN_3,
       ODT_EN_4,
       ODT_EN_5,
       ODT_EN_6,
       ODT_EN_7,
       ODT_EN_8,
       ODT_EN_9,
       ODT_EN_10,
       ODT_EN_11,
       ODT_EN_12,
       ODT_EN_13,
       ODT_EN_14,
       ODT_EN_15,
       ODT_EN_16,
       ODT_EN_17,
       ODT_EN_18
    );
input  ARST_N;
input  RX_SYNC_RST;
input  TX_SYNC_RST;
input  [0:0] HS_IO_CLK;
input  [0:0] RX_DQS_90;
input  [2:0] FIFO_WR_PTR;
input  [2:0] FIFO_RD_PTR;
input  [2:0] EYE_MONITOR_LANE_WIDTH;
input  EYE_MONITOR_CLEAR_FLAGS_0;
input  EYE_MONITOR_CLEAR_FLAGS_1;
input  EYE_MONITOR_CLEAR_FLAGS_2;
input  EYE_MONITOR_CLEAR_FLAGS_3;
input  EYE_MONITOR_CLEAR_FLAGS_4;
input  EYE_MONITOR_CLEAR_FLAGS_5;
input  EYE_MONITOR_CLEAR_FLAGS_6;
input  EYE_MONITOR_CLEAR_FLAGS_7;
input  EYE_MONITOR_CLEAR_FLAGS_8;
input  EYE_MONITOR_CLEAR_FLAGS_9;
input  EYE_MONITOR_CLEAR_FLAGS_10;
input  EYE_MONITOR_CLEAR_FLAGS_11;
input  EYE_MONITOR_CLEAR_FLAGS_12;
input  EYE_MONITOR_CLEAR_FLAGS_13;
input  EYE_MONITOR_CLEAR_FLAGS_14;
input  EYE_MONITOR_CLEAR_FLAGS_15;
input  EYE_MONITOR_CLEAR_FLAGS_16;
input  EYE_MONITOR_CLEAR_FLAGS_17;
input  EYE_MONITOR_CLEAR_FLAGS_18;
input  FAB_CLK;
output EYE_MONITOR_EARLY_0;
output EYE_MONITOR_EARLY_1;
output EYE_MONITOR_EARLY_2;
output EYE_MONITOR_EARLY_3;
output EYE_MONITOR_EARLY_4;
output EYE_MONITOR_EARLY_5;
output EYE_MONITOR_EARLY_6;
output EYE_MONITOR_EARLY_7;
output EYE_MONITOR_EARLY_8;
output EYE_MONITOR_EARLY_9;
output EYE_MONITOR_EARLY_10;
output EYE_MONITOR_EARLY_11;
output EYE_MONITOR_EARLY_12;
output EYE_MONITOR_EARLY_13;
output EYE_MONITOR_EARLY_14;
output EYE_MONITOR_EARLY_15;
output EYE_MONITOR_EARLY_16;
output EYE_MONITOR_EARLY_17;
output EYE_MONITOR_EARLY_18;
output EYE_MONITOR_LATE_0;
output EYE_MONITOR_LATE_1;
output EYE_MONITOR_LATE_2;
output EYE_MONITOR_LATE_3;
output EYE_MONITOR_LATE_4;
output EYE_MONITOR_LATE_5;
output EYE_MONITOR_LATE_6;
output EYE_MONITOR_LATE_7;
output EYE_MONITOR_LATE_8;
output EYE_MONITOR_LATE_9;
output EYE_MONITOR_LATE_10;
output EYE_MONITOR_LATE_11;
output EYE_MONITOR_LATE_12;
output EYE_MONITOR_LATE_13;
output EYE_MONITOR_LATE_14;
output EYE_MONITOR_LATE_15;
output EYE_MONITOR_LATE_16;
output EYE_MONITOR_LATE_17;
output EYE_MONITOR_LATE_18;
output [9:0] RX_DATA_0;
output [9:0] RX_DATA_1;
output [9:0] RX_DATA_2;
output [9:0] RX_DATA_3;
output [9:0] RX_DATA_4;
output [9:0] RX_DATA_5;
output [9:0] RX_DATA_6;
output [9:0] RX_DATA_7;
output [9:0] RX_DATA_8;
output [9:0] RX_DATA_9;
output [9:0] RX_DATA_10;
output [9:0] RX_DATA_11;
output [9:0] RX_DATA_12;
output [9:0] RX_DATA_13;
output [9:0] RX_DATA_14;
output [9:0] RX_DATA_15;
output [9:0] RX_DATA_16;
output [9:0] RX_DATA_17;
output [9:0] RX_DATA_18;
input  [18:0] PAD_I;
input  [18:0] PAD_I_N;
input  ODT_EN_0;
input  ODT_EN_1;
input  ODT_EN_2;
input  ODT_EN_3;
input  ODT_EN_4;
input  ODT_EN_5;
input  ODT_EN_6;
input  ODT_EN_7;
input  ODT_EN_8;
input  ODT_EN_9;
input  ODT_EN_10;
input  ODT_EN_11;
input  ODT_EN_12;
input  ODT_EN_13;
input  ODT_EN_14;
input  ODT_EN_15;
input  ODT_EN_16;
input  ODT_EN_17;
input  ODT_EN_18;

    wire GND_net, VCC_net, Y_I_INBUF_DIFF_0_net, Y_I_INBUF_DIFF_1_net, 
        Y_I_INBUF_DIFF_2_net, Y_I_INBUF_DIFF_3_net, 
        Y_I_INBUF_DIFF_4_net, Y_I_INBUF_DIFF_5_net, 
        Y_I_INBUF_DIFF_6_net, Y_I_INBUF_DIFF_7_net, 
        Y_I_INBUF_DIFF_8_net, Y_I_INBUF_DIFF_9_net, 
        Y_I_INBUF_DIFF_10_net, Y_I_INBUF_DIFF_11_net, 
        Y_I_INBUF_DIFF_12_net, Y_I_INBUF_DIFF_13_net, 
        Y_I_INBUF_DIFF_14_net, Y_I_INBUF_DIFF_15_net, 
        Y_I_INBUF_DIFF_16_net, Y_I_INBUF_DIFF_17_net, 
        Y_I_INBUF_DIFF_18_net;
    
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_9 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_9), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_9), .RX_DATA({RX_DATA_9[9], RX_DATA_9[8], 
        RX_DATA_9[7], RX_DATA_9[6], RX_DATA_9[5], RX_DATA_9[4], 
        RX_DATA_9[3], RX_DATA_9[2], RX_DATA_9[1], RX_DATA_9[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_9), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_9), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_9_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc0, nc1, nc2, nc3, nc4, 
        nc5, nc6, nc7, nc8, nc9, nc10}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_2 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_2), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_2), .RX_DATA({RX_DATA_2[9], RX_DATA_2[8], 
        RX_DATA_2[7], RX_DATA_2[6], RX_DATA_2[5], RX_DATA_2[4], 
        RX_DATA_2[3], RX_DATA_2[2], RX_DATA_2[1], RX_DATA_2[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_2), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_2), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_2_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc11, nc12, nc13, nc14, 
        nc15, nc16, nc17, nc18, nc19, nc20, nc21}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_3 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_3), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_3), .RX_DATA({RX_DATA_3[9], RX_DATA_3[8], 
        RX_DATA_3[7], RX_DATA_3[6], RX_DATA_3[5], RX_DATA_3[4], 
        RX_DATA_3[3], RX_DATA_3[2], RX_DATA_3[1], RX_DATA_3[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_3), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_3), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_3_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc22, nc23, nc24, nc25, 
        nc26, nc27, nc28, nc29, nc30, nc31, nc32}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_14 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_14), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_14), .RX_DATA({RX_DATA_14[9], RX_DATA_14[8], 
        RX_DATA_14[7], RX_DATA_14[6], RX_DATA_14[5], RX_DATA_14[4], 
        RX_DATA_14[3], RX_DATA_14[2], RX_DATA_14[1], RX_DATA_14[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_14), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_14), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_14_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc33, nc34, nc35, nc36, 
        nc37, nc38, nc39, nc40, nc41, nc42, nc43}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_8 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_8), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_8), .RX_DATA({RX_DATA_8[9], RX_DATA_8[8], 
        RX_DATA_8[7], RX_DATA_8[6], RX_DATA_8[5], RX_DATA_8[4], 
        RX_DATA_8[3], RX_DATA_8[2], RX_DATA_8[1], RX_DATA_8[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_8), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_8), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_8_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc44, nc45, nc46, nc47, 
        nc48, nc49, nc50, nc51, nc52, nc53, nc54}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_12 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_12), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_12), .RX_DATA({RX_DATA_12[9], RX_DATA_12[8], 
        RX_DATA_12[7], RX_DATA_12[6], RX_DATA_12[5], RX_DATA_12[4], 
        RX_DATA_12[3], RX_DATA_12[2], RX_DATA_12[1], RX_DATA_12[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_12), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_12), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_12_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc55, nc56, nc57, nc58, 
        nc59, nc60, nc61, nc62, nc63, nc64, nc65}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_3 (.PADP(PAD_I[3]), .PADN(PAD_I_N[3]), .Y(
        Y_I_INBUF_DIFF_3_net));
    INBUF_DIFF I_INBUF_DIFF_1 (.PADP(PAD_I[1]), .PADN(PAD_I_N[1]), .Y(
        Y_I_INBUF_DIFF_1_net));
    INBUF_DIFF I_INBUF_DIFF_2 (.PADP(PAD_I[2]), .PADN(PAD_I_N[2]), .Y(
        Y_I_INBUF_DIFF_2_net));
    INBUF_DIFF I_INBUF_DIFF_17 (.PADP(PAD_I[17]), .PADN(PAD_I_N[17]), 
        .Y(Y_I_INBUF_DIFF_17_net));
    INBUF_DIFF I_INBUF_DIFF_5 (.PADP(PAD_I[5]), .PADN(PAD_I_N[5]), .Y(
        Y_I_INBUF_DIFF_5_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_16 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_16), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_16), .RX_DATA({RX_DATA_16[9], RX_DATA_16[8], 
        RX_DATA_16[7], RX_DATA_16[6], RX_DATA_16[5], RX_DATA_16[4], 
        RX_DATA_16[3], RX_DATA_16[2], RX_DATA_16[1], RX_DATA_16[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_16), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_16), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_16_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc66, nc67, nc68, nc69, 
        nc70, nc71, nc72, nc73, nc74, nc75, nc76}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_0 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_0), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_0), .RX_DATA({RX_DATA_0[9], RX_DATA_0[8], 
        RX_DATA_0[7], RX_DATA_0[6], RX_DATA_0[5], RX_DATA_0[4], 
        RX_DATA_0[3], RX_DATA_0[2], RX_DATA_0[1], RX_DATA_0[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_0), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_0), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_0_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc77, nc78, nc79, nc80, 
        nc81, nc82, nc83, nc84, nc85, nc86, nc87}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_11 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_11), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_11), .RX_DATA({RX_DATA_11[9], RX_DATA_11[8], 
        RX_DATA_11[7], RX_DATA_11[6], RX_DATA_11[5], RX_DATA_11[4], 
        RX_DATA_11[3], RX_DATA_11[2], RX_DATA_11[1], RX_DATA_11[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_11), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_11), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_11_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc88, nc89, nc90, nc91, 
        nc92, nc93, nc94, nc95, nc96, nc97, nc98}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_11 (.PADP(PAD_I[11]), .PADN(PAD_I_N[11]), 
        .Y(Y_I_INBUF_DIFF_11_net));
    INBUF_DIFF I_INBUF_DIFF_6 (.PADP(PAD_I[6]), .PADN(PAD_I_N[6]), .Y(
        Y_I_INBUF_DIFF_6_net));
    INBUF_DIFF I_INBUF_DIFF_14 (.PADP(PAD_I[14]), .PADN(PAD_I_N[14]), 
        .Y(Y_I_INBUF_DIFF_14_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_5 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_5), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_5), .RX_DATA({RX_DATA_5[9], RX_DATA_5[8], 
        RX_DATA_5[7], RX_DATA_5[6], RX_DATA_5[5], RX_DATA_5[4], 
        RX_DATA_5[3], RX_DATA_5[2], RX_DATA_5[1], RX_DATA_5[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_5), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_5), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_5_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc99, nc100, nc101, nc102, 
        nc103, nc104, nc105, nc106, nc107, nc108, nc109}), .SWITCH(), 
        .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), .TX_DATA_OUT_8(), 
        .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT(), .INFF_SL_OUT(), 
        .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_15 (.PADP(PAD_I[15]), .PADN(PAD_I_N[15]), 
        .Y(Y_I_INBUF_DIFF_15_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_17 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_17), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_17), .RX_DATA({RX_DATA_17[9], RX_DATA_17[8], 
        RX_DATA_17[7], RX_DATA_17[6], RX_DATA_17[5], RX_DATA_17[4], 
        RX_DATA_17[3], RX_DATA_17[2], RX_DATA_17[1], RX_DATA_17[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_17), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_17), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_17_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc110, nc111, nc112, 
        nc113, nc114, nc115, nc116, nc117, nc118, nc119, nc120}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_4 (.PADP(PAD_I[4]), .PADN(PAD_I_N[4]), .Y(
        Y_I_INBUF_DIFF_4_net));
    INBUF_DIFF I_INBUF_DIFF_0 (.PADP(PAD_I[0]), .PADN(PAD_I_N[0]), .Y(
        Y_I_INBUF_DIFF_0_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_6 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_6), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_6), .RX_DATA({RX_DATA_6[9], RX_DATA_6[8], 
        RX_DATA_6[7], RX_DATA_6[6], RX_DATA_6[5], RX_DATA_6[4], 
        RX_DATA_6[3], RX_DATA_6[2], RX_DATA_6[1], RX_DATA_6[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_6), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_6), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_6_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc121, nc122, nc123, 
        nc124, nc125, nc126, nc127, nc128, nc129, nc130, nc131}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_9 (.PADP(PAD_I[9]), .PADN(PAD_I_N[9]), .Y(
        Y_I_INBUF_DIFF_9_net));
    INBUF_DIFF I_INBUF_DIFF_12 (.PADP(PAD_I[12]), .PADN(PAD_I_N[12]), 
        .Y(Y_I_INBUF_DIFF_12_net));
    VCC vcc_inst (.Y(VCC_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_15 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_15), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_15), .RX_DATA({RX_DATA_15[9], RX_DATA_15[8], 
        RX_DATA_15[7], RX_DATA_15[6], RX_DATA_15[5], RX_DATA_15[4], 
        RX_DATA_15[3], RX_DATA_15[2], RX_DATA_15[1], RX_DATA_15[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_15), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_15), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_15_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc132, nc133, nc134, 
        nc135, nc136, nc137, nc138, nc139, nc140, nc141, nc142}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_7 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_7), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_7), .RX_DATA({RX_DATA_7[9], RX_DATA_7[8], 
        RX_DATA_7[7], RX_DATA_7[6], RX_DATA_7[5], RX_DATA_7[4], 
        RX_DATA_7[3], RX_DATA_7[2], RX_DATA_7[1], RX_DATA_7[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_7), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_7), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_7_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc143, nc144, nc145, 
        nc146, nc147, nc148, nc149, nc150, nc151, nc152, nc153}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_8 (.PADP(PAD_I[8]), .PADN(PAD_I_N[8]), .Y(
        Y_I_INBUF_DIFF_8_net));
    INBUF_DIFF I_INBUF_DIFF_18 (.PADP(PAD_I[18]), .PADN(PAD_I_N[18]), 
        .Y(Y_I_INBUF_DIFF_18_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_4 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_4), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_4), .RX_DATA({RX_DATA_4[9], RX_DATA_4[8], 
        RX_DATA_4[7], RX_DATA_4[6], RX_DATA_4[5], RX_DATA_4[4], 
        RX_DATA_4[3], RX_DATA_4[2], RX_DATA_4[1], RX_DATA_4[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_4), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_4), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_4_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc154, nc155, nc156, 
        nc157, nc158, nc159, nc160, nc161, nc162, nc163, nc164}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_10 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_10), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_10), .RX_DATA({RX_DATA_10[9], RX_DATA_10[8], 
        RX_DATA_10[7], RX_DATA_10[6], RX_DATA_10[5], RX_DATA_10[4], 
        RX_DATA_10[3], RX_DATA_10[2], RX_DATA_10[1], RX_DATA_10[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_10), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_10), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_10_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc165, nc166, nc167, 
        nc168, nc169, nc170, nc171, nc172, nc173, nc174, nc175}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_7 (.PADP(PAD_I[7]), .PADN(PAD_I_N[7]), .Y(
        Y_I_INBUF_DIFF_7_net));
    GND gnd_inst (.Y(GND_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_1 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_1), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_1), .RX_DATA({RX_DATA_1[9], RX_DATA_1[8], 
        RX_DATA_1[7], RX_DATA_1[6], RX_DATA_1[5], RX_DATA_1[4], 
        RX_DATA_1[3], RX_DATA_1[2], RX_DATA_1[1], RX_DATA_1[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_1), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_1), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_1_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc176, nc177, nc178, 
        nc179, nc180, nc181, nc182, nc183, nc184, nc185, nc186}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_10 (.PADP(PAD_I[10]), .PADN(PAD_I_N[10]), 
        .Y(Y_I_INBUF_DIFF_10_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_18 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_18), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_18), .RX_DATA({RX_DATA_18[9], RX_DATA_18[8], 
        RX_DATA_18[7], RX_DATA_18[6], RX_DATA_18[5], RX_DATA_18[4], 
        RX_DATA_18[3], RX_DATA_18[2], RX_DATA_18[1], RX_DATA_18[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_18), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_18), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_18_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc187, nc188, nc189, 
        nc190, nc191, nc192, nc193, nc194, nc195, nc196, nc197}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_16 (.PADP(PAD_I[16]), .PADN(PAD_I_N[16]), 
        .Y(Y_I_INBUF_DIFF_16_net));
    IOD #( .DATA_RATE(1200.0), .FORMAL_NAME("RXD%STATIC_DELAY"), .INTERFACE_NAME("RX_DDRX_B_G_DYN")
        , .DELAY_LINE_SIMULATION_MODE("DISABLED"), .RESERVED_0(1'b0), .RX_CLK_EN(1'b1)
        , .RX_CLK_INV(1'b0), .TX_CLK_EN(1'b0), .TX_CLK_INV(1'b0), .HS_IO_CLK_SEL(3'b000)
        , .QDR_EN(1'b0), .EDGE_DETECT_EN(1'b0), .DELAY_LINE_MODE(2'b01)
        , .RX_MODE(4'b1101), .EYE_MONITOR_MODE(1'b0), .DYN_DELAY_LINE_EN(1'b0)
        , .FIFO_WR_EN(1'b1), .EYE_MONITOR_EN(1'b1), .TX_MODE(7'b0000000)
        , .TX_CLK_SEL(2'b00), .TX_OE_MODE(3'b111), .TX_OE_CLK_INV(1'b0)
        , .RX_DELAY_VAL(8'b00000001), .RX_DELAY_VAL_X2(1'b0), .TX_DELAY_VAL(8'b00000001)
        , .EYE_MONITOR_WIDTH(3'b001), .EYE_MONITOR_WIDTH_SRC(1'b1), .RESERVED_1(1'b0)
        , .DISABLE_LANECTRL_RESET(1'b0), .INPUT_DELAY_SEL(2'b00), .OEFF_EN_INV(1'b0)
        , .INFF_EN_INV(1'b0), .OUTFF_EN_INV(1'b0) )  I_IOD_13 (
        .EYE_MONITOR_EARLY(EYE_MONITOR_EARLY_13), .EYE_MONITOR_LATE(
        EYE_MONITOR_LATE_13), .RX_DATA({RX_DATA_13[9], RX_DATA_13[8], 
        RX_DATA_13[7], RX_DATA_13[6], RX_DATA_13[5], RX_DATA_13[4], 
        RX_DATA_13[3], RX_DATA_13[2], RX_DATA_13[1], RX_DATA_13[0]}), 
        .DELAY_LINE_OUT_OF_RANGE(), .TX_DATA({GND_net, GND_net, 
        GND_net, GND_net, GND_net, GND_net, GND_net, GND_net}), 
        .OE_DATA({GND_net, GND_net, GND_net, GND_net}), .RX_BIT_SLIP(
        GND_net), .EYE_MONITOR_CLEAR_FLAGS(EYE_MONITOR_CLEAR_FLAGS_13), 
        .DELAY_LINE_MOVE(GND_net), .DELAY_LINE_DIRECTION(GND_net), 
        .DELAY_LINE_LOAD(GND_net), .RX_CLK(FAB_CLK), .TX_CLK(GND_net), 
        .ODT_EN(ODT_EN_13), .INFF_SL(GND_net), .INFF_EN(GND_net), 
        .OUTFF_SL(GND_net), .OUTFF_EN(GND_net), .AL_N(GND_net), 
        .OEFF_LAT_N(GND_net), .OEFF_SD_N(GND_net), .OEFF_AD_N(GND_net), 
        .INFF_LAT_N(GND_net), .INFF_SD_N(GND_net), .INFF_AD_N(GND_net), 
        .OUTFF_LAT_N(GND_net), .OUTFF_SD_N(GND_net), .OUTFF_AD_N(
        GND_net), .RX_P(Y_I_INBUF_DIFF_13_net), .RX_N(), .TX_DATA_9(
        GND_net), .TX_DATA_8(GND_net), .ARST_N(ARST_N), .RX_SYNC_RST(
        RX_SYNC_RST), .TX_SYNC_RST(TX_SYNC_RST), .HS_IO_CLK({GND_net, 
        GND_net, GND_net, GND_net, GND_net, HS_IO_CLK[0]}), .RX_DQS_90({
        GND_net, RX_DQS_90[0]}), .TX_DQS(GND_net), .TX_DQS_270(GND_net)
        , .FIFO_WR_PTR({FIFO_WR_PTR[2], FIFO_WR_PTR[1], FIFO_WR_PTR[0]})
        , .FIFO_RD_PTR({FIFO_RD_PTR[2], FIFO_RD_PTR[1], FIFO_RD_PTR[0]})
        , .TX(), .OE(), .CDR_CLK(GND_net), .CDR_NEXT_CLK(GND_net), 
        .EYE_MONITOR_LANE_WIDTH({EYE_MONITOR_LANE_WIDTH[2], 
        EYE_MONITOR_LANE_WIDTH[1], EYE_MONITOR_LANE_WIDTH[0]}), 
        .DDR_DO_READ(), .CDR_CLK_A_SEL_8(), .CDR_CLK_A_SEL_9(), 
        .CDR_CLK_A_SEL_10(), .CDR_CLK_B_SEL({nc198, nc199, nc200, 
        nc201, nc202, nc203, nc204, nc205, nc206, nc207, nc208}), 
        .SWITCH(), .CDR_CLR_NEXT_CLK_N(), .TX_DATA_OUT_9(), 
        .TX_DATA_OUT_8(), .AL_N_OUT(), .OUTFF_SL_OUT(), .OUTFF_EN_OUT()
        , .INFF_SL_OUT(), .INFF_EN_OUT(), .RX_CLK_OUT(), .TX_CLK_OUT());
    INBUF_DIFF I_INBUF_DIFF_13 (.PADP(PAD_I[13]), .PADN(PAD_I_N[13]), 
        .Y(Y_I_INBUF_DIFF_13_net));
    
endmodule
