
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.obdt_pkg.all;
use work.vendor_specific_gbt_bank_package.all;

library polarfire;
use polarfire.all;

entity obdt_top is
  port (  
    -- clocks, resets
    clockdes3_p         : IN std_logic;
    clockdes3_n         : IN std_logic;
    
    -- TDC inputs
    data_in_from_pins_p : IN std_logic_vector(TDC_N-1 downto 0);
    data_in_from_pins_n : IN std_logic_vector(TDC_N-1 downto 0);

    -- Serial lanes
    -- range is ascending (to and not downto) to match the gbt module convention
    -- the index matches the schematic signal name, not the polarfire transceiver channel index pin
    QSFP_TX_P           : out std_logic_vector(1 to 2);
    QSFP_TX_N           : out std_logic_vector(1 to 2);
    QSFP_RX_P           : in  std_logic_vector(1 to 2);
    QSFP_RX_N           : in  std_logic_vector(1 to 2);

    -- SCA control
    sca_reset_b         : OUT std_logic;
    sca_auxporten       : OUT std_logic;

    -- GBTx control
    gbtx_reset_b        : OUT std_logic;
    gbtx_rxlockmode     : OUT std_logic_vector(1 downto 0);
    gbtx_stateoverride  : OUT std_logic;
    gbtx_mode           : OUT std_logic_vector(3 downto 0);
    gbtx_refclkselect   : OUT std_logic;
    gbtx_refclk_n       : OUT std_logic;
    gbtx_refclk_p       : OUT std_logic;
    gbtx_rxdatavalid    : IN  std_logic;
    gbtx_txdatavalid    : OUT std_logic;
    gbtx_rxready        : IN std_logic;
    gbtx_txready        : IN std_logic;
    
    -- E-Link
    E_IN_P, E_IN_N      : in std_logic;
    E_OUT_P, E_OUT_N    : out std_logic;
   
    -- Testpulses
    fmaskr_p, fmaskr_n  : out std_logic;
    fmaskl_p, fmaskl_n  : out std_logic;
    TestPulse           : OUT std_logic
  );
end obdt_top;

architecture structural of obdt_top is
   
  -- RESETS
  signal device_init_done         : std_logic;
  signal rst                      : std_logic := '0' ;
  signal genRst_to_gbt            : std_logic := '0' ;
  signal txRst_to_gbt             : std_logic := '0' ;
  signal rxRst_to_gbt             : std_logic := '0' ;
  signal genRst_from_elink        : std_logic := '0' ;
  
  signal gbtx_reset_period_log2   : unsigned(4 downto 0);

  -- PLL LOCK
  signal pll120_locked            : std_logic;
  signal pll240_locked            : std_logic;
  signal pll320_locked            : std_logic;
  signal serdesPllLocked_0        : std_logic;
  signal serdesPllLocked_1        : std_logic;
  signal serdesPllLocked_2        : std_logic;
  signal serdesPllLocked_4        : std_logic;

  -- DATA PATH
  signal tdc_enc                  : tdc_enc_a := (others=>(others=>'0'));
  signal data_from_readout_0      : std_logic_vector(24 downto 0) :=(others=>'0');
  signal data_from_readout_1      : std_logic_vector(24 downto 0) :=(others=>'0');
  signal strobe_from_readout_0    : std_logic;
  signal strobe_from_readout_1    : std_logic;
  signal empty_from_readout_0     : std_logic;
  signal empty_from_readout_1     : std_logic;
  signal tictoc                   : std_logic;
  
  -- GBT DATA SIGNALS
  -- range is ascending (to and not downto) to match the gbt module convention
  signal txIsDataSel              : std_logic_vector(1 to 2);
  signal txData                   : gbtframe_A      (1 to 2);
  signal rxIsData                 : std_logic_vector(1 to 2);
  signal rxData                   : gbtframe_A      (1 to 2);

  -- CLOCKS
  signal oscClk160                : std_logic;  -- local oscillator 160 MHz   
  signal oscClk2                  : std_logic;  -- local oscillator 2 MHz     
  
  signal clk40_i, clk40           : std_logic;  -- LHC Clock
  signal clk120                   : std_logic;  -- 120 MHz 
  signal clk240                   : std_logic;  -- 240 MHz
  signal clk320                   : std_logic;  -- 320 MHz
  
  signal clkSerdes600_0           : std_logic;  -- 600 MHz serdes
  signal clkSerdes600_1           : std_logic;  -- 600 MHz serdes
  signal clkSerdes600_2           : std_logic;  -- 600 MHz serdes
  signal clkSerdes600_4           : std_logic;  -- 600 MHz serdes

  signal clk600g_0                : std_logic;  -- ccc0 feedback signals
  signal clk600g_1                : std_logic;  -- ccc0 feedback signals
  signal clk600g_2                : std_logic;  -- ccc0 feedback signals
  signal clk600g_4                : std_logic;  -- ccc0 feedback signals
  
  -- ELINK and conf
  signal elink_data : std_logic_vector(7 downto 0);
  signal conf   : conf_t;
  signal pulses : std_logic_vector(15 downto 0);
  signal conf_channel_enable : std_logic_vector(TDC_N-1 downto 0);
  signal deadtime : std_logic_vector(3 downto 0);

  -- TTC
  signal bc0, oc0 : std_logic;
  signal bunch_ctr : std_logic_vector(11 DOWNTO 0) := (others =>'0');
  signal orbit_ctr : std_logic_vector(31 DOWNTO 0) := (others =>'0');
  
  -- TestPulse
  signal tp_bgo, tp_enable, tp_intgen : std_logic;
  signal tp_delay     : unsigned(15 downto 0);
  signal tp_duration  : unsigned(5 downto 0);
  signal tp_fmaskr, tp_fmaskl : std_logic;

  component init_core
  port(
    -- Outputs
    AUTOCALIB_DONE              : out std_logic;
    DEVICE_INIT_DONE            : out std_logic;
    FABRIC_POR_N                : out std_logic;
    PCIE_INIT_DONE              : out std_logic;
    SRAM_INIT_DONE              : out std_logic;
    SRAM_INIT_FROM_SNVM_DONE    : out std_logic;
    SRAM_INIT_FROM_SPI_DONE     : out std_logic;
    SRAM_INIT_FROM_UPROM_DONE   : out std_logic;
    USRAM_INIT_DONE             : out std_logic;
    USRAM_INIT_FROM_SNVM_DONE   : out std_logic;
    USRAM_INIT_FROM_SPI_DONE    : out std_logic;
    USRAM_INIT_FROM_UPROM_DONE  : out std_logic;
    XCVR_INIT_DONE              : out std_logic
    );
  end component;

  component user_clock
  port(
    RCOSC_160MHZ_GL   : out std_logic;
    RCOSC_2MHZ_GL     : out std_logic
    );
  end component; 
   
  component ccc0
  port(
    REF_CLK_0         : in std_logic;
    FB_CLK_0          : in std_logic;
    PLL_POWERDOWN_N_0 : in std_logic;
    OUT0_FABCLK_0     : out std_logic;
    OUT1_HS_IO_CLK_0  : out std_logic;
    PLL_LOCK_0        : out std_logic
    );
  end component;  
  
  component ccc1
  port(
    REF_CLK_0         : in std_logic;
    FB_CLK_0          : in std_logic;
    PLL_POWERDOWN_N_0 : in std_logic;
    OUT0_FABCLK_0     : out std_logic;
    OUT1_FABCLK_0     : out std_logic;
    OUT2_FABCLK_0     : out std_logic;
    PLL_LOCK_0        : out std_logic
    );
  end component;  

  component pll320
  port(
    REF_CLK_0         : in std_logic;
    FB_CLK_0          : in std_logic;
    PLL_POWERDOWN_N_0 : in std_logic;
    OUT0_FABCLK_0     : out std_logic;
    PLL_LOCK_0        : out std_logic
    );
  end component;  

  component INBUF_DIFF
  port(
    PADP              : in std_logic;
    PADN              : in std_logic;
    Y                 : out std_logic
    );
  end component;
  
  component OUTBUF_DIFF
  port(
    D                 : in std_logic;
    PADP              : out std_logic;
    PADN              : out std_logic
    );
  end component;

  component CLKINT
  port (
    A                 : in std_logic;
    Y                 : out std_logic
    );
  end component;

begin 
  
  -----------------------
  -- INIT CORE
  -----------------------
  init_0 : init_core
  port map (
    AUTOCALIB_DONE              => open,
    DEVICE_INIT_DONE            => device_init_done,
    FABRIC_POR_N                => open,
    PCIE_INIT_DONE              => open,
    SRAM_INIT_DONE              => open,
    SRAM_INIT_FROM_SNVM_DONE    => open,
    SRAM_INIT_FROM_SPI_DONE     => open,
    SRAM_INIT_FROM_UPROM_DONE   => open,
    USRAM_INIT_DONE             => open,
    USRAM_INIT_FROM_SNVM_DONE   => open,
    USRAM_INIT_FROM_SPI_DONE    => open,
    USRAM_INIT_FROM_UPROM_DONE  => open,
    XCVR_INIT_DONE              => open
  );

  -----------------------
  -- clocks
  -----------------------
  user_clock_i: user_clock
  port map(
    RCOSC_160MHZ_GL     => oscClk160,
    RCOSC_2MHZ_GL       => oscClk2
  );

  clockdes3_i : INBUF_DIFF
  port map(
    PADP                => clockdes3_p,
    PADN                => clockdes3_n,
    Y                   => clk40_i
  );

  global_40_i: CLKINT
  port map(
    A                   => clk40_i,
    Y                   => clk40
  );    

  pll120_inst: ccc1
  port map(
    REF_CLK_0           => clk40,
    FB_CLK_0            => clk120,
    PLL_POWERDOWN_N_0   => device_init_done,
    OUT0_FABCLK_0       => clk120,
    OUT1_FABCLK_0       => clk240,
    OUT2_FABCLK_0       => open,
    PLL_LOCK_0          => pll120_locked
  );
  
  pll320_inst: pll320
  port map(
    REF_CLK_0           => clk40,
    FB_CLK_0            => clk320,
    PLL_POWERDOWN_N_0   => device_init_done,
    OUT0_FABCLK_0       => clk320,
    PLL_LOCK_0          => pll320_locked
  );
  
  serdesPll_0 : ccc0
  port map(
    REF_CLK_0           => clk40,
    FB_CLK_0            => clk600g_0,
    PLL_POWERDOWN_N_0   => device_init_done,
    OUT0_FABCLK_0       => clk600g_0,
    OUT1_HS_IO_CLK_0    => clkSerdes600_0,
    PLL_LOCK_0          => serDesPllLocked_0
  );

  serdesPll_1 : ccc0
  port map(
    REF_CLK_0           => clk40,
    FB_CLK_0            => clk600g_1,
    PLL_POWERDOWN_N_0   => device_init_done,
    OUT0_FABCLK_0       => clk600g_1,
    OUT1_HS_IO_CLK_0    => clkSerdes600_1,
    PLL_LOCK_0          => serDesPllLocked_1
  );

  serdesPll_2 : ccc0
  port map(
    REF_CLK_0           => clk40,
    FB_CLK_0            => clk600g_2,
    PLL_POWERDOWN_N_0   => device_init_done,
    OUT0_FABCLK_0       => clk600g_2,
    OUT1_HS_IO_CLK_0    => clkSerdes600_2,
    PLL_LOCK_0          => serDesPllLocked_2
  );

  serdesPll_4 : ccc0
  port map(
    REF_CLK_0           => clk40,
    FB_CLK_0            => clk600g_4,
    PLL_POWERDOWN_N_0   => device_init_done,
    OUT0_FABCLK_0       => clk600g_4,
    OUT1_HS_IO_CLK_0    => clkSerdes600_4,
    PLL_LOCK_0          => serDesPllLocked_4
  );

  
  rst <= not gbtx_rxready;

  -----------------------
  -- GBTx Control
  -----------------------
  gbtx_rxlockmode     <= "01"; -- auto DAC calibration
  gbtx_stateoverride  <= '0';  -- doesn't halt FSM
  gbtx_mode           <= "0010";   -- trasnceiver mode
  gbtx_txdatavalid    <= '1';
  gbtx_refclkselect   <= '0';  -- internal xPLL
  
  refclk_obuf_inst : OUTBUF_DIFF
  PORT MAP (
    D => '1',
    PADP => gbtx_refclk_p,
    PADN => gbtx_refclk_n
  );

  process (oscClk2, device_init_done)
    variable rstcntr : unsigned(41 downto 0) := (others => '1');
  begin
    if rising_edge(oscClk2) then
      if not device_init_done then
        rstcntr := (25 => '1', others => '0');
        gbtx_reset_b <= '1';
      elsif gbtx_reset_period_log2 /= (gbtx_reset_period_log2'range => '1') then
        if rstcntr < 8 then   gbtx_reset_b <= '0';
        else                  gbtx_reset_b <= '1';
        end if;

        if serDesPllLocked_0 = '1' or rstcntr = (rstcntr'range => '0') then
          rstcntr := (others => '0');
          if gbtx_reset_period_log2 = (gbtx_reset_period_log2'range => '0') then  rstcntr(25) := '1'; -- Default (after boot) autoreset period is 16 seconds
          else                                                                    rstcntr(11 + to_integer(gbtx_reset_period_log2)) := '1';
          end if;
        else
          rstcntr := rstcntr - 1;
        end if;
      else
        gbtx_reset_b <= '1';
      end if;
    end if;
  end process;



  -----------------------
  -- e-link
  -----------------------
  elink_inst: entity work.elink
  port map(
    rst         => not device_init_done,

    -- E-LINK
    E_IN_P      => E_IN_P     ,
    E_IN_N      => E_IN_N     ,
    E_OUT_P     => E_OUT_P    ,
    E_OUT_N     => E_OUT_N    ,
    
    -- Clocks
    clk40       => clk40      ,
    clk320      => clk320     ,
    
    -- DATA                
    elink_data  => elink_data ,
    conf        => conf       ,
    pulses      => pulses
  );
  
  -- Elink/conf assignments done here to ease identification of dual purpose bits
  
  -- fast lanes
  bc0     <= elink_data(0);
  oc0     <= elink_data(2);
  tp_bgo  <= elink_data(4);
  
  -- Pulse outputs
  genRst_from_elink   <= pulses(0);
  txRst_to_gbt        <= pulses(1);
  rxRst_to_gbt        <= pulses(2);
  
  -- Conf register
  -- 14 downto 0 --> masks
  masks_gen: for ch in TDC_N-1 downto 0 generate
    --conf_channel_enable(ch) <= '1' when ch=0 or ch=36 or ch=55 or ch=98 or ch=144 or ch=168 or ch=204 else '0';
    conf_channel_enable(ch) <= conf(ch/16)(ch mod 16);
  end generate;
  -- 15 & 16 --> TPs
  tp_enable    <= conf(15)(0);
  tp_intgen    <= conf(15)(1);
  tp_duration  <= unsigned(conf(15)(7 downto 2));
  tp_fmaskr    <= conf(15)(8);
  tp_fmaskl    <= conf(15)(9);
  tp_delay     <= unsigned(conf(16));

  -- 17 --> gbtx_reset
  gbtx_reset_period_log2  <= unsigned(conf(17)(4 downto 0));
  
  -- 18 deadtime
  deadtime <= conf(18)(3 downto 0);
  
  -----------------------
  -- TTC
  -----------------------
  ttc_inst: entity work.ttc
  port map(
    bc0           => bc0,
    oc0           => oc0,
    clk40         => clk40,
    bunch_ctr     => bunch_ctr,
    orbit_ctr     => orbit_ctr
  );
    

  -----------------------
  -- TDC
  -----------------------
  tdc_inst: entity work.tdc
  port map (
    -- TDC inputs
    data_in_from_pins_p => data_in_from_pins_p,
    data_in_from_pins_n => data_in_from_pins_n,
    -- Other inputs
    bx_cnt              => bunch_ctr,
    deadtime            => deadtime,
    -- clocks, resets
    rst                 => rst,
    clk40               => clk40,
    clk120              => clk120,
    clk_bank0           => clkSerdes600_0,
    clk_bank1           => clkSerdes600_1,
    clk_bank2           => clkSerdes600_2,
    clk_bank4           => clkSerdes600_4,
    clk_bank5           => clkSerdes600_4,
    clk_bank6           => clkSerdes600_2,
    clk_bank7           => clkSerdes600_0,
    -- TDC output
    tdc_enc             => tdc_enc,
    enable_mask         => conf_channel_enable
  );

  -----------------------
  -- Readout
  -----------------------
  readout_inst: entity work.readout
  port map (
    clk           => clk240,
    rst           => rst,
    data_in       => tdc_enc,
    bx_cnt        => bunch_ctr,
    
    rdclk_0       => clk120,
    data_out_0    => data_from_readout_0,
    strobe_out_0  => strobe_from_readout_0,
    empty_out_0   => empty_from_readout_0,

    rdclk_1       => clk40,
    data_out_1    => data_from_readout_1,
    strobe_out_1  => strobe_from_readout_1,
    empty_out_1   => empty_from_readout_1
  );

  tictoc <= not tictoc when rising_edge(clk40);

  process (clk120)
    variable tictoc_pipe : std_logic;
    variable next_txIsDataSel : std_logic;
    variable next_data_out : std_logic_vector(83 downto 0);
  begin
    if rising_edge(clk120) then
      if strobe_from_readout_0 = '1' and empty_from_readout_0 = '0' then
        next_txIsDataSel := '1';
        next_data_out := next_data_out(55 downto 0) & "100" & data_from_readout_0;
      end if;
      
      if tictoc /= tictoc_pipe then
        txData(1) <= next_data_out;
        txIsDataSel(1) <= next_txIsDataSel;
        next_data_out := (others => '0');
        next_txIsDataSel := '0';
      end if;

      tictoc_pipe := tictoc;
    end if;
  end process;

  process (clk40,rst)
  begin
    if rising_edge(clk40) then
      if rst = '1' then
        txIsDataSel(2) <='0';
      else               
        if (strobe_from_readout_1 = '1') then
          txData(2)(63 downto 62) <= "01";
          txData(2)(61 downto 58) <= "0001";
          txData(2)(57 downto 49) <=  '0' & data_from_readout_1(7 downto 0);  -- channel nummber
          txData(2)(48 downto 17) <= orbit_ctr;  -- orbit counter
          txData(2)(16 downto 5) <= data_from_readout_1(19 downto 8);  -- bunch counter
          txData(2)(4 downto 0) <= data_from_readout_1(24 downto 20); -- tdc count
          txIsDataSel(2) <='1';
        else
          txData(2)(74 downto 0) <= (others => '0');
          txIsDataSel(2) <='0';
        end if;
      end if;
    end if;
  end process;

 
  -----------------------
  -- GBT inst
  -----------------------
  gbt_inst: entity work.gbt
  generic map(
    NUM_LINKS   => 2
  )
  port map (
    txFrameClk40  => clk40,
    tdcMgtClk120  => clk120,
    drpClk        => oscClk160,

    QSFP_TX_P     => QSFP_TX_P,
    QSFP_TX_N     => QSFP_TX_N,
    QSFP_RX_P     => QSFP_RX_P,
    QSFP_RX_N     => QSFP_RX_N,
    
    generalReset  => genRst_to_gbt,
    manualResetTx => txRst_to_gbt,
    manualResetRx => rxRst_to_gbt,
    
    txData        => txData,
    txIsDataSel   => txIsDataSel,
    rxData        => rxData,
    rxIsData      => rxIsData
  );
 
  genRst: entity work.msemi_pf_reset
  generic map ( CLK_FREQ      => 2**13-1 ) -- reset is held active this many clock cycles
  port map (     
    CLK_I        => clk40,
    RESET1_B_I   => gbtx_rxready,
    RESET2_B_I   => not genRst_from_elink,
    RESET_O      => genRst_to_gbt
  ); 



  -----------------------
  -- SCA Control
  -----------------------
  sca_reset_b <= '1';
  sca_auxporten <= '0';
  
  
  
  -----------------------
  -- TestPulses
  -----------------------
  tp_inst: entity work.testpulse
  port map(
    clk           => clk320,
    rst           => rst,
    -- configuration
    enable        => tp_enable,
    intgen        => tp_intgen,
    delay         => tp_delay,
    duration      => tp_duration,
    -- signals
    bgo           => tp_bgo,
    TestPulse     => TestPulse
  );

  fmaskr_inst : OUTBUF_DIFF
  PORT MAP (
    D => tp_fmaskr,
    PADP => fmaskr_p,
    PADN => fmaskr_n
  );

  fmaskl_inst : OUTBUF_DIFF
  PORT MAP (
    D => tp_fmaskl,
    PADP => fmaskl_p,
    PADN => fmaskl_n
  );


end structural;