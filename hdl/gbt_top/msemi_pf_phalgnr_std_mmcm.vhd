----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.01.2016 09:00:47
-- Design Name: 
-- Module Name: msemi_pf_phalgnr_std_mmcm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity phaligner_std_pll is
  Port (
     RX_WORDCLK_I       : in std_logic;
     RX_FRAMECLK_O      : out std_logic;
     
     RESET_I            : in std_logic;
     
     PHASE_SHIFT        : in std_logic;
     SHIFT_DONE         : out std_logic;
     
     LOCKED             : out std_logic
   );
end phaligner_std_pll;

architecture Behavioral of phaligner_std_pll is

shared variable cnt : integer := 0; 

component polarfire_gbt_rx_frameclk_phalgnr_mmcm
    port(
    -- Inputs
    LOAD_PHASE_N_0 : in std_logic;
    PHASE_DIRECTION_0 : in std_logic;
    PHASE_OUT0_SEL_0 : in std_logic;
    PHASE_ROTATE_0 : in std_logic;
    REF_CLK_0 : in std_logic;
    -- Outputs
    OUT0_FABCLK_0 : out std_logic;
    PLL_LOCK_0 : out std_logic
);
end component;
begin

   --=====--
   -- PLL --
   --=====--
    pll: polarfire_gbt_rx_frameclk_phalgnr_mmcm                      -- AT 
      port map (                                                        
         -- Clock Input:                                           
         REF_CLK_0                                => RX_WORDCLK_I,                    
         -- Phase Shift Control:                                   
         LOAD_PHASE_N_0                                  => not RESET_I,
         PHASE_ROTATE_0                                   => PHASE_SHIFT,
         PHASE_DIRECTION_0                               => '0',
         PHASE_OUT0_SEL_0                                => '1',        
         -- Pll Status:                         
         PLL_LOCK_0                                 => LOCKED,
         -- Clock Outputs:                      
         OUT0_FABCLK_0                               => RX_FRAMECLK_O    -- Comment: Phase aligned 40MHz output.                             
      );

SHIFT_DONE_p: process(RX_WORDCLK_I)
begin
    if rising_edge(RX_WORDCLK_I) then
        if PHASE_SHIFT = '1' then
            cnt := 0;
        else 
            if cnt < 127 then
                cnt := cnt + 1;
            else
                cnt := 0;
            end if;
        end if;
        if cnt = 127 then
            SHIFT_DONE <= '1';
        else
            SHIFT_DONE <= '0';
        end if;
    end if;
end process;

end Behavioral;
