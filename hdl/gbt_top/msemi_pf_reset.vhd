--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (PH-ESE-BE)                                                         
-- Engineer:              Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros.marin@ieee.org)
--                        (Original design by Paschalis Vichoudis (CERN))      
--                                                                                            
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           Xilinx Kintex 7 & Virtex 7 - Reset                                       
--                                                                                            
-- Language:              VHDL'93                                                                  
--                                                                                              
-- Target Device:         Xilinx Kintex 7 & Virtex 7                                                      
-- Tool version:          ISE 14.5                                                                
--                                                                                              
-- Version:               3.0                                                                      
--
-- Description:        
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        22/06/2013   3.0       M. Barros Marin   First .vhd module definition           
--
-- Additional Comments:                                                                          
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity msemi_pf_reset is
   generic (   
      CLK_FREQ                                  : integer := 125e6        
   );          
   port(  
      
      --=======--
      -- Clock --
      --=======-- 
      
      CLK_I                                     : in  std_logic;
      
      --==============--     
      -- Reset scheme --     
      --==============--  
      
      RESET1_B_I                                : in  std_logic;                        
      RESET2_B_I                                : in  std_logic; 
      ------------------------------------------
      RESET_O                                   : out std_logic
      
   );
end msemi_pf_reset;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of msemi_pf_reset is

   --================================ Signal Declarations ================================--          
   
   signal rst_powerup_b                         : std_logic;
   signal rst_from_orGate                       : std_logic;
   
   --=====================================================================================--   
   signal reg                                   : std_logic_vector(15 downto 0) := "0000000000000001";

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================--
   
   --================--
   -- Power up reset --
   --================--
    
    shift_reg_p: process(CLK_I)     --AT
    begin
        if rising_edge(CLK_I) then
            for i in 0 to 14 loop
                reg(i+1) <= reg(i);
            end loop;
        end if;
    end process;

    rst_powerup_b <= reg(15);

   --===============--
   -- Delayed reset --
   --===============--

   -- Comment: Reset OR gate for the dly_rst_ctrl process:
   rst_from_orGate                              <= (not rst_powerup_b) or (not RESET1_B_I) or (not RESET2_B_I);
   
   -- Comment: Delayed reset control process:
   dlyRstCtrl: process(rst_from_orGate, CLK_I)
      variable timer                            : integer range 0 to CLK_FREQ;
   begin       
      if rst_from_orGate = '1' then         
         timer                                  := CLK_FREQ; -- Comment: Delay = 1s
         RESET_O                                <= '1';
      elsif rising_edge(CLK_I) then       
         if timer > 0 then       
           timer                                := timer - 1;
           RESET_O                              <= '1';
         else        
           RESET_O                              <= '0';
         end if;
    end if;
  end process;

   --=====================================================================================--   
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--