library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;


package obdt_pkg is

  constant FW_VERS : natural range 255 downto 0 := 8;
  constant FW_VERS_SLV : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(FW_VERS,8));

  constant TDC_N : natural := 240; -- real number of TDCs (not -1)

  type tdc_enc_a    is array (TDC_N-1 downto 0) of std_logic_vector(4 downto 0);

  constant CONF_ADDR_WIDTH : natural := 5;
  type conf_t       is array(2**CONF_ADDR_WIDTH-1 downto 0) of std_logic_vector(15 downto 0);

end obdt_pkg;