## When you move the folder of the project, or just clone from the repo in a different one
## Libero will not be able to correctly load the synthesis project, and thus you will lose
## the different implementations you have (regular, instrumented, etc.)
##
## This script opens the synthesis file and replaces the old base folder of the project
## with the current one to fix this. It should work both in windows and linux.
## Please review the results before opening the project again


import os

with open('synthesis/obdt_top_syn.prj','r')  as f:
    lines = f.readlines()

addfile_lines = [line for line in lines if line[:8] == 'add_file' and '/hdl/' in line ]
added_files = [line.split('"')[1] for line in addfile_lines]

i=0
while all( [added_files[0][:i] == added_file[:i] for added_file in added_files] ):
    i+=1
i-=6

old_base_folder = added_files[0][:i]

new_base_folder = os.getcwd()
new_base_folder = new_base_folder.replace('\\','/')


print old_base_folder
print new_base_folder

lines = [line.replace(old_base_folder, new_base_folder) for line in lines]



with open('synthesis/obdt_top_syn.prj','w')  as f:
    f.writelines(lines)

