device jtagport builtin
iice new {IICE} -type regular
iice controller -iice {IICE} none
iice sampler -iice {IICE} -depth 2048
iice clock -iice {IICE} -edge positive {/structural/elink_inst/e_clk_o}
signals add -iice {IICE} -silent -sample {/structural/elink_inst/behavioral/eout_txword_sig}\
{/structural/elink_inst/behavioral/pipe_sig}\
{/structural/txdata}\
{/structural/strobe_from_readout_1}\
{/structural/strobe_from_readout_0}\
{/structural/txisdatasel}\
{/structural/data_from_readout_1}\
{/structural/tdc_enc}\
{/structural/data_from_readout_0}\
{/structural/pulses}\
{/structural/orbit_ctr}\
{/structural/conf|[20:0]}\
{/structural/bunch_ctr}\
{/testpulse}
signals add -iice {IICE} -silent -sample -trigger {/structural/elink_inst/behavioral/bit0}\
{/structural/elink_inst/behavioral/bit3}\
{/structural/elink_inst/behavioral/bit1}\
{/structural/elink_inst/behavioral/bit2}\
{/structural/elink_inst/behavioral/bit4}\
{/structural/elink_inst/behavioral/bit7}\
{/structural/elink_inst/behavioral/bit5}\
{/structural/elink_inst/behavioral/bit6}\
{/structural/readout_i/behavioral/wr_en_1}\
{/structural/readout_i/behavioral/wr_en_2}\
{/structural/readout_i/behavioral/wr_en_out}

