# Microsemi Corp.
# Date: 2019-Oct-05 23:57:25
# This file was generated based on the following SDC source files:
#   C:/phase2/fws_obdt/obdt_fw/component/work/ccc1/ccc1_0/ccc1_ccc1_0_PF_CCC.sdc
#   C:/phase2/fws_obdt/obdt_fw/component/work/pll320/pll320_0/pll320_pll320_0_PF_CCC.sdc
#   C:/phase2/fws_obdt/obdt_fw/component/work/ccc0/ccc0_0/ccc0_ccc0_0_PF_CCC.sdc
#   C:/phase2/fws_obdt/obdt_fw/component/work/PF_TX_PLL_0/PF_TX_PLL_0_0/PF_TX_PLL_0_PF_TX_PLL_0_0_PF_TX_PLL.sdc
#   C:/phase2/fws_obdt/obdt_fw/component/work/polarfire_mgt_std/I_XCVR/polarfire_mgt_std_I_XCVR_PF_XCVR.sdc
#   C:/Microsemi/Libero_SoC_v12.2/Designer/data/aPA5M/cores/constraints/osc_rc2mhz.sdc
#   C:/Microsemi/Libero_SoC_v12.2/Designer/data/aPA5M/cores/constraints/osc_rc160mhz.sdc
#

create_clock -name {clockdes3_p} -period 25 [ get_ports { clockdes3_p } ]
create_clock -name {gbt_inst/pf_gbt_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtStd_gen.mgtStd/gtxStd_gen.1.msemi_pf_mgt_std_i/I_XCVR/LANE0/TX_CLK_R} -period 8.33333 [ get_pins { gbt_inst/pf_gbt_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtStd_gen.mgtStd/gtxStd_gen.1.msemi_pf_mgt_std_i/I_XCVR/LANE0/TX_CLK_R } ]
create_clock -name {gbt_inst/pf_gbt_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtStd_gen.mgtStd/gtxStd_gen.1.msemi_pf_mgt_std_i/I_XCVR/LANE0/RX_CLK_R} -period 8.33333 [ get_pins { gbt_inst/pf_gbt_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtStd_gen.mgtStd/gtxStd_gen.1.msemi_pf_mgt_std_i/I_XCVR/LANE0/RX_CLK_R } ]
create_clock -name {gbt_inst/pf_gbt_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtStd_gen.mgtStd/gtxStd_gen.2.msemi_pf_mgt_std_i/I_XCVR/LANE0/TX_CLK_R} -period 8.33333 [ get_pins { gbt_inst/pf_gbt_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtStd_gen.mgtStd/gtxStd_gen.2.msemi_pf_mgt_std_i/I_XCVR/LANE0/TX_CLK_R } ]
create_clock -name {gbt_inst/pf_gbt_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtStd_gen.mgtStd/gtxStd_gen.2.msemi_pf_mgt_std_i/I_XCVR/LANE0/RX_CLK_R} -period 8.33333 [ get_pins { gbt_inst/pf_gbt_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtStd_gen.mgtStd/gtxStd_gen.2.msemi_pf_mgt_std_i/I_XCVR/LANE0/RX_CLK_R } ]
create_clock -name {osc_rc2mhz} -period 500 [ get_pins { user_clock_i/user_clock_0/I_OSC_2/CLK } ]
create_generated_clock -name {pll120_inst/ccc1_0/pll_inst_0/OUT0} -multiply_by 3 -source [ get_pins { pll120_inst/ccc1_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { pll120_inst/ccc1_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { pll120_inst/ccc1_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { pll120_inst/ccc1_0/pll_inst_0/OUT0 } ]
create_generated_clock -name {pll120_inst/ccc1_0/pll_inst_0/OUT1} -multiply_by 6 -source [ get_pins { pll120_inst/ccc1_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { pll120_inst/ccc1_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { pll120_inst/ccc1_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { pll120_inst/ccc1_0/pll_inst_0/OUT1 } ]
create_generated_clock -name {pll320_inst/pll320_0/pll_inst_0/OUT0} -multiply_by 8 -source [ get_pins { pll320_inst/pll320_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { pll320_inst/pll320_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { pll320_inst/pll320_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { pll320_inst/pll320_0/pll_inst_0/OUT0 } ]
create_generated_clock -name {serdesPll_0/ccc0_0/pll_inst_0/OUT0} -multiply_by 15 -source [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/OUT0 } ]
create_generated_clock -name {serdesPll_0/ccc0_0/pll_inst_0/OUT1} -multiply_by 15 -source [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/OUT1 } ]
create_generated_clock -name {serdesPll_1/ccc0_0/pll_inst_0/OUT0} -multiply_by 15 -source [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/OUT0 } ]
create_generated_clock -name {serdesPll_1/ccc0_0/pll_inst_0/OUT1} -multiply_by 15 -source [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/OUT1 } ]
create_generated_clock -name {serdesPll_2/ccc0_0/pll_inst_0/OUT0} -multiply_by 15 -source [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/OUT0 } ]
create_generated_clock -name {serdesPll_2/ccc0_0/pll_inst_0/OUT1} -multiply_by 15 -source [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/OUT1 } ]
create_generated_clock -name {serdesPll_4/ccc0_0/pll_inst_0/OUT0} -multiply_by 15 -source [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/OUT0 } ]
create_generated_clock -name {serdesPll_4/ccc0_0/pll_inst_0/OUT1} -multiply_by 15 -source [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/OUT1 } ]
set_false_path -through [ get_pins { pll120_inst/ccc1_0/pll_inst_0/OUT0 } ] -to [ get_cells { pll120_inst/ccc1_0/Pll_Ext_FeedBack_Mode_Soft_Logic_Inst/* } ]
set_false_path -through [ get_pins { pll320_inst/pll320_0/pll_inst_0/OUT0 } ] -to [ get_cells { pll320_inst/pll320_0/Pll_Ext_FeedBack_Mode_Soft_Logic_Inst/* } ]
set_false_path -through [ get_pins { serdesPll_0/ccc0_0/pll_inst_0/OUT0 } ] -to [ get_cells { serdesPll_0/ccc0_0/Pll_Ext_FeedBack_Mode_Soft_Logic_Inst/* } ]
set_false_path -through [ get_pins { serdesPll_1/ccc0_0/pll_inst_0/OUT0 } ] -to [ get_cells { serdesPll_1/ccc0_0/Pll_Ext_FeedBack_Mode_Soft_Logic_Inst/* } ]
set_false_path -through [ get_pins { serdesPll_2/ccc0_0/pll_inst_0/OUT0 } ] -to [ get_cells { serdesPll_2/ccc0_0/Pll_Ext_FeedBack_Mode_Soft_Logic_Inst/* } ]
set_false_path -through [ get_pins { serdesPll_4/ccc0_0/pll_inst_0/OUT0 } ] -to [ get_cells { serdesPll_4/ccc0_0/Pll_Ext_FeedBack_Mode_Soft_Logic_Inst/* } ]
