set_false_path -from [ get_pins { elink_inst/conf_*/CLK } ]
set_false_path -from [ get_pins {elink_inst/bitslip/CLK} ] -to [ get_pins {elink_inst/bitslip_pipe[0]/D } ]
set_false_path -to [ get_pins { tp_inst/bgo_pipe[0]/D } ]
set_false_path -from [ get_pins {readout_inst/fifo_out_*/ro_fifo_out_0/*corefifo_async*/CLK} ] -to [ get_pins {readout_inst/fifo_out_*/ro_fifo_out_0/*corefifo_async/*/*/D } ]

#create_clock -name {e_clk} -period 3.125 -waveform {0 1.5625 } [ get_ports { E_CLK_P } ]

#set_false_path -from tdc_inst/rst_io_auto