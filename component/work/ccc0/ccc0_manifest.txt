Microsemi Corporation - Microsemi Libero Software Release v12.2 (Version 12.700.0.21)

Date      :  Mon Sep 30 22:00:37 2019
Project   :  C:\phase2\fws_obdt\obdt_fw
Component :  ccc0
Family    :  PolarFire


HDL source files for all Synthesis and Simulation tools:
    C:/phase2/fws_obdt/obdt_fw/component/work/ccc0/ccc0.v
    C:/phase2/fws_obdt/obdt_fw/component/work/ccc0/ccc0_0/ccc0_ccc0_0_PF_CCC.v
    C:/phase2/fws_obdt/obdt_fw/component/work/ccc0/ccc0_0/pll_ext_feedback_mode_soft_logic.v

