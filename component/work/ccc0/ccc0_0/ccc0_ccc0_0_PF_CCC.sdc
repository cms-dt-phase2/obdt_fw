set_component ccc0_ccc0_0_PF_CCC
# Microsemi Corp.
# Date: 2019-Sep-30 22:00:36
#

# Base clock for PLL #0
create_clock -period 25 [ get_pins { pll_inst_0/REF_CLK_0 } ]
create_generated_clock -multiply_by 15 -source [ get_pins { pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { pll_inst_0/OUT0 } ]
create_generated_clock -multiply_by 15 -source [ get_pins { pll_inst_0/REF_CLK_0 } ] -pll_output [ get_pins { pll_inst_0/OUT0 } ] -pll_feedback [ get_pins { pll_inst_0/FB_CLK } ] -phase 0 [ get_pins { pll_inst_0/OUT1 } ]
set_false_path -through [ get_pins { pll_inst_0/OUT0 } ] -to [ get_cells { Pll_Ext_FeedBack_Mode_Soft_Logic_Inst/* } ]
