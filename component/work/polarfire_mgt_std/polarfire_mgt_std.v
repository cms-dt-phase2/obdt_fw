//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Mon Sep 30 22:00:50 2019
// Version: v12.2 12.700.0.21
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// polarfire_mgt_std
module polarfire_mgt_std(
    // Inputs
    LANE0_CDR_REF_CLK_FAB,
    LANE0_PCS_ARST_N,
    LANE0_PMA_ARST_N,
    LANE0_RXD_N,
    LANE0_RXD_P,
    LANE0_TX_DATA,
    TX_BIT_CLK_0,
    TX_PLL_LOCK_0,
    TX_PLL_REF_CLK_0,
    // Outputs
    LANE0_RX_BYPASS_DATA,
    LANE0_RX_CLK_R,
    LANE0_RX_DATA,
    LANE0_RX_IDLE,
    LANE0_RX_READY,
    LANE0_RX_VAL,
    LANE0_TXD_N,
    LANE0_TXD_P,
    LANE0_TX_CLK_R,
    LANE0_TX_CLK_STABLE
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input         LANE0_CDR_REF_CLK_FAB;
input         LANE0_PCS_ARST_N;
input         LANE0_PMA_ARST_N;
input         LANE0_RXD_N;
input         LANE0_RXD_P;
input  [39:0] LANE0_TX_DATA;
input         TX_BIT_CLK_0;
input         TX_PLL_LOCK_0;
input         TX_PLL_REF_CLK_0;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output        LANE0_RX_BYPASS_DATA;
output        LANE0_RX_CLK_R;
output [39:0] LANE0_RX_DATA;
output        LANE0_RX_IDLE;
output        LANE0_RX_READY;
output        LANE0_RX_VAL;
output        LANE0_TXD_N;
output        LANE0_TXD_P;
output        LANE0_TX_CLK_R;
output        LANE0_TX_CLK_STABLE;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire          TX_BIT_CLK_0;
wire          TX_PLL_LOCK_0;
wire          TX_PLL_REF_CLK_0;
wire          LANE0_CDR_REF_CLK_FAB;
wire          LANE0_PCS_ARST_N;
wire          LANE0_PMA_ARST_N;
wire          LANE0_RX_BYPASS_DATA_net_0;
wire          LANE0_RX_CLK_R_net_0;
wire   [39:0] LANE0_RX_DATA_net_0;
wire          LANE0_RX_IDLE_net_0;
wire          LANE0_RX_READY_net_0;
wire          LANE0_RX_VAL_net_0;
wire          LANE0_RXD_N;
wire          LANE0_RXD_P;
wire          LANE0_TX_CLK_R_net_0;
wire          LANE0_TX_CLK_STABLE_net_0;
wire   [39:0] LANE0_TX_DATA;
wire          LANE0_TXD_N_net_0;
wire          LANE0_TXD_P_net_0;
wire          LANE0_TXD_P_net_1;
wire          LANE0_TXD_N_net_1;
wire   [39:0] LANE0_RX_DATA_net_1;
wire          LANE0_TX_CLK_R_net_1;
wire          LANE0_RX_CLK_R_net_1;
wire          LANE0_RX_IDLE_net_1;
wire          LANE0_RX_READY_net_1;
wire          LANE0_RX_VAL_net_1;
wire          LANE0_TX_CLK_STABLE_net_1;
wire          LANE0_RX_BYPASS_DATA_net_1;
//--------------------------------------------------------------------
// TiedOff Nets
//--------------------------------------------------------------------
wire          GND_net;
wire   [9:0]  LANE0_TX_DISPFNC_const_net_0;
wire   [4:0]  LANE0_8B10B_TX_K_const_net_0;
wire   [3:0]  LANE0_TX_HDR_const_net_0;
wire   [3:0]  LANE0_TXDATAK_const_net_0;
wire   [1:0]  LANE0_SATA_TX_OOB_const_net_0;
wire   [1:0]  LANE0_POWERDOWN_const_net_0;
wire   [1:0]  LANE0_SATA_RATE_const_net_0;
wire   [1:0]  LANE0_TXPATTERN_const_net_0;
wire   [1:0]  LANE0_CDR_LOCKMODE_const_net_0;
wire   [39:0] LANE1_TX_DATA_const_net_0;
wire   [9:0]  LANE1_TX_DISPFNC_const_net_0;
wire   [4:0]  LANE1_8B10B_TX_K_const_net_0;
wire   [3:0]  LANE1_TX_HDR_const_net_0;
wire   [3:0]  LANE1_TXDATAK_const_net_0;
wire   [1:0]  LANE1_SATA_TX_OOB_const_net_0;
wire   [1:0]  LANE1_POWERDOWN_const_net_0;
wire   [1:0]  LANE1_SATA_RATE_const_net_0;
wire   [1:0]  LANE1_TXPATTERN_const_net_0;
wire   [1:0]  LANE1_CDR_LOCKMODE_const_net_0;
wire   [39:0] LANE2_TX_DATA_const_net_0;
wire   [9:0]  LANE2_TX_DISPFNC_const_net_0;
wire   [4:0]  LANE2_8B10B_TX_K_const_net_0;
wire   [3:0]  LANE2_TX_HDR_const_net_0;
wire   [3:0]  LANE2_TXDATAK_const_net_0;
wire   [1:0]  LANE2_SATA_TX_OOB_const_net_0;
wire   [1:0]  LANE2_POWERDOWN_const_net_0;
wire   [1:0]  LANE2_SATA_RATE_const_net_0;
wire   [1:0]  LANE2_TXPATTERN_const_net_0;
wire   [1:0]  LANE2_CDR_LOCKMODE_const_net_0;
wire   [39:0] LANE3_TX_DATA_const_net_0;
wire   [9:0]  LANE3_TX_DISPFNC_const_net_0;
wire   [4:0]  LANE3_8B10B_TX_K_const_net_0;
wire   [3:0]  LANE3_TX_HDR_const_net_0;
wire   [3:0]  LANE3_TXDATAK_const_net_0;
wire   [1:0]  LANE3_SATA_TX_OOB_const_net_0;
wire   [1:0]  LANE3_POWERDOWN_const_net_0;
wire   [1:0]  LANE3_SATA_RATE_const_net_0;
wire   [1:0]  LANE3_TXPATTERN_const_net_0;
wire   [1:0]  LANE3_CDR_LOCKMODE_const_net_0;
wire   [1:0]  POWERDOWN_const_net_0;
wire   [2:0]  TXMARGIN_const_net_0;
wire   [32:0] LANE0_DRI_WDATA_const_net_0;
wire   [10:0] LANE0_DRI_CTRL_const_net_0;
wire   [32:0] LANE1_DRI_WDATA_const_net_0;
wire   [10:0] LANE1_DRI_CTRL_const_net_0;
wire   [32:0] LANE2_DRI_WDATA_const_net_0;
wire   [10:0] LANE2_DRI_CTRL_const_net_0;
wire   [32:0] LANE3_DRI_WDATA_const_net_0;
wire   [10:0] LANE3_DRI_CTRL_const_net_0;
wire   [2:0]  LANE0_LINK_ADDR_const_net_0;
wire   [3:0]  LANE0_LINK_WDATA_const_net_0;
wire   [2:0]  LANE1_LINK_ADDR_const_net_0;
wire   [3:0]  LANE1_LINK_WDATA_const_net_0;
wire   [2:0]  LANE2_LINK_ADDR_const_net_0;
wire   [3:0]  LANE2_LINK_WDATA_const_net_0;
wire   [2:0]  LANE3_LINK_ADDR_const_net_0;
wire   [3:0]  LANE3_LINK_WDATA_const_net_0;
//--------------------------------------------------------------------
// Constant assignments
//--------------------------------------------------------------------
assign GND_net                        = 1'b0;
assign LANE0_TX_DISPFNC_const_net_0   = 10'h000;
assign LANE0_8B10B_TX_K_const_net_0   = 5'h00;
assign LANE0_TX_HDR_const_net_0       = 4'h0;
assign LANE0_TXDATAK_const_net_0      = 4'h0;
assign LANE0_SATA_TX_OOB_const_net_0  = 2'h0;
assign LANE0_POWERDOWN_const_net_0    = 2'h0;
assign LANE0_SATA_RATE_const_net_0    = 2'h0;
assign LANE0_TXPATTERN_const_net_0    = 2'h0;
assign LANE0_CDR_LOCKMODE_const_net_0 = 2'h0;
assign LANE1_TX_DATA_const_net_0      = 40'h0000000000;
assign LANE1_TX_DISPFNC_const_net_0   = 10'h000;
assign LANE1_8B10B_TX_K_const_net_0   = 5'h00;
assign LANE1_TX_HDR_const_net_0       = 4'h0;
assign LANE1_TXDATAK_const_net_0      = 4'h0;
assign LANE1_SATA_TX_OOB_const_net_0  = 2'h0;
assign LANE1_POWERDOWN_const_net_0    = 2'h0;
assign LANE1_SATA_RATE_const_net_0    = 2'h0;
assign LANE1_TXPATTERN_const_net_0    = 2'h0;
assign LANE1_CDR_LOCKMODE_const_net_0 = 2'h0;
assign LANE2_TX_DATA_const_net_0      = 40'h0000000000;
assign LANE2_TX_DISPFNC_const_net_0   = 10'h000;
assign LANE2_8B10B_TX_K_const_net_0   = 5'h00;
assign LANE2_TX_HDR_const_net_0       = 4'h0;
assign LANE2_TXDATAK_const_net_0      = 4'h0;
assign LANE2_SATA_TX_OOB_const_net_0  = 2'h0;
assign LANE2_POWERDOWN_const_net_0    = 2'h0;
assign LANE2_SATA_RATE_const_net_0    = 2'h0;
assign LANE2_TXPATTERN_const_net_0    = 2'h0;
assign LANE2_CDR_LOCKMODE_const_net_0 = 2'h0;
assign LANE3_TX_DATA_const_net_0      = 40'h0000000000;
assign LANE3_TX_DISPFNC_const_net_0   = 10'h000;
assign LANE3_8B10B_TX_K_const_net_0   = 5'h00;
assign LANE3_TX_HDR_const_net_0       = 4'h0;
assign LANE3_TXDATAK_const_net_0      = 4'h0;
assign LANE3_SATA_TX_OOB_const_net_0  = 2'h0;
assign LANE3_POWERDOWN_const_net_0    = 2'h0;
assign LANE3_SATA_RATE_const_net_0    = 2'h0;
assign LANE3_TXPATTERN_const_net_0    = 2'h0;
assign LANE3_CDR_LOCKMODE_const_net_0 = 2'h0;
assign POWERDOWN_const_net_0          = 2'h0;
assign TXMARGIN_const_net_0           = 3'h0;
assign LANE0_DRI_WDATA_const_net_0    = 33'h000000000;
assign LANE0_DRI_CTRL_const_net_0     = 11'h000;
assign LANE1_DRI_WDATA_const_net_0    = 33'h000000000;
assign LANE1_DRI_CTRL_const_net_0     = 11'h000;
assign LANE2_DRI_WDATA_const_net_0    = 33'h000000000;
assign LANE2_DRI_CTRL_const_net_0     = 11'h000;
assign LANE3_DRI_WDATA_const_net_0    = 33'h000000000;
assign LANE3_DRI_CTRL_const_net_0     = 11'h000;
assign LANE0_LINK_ADDR_const_net_0    = 3'h0;
assign LANE0_LINK_WDATA_const_net_0   = 4'h0;
assign LANE1_LINK_ADDR_const_net_0    = 3'h0;
assign LANE1_LINK_WDATA_const_net_0   = 4'h0;
assign LANE2_LINK_ADDR_const_net_0    = 3'h0;
assign LANE2_LINK_WDATA_const_net_0   = 4'h0;
assign LANE3_LINK_ADDR_const_net_0    = 3'h0;
assign LANE3_LINK_WDATA_const_net_0   = 4'h0;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign LANE0_TXD_P_net_1          = LANE0_TXD_P_net_0;
assign LANE0_TXD_P                = LANE0_TXD_P_net_1;
assign LANE0_TXD_N_net_1          = LANE0_TXD_N_net_0;
assign LANE0_TXD_N                = LANE0_TXD_N_net_1;
assign LANE0_RX_DATA_net_1        = LANE0_RX_DATA_net_0;
assign LANE0_RX_DATA[39:0]        = LANE0_RX_DATA_net_1;
assign LANE0_TX_CLK_R_net_1       = LANE0_TX_CLK_R_net_0;
assign LANE0_TX_CLK_R             = LANE0_TX_CLK_R_net_1;
assign LANE0_RX_CLK_R_net_1       = LANE0_RX_CLK_R_net_0;
assign LANE0_RX_CLK_R             = LANE0_RX_CLK_R_net_1;
assign LANE0_RX_IDLE_net_1        = LANE0_RX_IDLE_net_0;
assign LANE0_RX_IDLE              = LANE0_RX_IDLE_net_1;
assign LANE0_RX_READY_net_1       = LANE0_RX_READY_net_0;
assign LANE0_RX_READY             = LANE0_RX_READY_net_1;
assign LANE0_RX_VAL_net_1         = LANE0_RX_VAL_net_0;
assign LANE0_RX_VAL               = LANE0_RX_VAL_net_1;
assign LANE0_TX_CLK_STABLE_net_1  = LANE0_TX_CLK_STABLE_net_0;
assign LANE0_TX_CLK_STABLE        = LANE0_TX_CLK_STABLE_net_1;
assign LANE0_RX_BYPASS_DATA_net_1 = LANE0_RX_BYPASS_DATA_net_0;
assign LANE0_RX_BYPASS_DATA       = LANE0_RX_BYPASS_DATA_net_1;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------polarfire_mgt_std_I_XCVR_PF_XCVR   -   Actel:SgCore:PF_XCVR:2.0.109
polarfire_mgt_std_I_XCVR_PF_XCVR I_XCVR(
        // Inputs
        .LANE0_TX_DATA         ( LANE0_TX_DATA ),
        .LANE0_CDR_REF_CLK_FAB ( LANE0_CDR_REF_CLK_FAB ),
        .LANE0_PCS_ARST_N      ( LANE0_PCS_ARST_N ),
        .LANE0_PMA_ARST_N      ( LANE0_PMA_ARST_N ),
        .TX_BIT_CLK_0          ( TX_BIT_CLK_0 ),
        .TX_PLL_REF_CLK_0      ( TX_PLL_REF_CLK_0 ),
        .TX_PLL_LOCK_0         ( TX_PLL_LOCK_0 ),
        .LANE0_RXD_P           ( LANE0_RXD_P ),
        .LANE0_RXD_N           ( LANE0_RXD_N ),
        // Outputs
        .LANE0_RX_DATA         ( LANE0_RX_DATA_net_0 ),
        .LANE0_TX_CLK_R        ( LANE0_TX_CLK_R_net_0 ),
        .LANE0_RX_CLK_R        ( LANE0_RX_CLK_R_net_0 ),
        .LANE0_RX_IDLE         ( LANE0_RX_IDLE_net_0 ),
        .LANE0_RX_READY        ( LANE0_RX_READY_net_0 ),
        .LANE0_RX_VAL          ( LANE0_RX_VAL_net_0 ),
        .LANE0_TX_CLK_STABLE   ( LANE0_TX_CLK_STABLE_net_0 ),
        .LANE0_RX_BYPASS_DATA  ( LANE0_RX_BYPASS_DATA_net_0 ),
        .LANE0_TXD_P           ( LANE0_TXD_P_net_0 ),
        .LANE0_TXD_N           ( LANE0_TXD_N_net_0 ) 
        );


endmodule
