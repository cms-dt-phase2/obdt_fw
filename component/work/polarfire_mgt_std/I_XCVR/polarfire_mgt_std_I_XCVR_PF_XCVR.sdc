set_component polarfire_mgt_std_I_XCVR_PF_XCVR
# Microsemi Corp.
# Date: 2019-Sep-30 22:00:50
#

create_clock -period 8.33333 [ get_pins { LANE0/RX_REF_CLK } ]
create_clock -period 8.33333 [ get_pins { LANE0/TX_CLK_R } ]
create_clock -period 8.33333 [ get_pins { LANE0/RX_CLK_R } ]
