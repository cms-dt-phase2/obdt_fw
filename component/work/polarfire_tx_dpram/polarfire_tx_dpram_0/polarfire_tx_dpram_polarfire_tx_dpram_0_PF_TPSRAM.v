`timescale 1 ns/100 ps
// Version: v12.2 12.700.0.21


module polarfire_tx_dpram_polarfire_tx_dpram_0_PF_TPSRAM(
       W_DATA,
       R_DATA,
       W_ADDR,
       R_ADDR,
       W_EN,
       W_CLK,
       R_CLK
    );
input  [159:0] W_DATA;
output [39:0] R_DATA;
input  [2:0] W_ADDR;
input  [4:0] R_ADDR;
input  W_EN;
input  W_CLK;
input  R_CLK;

    wire \ACCESS_BUSY[0][0] , \ACCESS_BUSY[0][1] , \ACCESS_BUSY[0][2] , 
        \ACCESS_BUSY[0][3] , VCC, GND, ADLIB_VCC;
    wire GND_power_net1;
    wire VCC_power_net1;
    assign GND = GND_power_net1;
    assign VCC = VCC_power_net1;
    assign ADLIB_VCC = VCC_power_net1;
    
    RAM1K20 #( .RAMINDEX("polarfire_tx_dpram_0%32-8%40-160%SPEED%0%1%TWO-PORT%ECC_EN-0")
         )  polarfire_tx_dpram_polarfire_tx_dpram_0_PF_TPSRAM_R0C1 (
        .A_DOUT({nc0, nc1, nc2, nc3, nc4, nc5, nc6, nc7, nc8, nc9, 
        R_DATA[19], R_DATA[18], R_DATA[17], R_DATA[16], R_DATA[15], 
        R_DATA[14], R_DATA[13], R_DATA[12], R_DATA[11], R_DATA[10]}), 
        .B_DOUT({nc10, nc11, nc12, nc13, nc14, nc15, nc16, nc17, nc18, 
        nc19, nc20, nc21, nc22, nc23, nc24, nc25, nc26, nc27, nc28, 
        nc29}), .DB_DETECT(), .SB_CORRECT(), .ACCESS_BUSY(
        \ACCESS_BUSY[0][1] ), .A_ADDR({GND, GND, GND, GND, GND, GND, 
        R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], R_ADDR[0], GND, 
        GND, GND}), .A_BLK_EN({VCC, VCC, VCC}), .A_CLK(R_CLK), .A_DIN({
        W_DATA[139], W_DATA[138], W_DATA[137], W_DATA[136], 
        W_DATA[135], W_DATA[134], W_DATA[133], W_DATA[132], 
        W_DATA[131], W_DATA[130], W_DATA[99], W_DATA[98], W_DATA[97], 
        W_DATA[96], W_DATA[95], W_DATA[94], W_DATA[93], W_DATA[92], 
        W_DATA[91], W_DATA[90]}), .A_REN(VCC), .A_WEN({VCC, VCC}), 
        .A_DOUT_EN(VCC), .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), 
        .B_ADDR({GND, GND, GND, GND, GND, GND, W_ADDR[2], W_ADDR[1], 
        W_ADDR[0], GND, GND, GND, GND, GND}), .B_BLK_EN({W_EN, VCC, 
        VCC}), .B_CLK(W_CLK), .B_DIN({W_DATA[59], W_DATA[58], 
        W_DATA[57], W_DATA[56], W_DATA[55], W_DATA[54], W_DATA[53], 
        W_DATA[52], W_DATA[51], W_DATA[50], W_DATA[19], W_DATA[18], 
        W_DATA[17], W_DATA[16], W_DATA[15], W_DATA[14], W_DATA[13], 
        W_DATA[12], W_DATA[11], W_DATA[10]}), .B_REN(VCC), .B_WEN({VCC, 
        VCC}), .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(
        VCC), .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, VCC}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({VCC, GND, VCC})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("polarfire_tx_dpram_0%32-8%40-160%SPEED%0%3%TWO-PORT%ECC_EN-0")
         )  polarfire_tx_dpram_polarfire_tx_dpram_0_PF_TPSRAM_R0C3 (
        .A_DOUT({nc30, nc31, nc32, nc33, nc34, nc35, nc36, nc37, nc38, 
        nc39, R_DATA[39], R_DATA[38], R_DATA[37], R_DATA[36], 
        R_DATA[35], R_DATA[34], R_DATA[33], R_DATA[32], R_DATA[31], 
        R_DATA[30]}), .B_DOUT({nc40, nc41, nc42, nc43, nc44, nc45, 
        nc46, nc47, nc48, nc49, nc50, nc51, nc52, nc53, nc54, nc55, 
        nc56, nc57, nc58, nc59}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][3] ), .A_ADDR({GND, GND, GND, GND, 
        GND, GND, R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND, GND}), .A_BLK_EN({VCC, VCC, VCC}), .A_CLK(
        R_CLK), .A_DIN({W_DATA[159], W_DATA[158], W_DATA[157], 
        W_DATA[156], W_DATA[155], W_DATA[154], W_DATA[153], 
        W_DATA[152], W_DATA[151], W_DATA[150], W_DATA[119], 
        W_DATA[118], W_DATA[117], W_DATA[116], W_DATA[115], 
        W_DATA[114], W_DATA[113], W_DATA[112], W_DATA[111], 
        W_DATA[110]}), .A_REN(VCC), .A_WEN({VCC, VCC}), .A_DOUT_EN(VCC)
        , .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({GND, GND, 
        GND, GND, GND, GND, W_ADDR[2], W_ADDR[1], W_ADDR[0], GND, GND, 
        GND, GND, GND}), .B_BLK_EN({W_EN, VCC, VCC}), .B_CLK(W_CLK), 
        .B_DIN({W_DATA[79], W_DATA[78], W_DATA[77], W_DATA[76], 
        W_DATA[75], W_DATA[74], W_DATA[73], W_DATA[72], W_DATA[71], 
        W_DATA[70], W_DATA[39], W_DATA[38], W_DATA[37], W_DATA[36], 
        W_DATA[35], W_DATA[34], W_DATA[33], W_DATA[32], W_DATA[31], 
        W_DATA[30]}), .B_REN(VCC), .B_WEN({VCC, VCC}), .B_DOUT_EN(VCC), 
        .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), .ECC_EN(GND), 
        .BUSY_FB(GND), .A_WIDTH({GND, VCC, VCC}), .A_WMODE({GND, GND}), 
        .A_BYPASS(VCC), .B_WIDTH({VCC, GND, VCC}), .B_WMODE({GND, GND})
        , .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("polarfire_tx_dpram_0%32-8%40-160%SPEED%0%0%TWO-PORT%ECC_EN-0")
         )  polarfire_tx_dpram_polarfire_tx_dpram_0_PF_TPSRAM_R0C0 (
        .A_DOUT({nc60, nc61, nc62, nc63, nc64, nc65, nc66, nc67, nc68, 
        nc69, R_DATA[9], R_DATA[8], R_DATA[7], R_DATA[6], R_DATA[5], 
        R_DATA[4], R_DATA[3], R_DATA[2], R_DATA[1], R_DATA[0]}), 
        .B_DOUT({nc70, nc71, nc72, nc73, nc74, nc75, nc76, nc77, nc78, 
        nc79, nc80, nc81, nc82, nc83, nc84, nc85, nc86, nc87, nc88, 
        nc89}), .DB_DETECT(), .SB_CORRECT(), .ACCESS_BUSY(
        \ACCESS_BUSY[0][0] ), .A_ADDR({GND, GND, GND, GND, GND, GND, 
        R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], R_ADDR[0], GND, 
        GND, GND}), .A_BLK_EN({VCC, VCC, VCC}), .A_CLK(R_CLK), .A_DIN({
        W_DATA[129], W_DATA[128], W_DATA[127], W_DATA[126], 
        W_DATA[125], W_DATA[124], W_DATA[123], W_DATA[122], 
        W_DATA[121], W_DATA[120], W_DATA[89], W_DATA[88], W_DATA[87], 
        W_DATA[86], W_DATA[85], W_DATA[84], W_DATA[83], W_DATA[82], 
        W_DATA[81], W_DATA[80]}), .A_REN(VCC), .A_WEN({VCC, VCC}), 
        .A_DOUT_EN(VCC), .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), 
        .B_ADDR({GND, GND, GND, GND, GND, GND, W_ADDR[2], W_ADDR[1], 
        W_ADDR[0], GND, GND, GND, GND, GND}), .B_BLK_EN({W_EN, VCC, 
        VCC}), .B_CLK(W_CLK), .B_DIN({W_DATA[49], W_DATA[48], 
        W_DATA[47], W_DATA[46], W_DATA[45], W_DATA[44], W_DATA[43], 
        W_DATA[42], W_DATA[41], W_DATA[40], W_DATA[9], W_DATA[8], 
        W_DATA[7], W_DATA[6], W_DATA[5], W_DATA[4], W_DATA[3], 
        W_DATA[2], W_DATA[1], W_DATA[0]}), .B_REN(VCC), .B_WEN({VCC, 
        VCC}), .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(
        VCC), .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, VCC}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({VCC, GND, VCC})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("polarfire_tx_dpram_0%32-8%40-160%SPEED%0%2%TWO-PORT%ECC_EN-0")
         )  polarfire_tx_dpram_polarfire_tx_dpram_0_PF_TPSRAM_R0C2 (
        .A_DOUT({nc90, nc91, nc92, nc93, nc94, nc95, nc96, nc97, nc98, 
        nc99, R_DATA[29], R_DATA[28], R_DATA[27], R_DATA[26], 
        R_DATA[25], R_DATA[24], R_DATA[23], R_DATA[22], R_DATA[21], 
        R_DATA[20]}), .B_DOUT({nc100, nc101, nc102, nc103, nc104, 
        nc105, nc106, nc107, nc108, nc109, nc110, nc111, nc112, nc113, 
        nc114, nc115, nc116, nc117, nc118, nc119}), .DB_DETECT(), 
        .SB_CORRECT(), .ACCESS_BUSY(\ACCESS_BUSY[0][2] ), .A_ADDR({GND, 
        GND, GND, GND, GND, GND, R_ADDR[4], R_ADDR[3], R_ADDR[2], 
        R_ADDR[1], R_ADDR[0], GND, GND, GND}), .A_BLK_EN({VCC, VCC, 
        VCC}), .A_CLK(R_CLK), .A_DIN({W_DATA[149], W_DATA[148], 
        W_DATA[147], W_DATA[146], W_DATA[145], W_DATA[144], 
        W_DATA[143], W_DATA[142], W_DATA[141], W_DATA[140], 
        W_DATA[109], W_DATA[108], W_DATA[107], W_DATA[106], 
        W_DATA[105], W_DATA[104], W_DATA[103], W_DATA[102], 
        W_DATA[101], W_DATA[100]}), .A_REN(VCC), .A_WEN({VCC, VCC}), 
        .A_DOUT_EN(VCC), .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), 
        .B_ADDR({GND, GND, GND, GND, GND, GND, W_ADDR[2], W_ADDR[1], 
        W_ADDR[0], GND, GND, GND, GND, GND}), .B_BLK_EN({W_EN, VCC, 
        VCC}), .B_CLK(W_CLK), .B_DIN({W_DATA[69], W_DATA[68], 
        W_DATA[67], W_DATA[66], W_DATA[65], W_DATA[64], W_DATA[63], 
        W_DATA[62], W_DATA[61], W_DATA[60], W_DATA[29], W_DATA[28], 
        W_DATA[27], W_DATA[26], W_DATA[25], W_DATA[24], W_DATA[23], 
        W_DATA[22], W_DATA[21], W_DATA[20]}), .B_REN(VCC), .B_WEN({VCC, 
        VCC}), .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(
        VCC), .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, VCC}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({VCC, GND, VCC})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    GND GND_power_inst1 (.Y(GND_power_net1));
    VCC VCC_power_inst1 (.Y(VCC_power_net1));
    
endmodule
