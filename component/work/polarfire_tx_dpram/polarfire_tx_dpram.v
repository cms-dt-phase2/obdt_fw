//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Mon Sep 30 22:01:04 2019
// Version: v12.2 12.700.0.21
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// polarfire_tx_dpram
module polarfire_tx_dpram(
    // Inputs
    R_ADDR,
    R_CLK,
    W_ADDR,
    W_CLK,
    W_DATA,
    W_EN,
    // Outputs
    R_DATA
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input  [4:0]   R_ADDR;
input          R_CLK;
input  [2:0]   W_ADDR;
input          W_CLK;
input  [159:0] W_DATA;
input          W_EN;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output [39:0]  R_DATA;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire   [4:0]   R_ADDR;
wire           R_CLK;
wire   [39:0]  R_DATA_0;
wire   [2:0]   W_ADDR;
wire           W_CLK;
wire   [159:0] W_DATA;
wire           W_EN;
wire   [39:0]  R_DATA_0_net_0;
//--------------------------------------------------------------------
// TiedOff Nets
//--------------------------------------------------------------------
wire           GND_net;
//--------------------------------------------------------------------
// Constant assignments
//--------------------------------------------------------------------
assign GND_net    = 1'b0;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign R_DATA_0_net_0 = R_DATA_0;
assign R_DATA[39:0]   = R_DATA_0_net_0;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------polarfire_tx_dpram_polarfire_tx_dpram_0_PF_TPSRAM   -   Actel:SgCore:PF_TPSRAM:1.1.108
polarfire_tx_dpram_polarfire_tx_dpram_0_PF_TPSRAM polarfire_tx_dpram_0(
        // Inputs
        .W_EN   ( W_EN ),
        .W_CLK  ( W_CLK ),
        .R_CLK  ( R_CLK ),
        .W_DATA ( W_DATA ),
        .W_ADDR ( W_ADDR ),
        .R_ADDR ( R_ADDR ),
        // Outputs
        .R_DATA ( R_DATA_0 ) 
        );


endmodule
