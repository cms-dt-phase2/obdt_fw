Microsemi Corporation - Microsemi Libero Software Release v12.2 (Version 12.700.0.21)

Date      :  Mon Sep 30 22:01:13 2019
Project   :  C:\phase2\fws_obdt\obdt_fw
Component :  ro_fifo_out
Family    :  PolarFire


HDL source files for all Synthesis and Simulation tools:
    C:/phase2/fws_obdt/obdt_fw/component/Actel/DirectCore/COREFIFO/2.7.105/rtl/vhdl/core/fifo_pkg.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/COREFIFO.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/corefifo_NstagesSync.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/corefifo_async.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/corefifo_fwft.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/corefifo_grayToBinConv.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/corefifo_resetSync.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/corefifo_sync.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/corefifo_sync_scntr.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/ro_fifo_out_ro_fifo_out_0_LSRAM_top.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/core/ro_fifo_out_ro_fifo_out_0_ram_wrapper.vhd

Stimulus files for all Simulation tools:
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/mti/scripts/runall_vhdl.do
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/mti/scripts/wave_vhdl.do

    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/coreparameters.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/test/user/TB.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/test/user/XHDL_misc.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/test/user/XHDL_std_logic.vhd
    C:/phase2/fws_obdt/obdt_fw/component/work/ro_fifo_out/ro_fifo_out_0/rtl/vhdl/test/user/g4_dp_ext_mem.vhd

