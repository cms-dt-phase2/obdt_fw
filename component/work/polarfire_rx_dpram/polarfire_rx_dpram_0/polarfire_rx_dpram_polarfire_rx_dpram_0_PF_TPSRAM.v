`timescale 1 ns/100 ps
// Version: v12.2 12.700.0.21


module polarfire_rx_dpram_polarfire_rx_dpram_0_PF_TPSRAM(
       W_DATA,
       R_DATA,
       W_ADDR,
       R_ADDR,
       W_EN,
       W_CLK,
       R_CLK
    );
input  [39:0] W_DATA;
output [159:0] R_DATA;
input  [4:0] W_ADDR;
input  [2:0] R_ADDR;
input  W_EN;
input  W_CLK;
input  R_CLK;

    wire \ACCESS_BUSY[0][0] , \ACCESS_BUSY[0][1] , \ACCESS_BUSY[0][2] , 
        \ACCESS_BUSY[0][3] , VCC, GND, ADLIB_VCC;
    wire GND_power_net1;
    wire VCC_power_net1;
    assign GND = GND_power_net1;
    assign VCC = VCC_power_net1;
    assign ADLIB_VCC = VCC_power_net1;
    
    RAM1K20 #( .RAMINDEX("polarfire_rx_dpram_0%8-32%160-40%SPEED%0%3%TWO-PORT%ECC_EN-0")
         )  polarfire_rx_dpram_polarfire_rx_dpram_0_PF_TPSRAM_R0C3 (
        .A_DOUT({R_DATA[159], R_DATA[158], R_DATA[157], R_DATA[156], 
        R_DATA[155], R_DATA[154], R_DATA[153], R_DATA[152], 
        R_DATA[151], R_DATA[150], R_DATA[119], R_DATA[118], 
        R_DATA[117], R_DATA[116], R_DATA[115], R_DATA[114], 
        R_DATA[113], R_DATA[112], R_DATA[111], R_DATA[110]}), .B_DOUT({
        R_DATA[79], R_DATA[78], R_DATA[77], R_DATA[76], R_DATA[75], 
        R_DATA[74], R_DATA[73], R_DATA[72], R_DATA[71], R_DATA[70], 
        R_DATA[39], R_DATA[38], R_DATA[37], R_DATA[36], R_DATA[35], 
        R_DATA[34], R_DATA[33], R_DATA[32], R_DATA[31], R_DATA[30]}), 
        .DB_DETECT(), .SB_CORRECT(), .ACCESS_BUSY(\ACCESS_BUSY[0][3] ), 
        .A_ADDR({GND, GND, GND, GND, GND, GND, R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND, GND, GND, GND}), .A_BLK_EN({VCC, VCC, VCC})
        , .A_CLK(R_CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND}), .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({GND, GND, 
        GND, GND, GND, GND, W_ADDR[4], W_ADDR[3], W_ADDR[2], W_ADDR[1], 
        W_ADDR[0], GND, GND, GND}), .B_BLK_EN({W_EN, VCC, VCC}), 
        .B_CLK(W_CLK), .B_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[39], W_DATA[38], W_DATA[37], W_DATA[36], 
        W_DATA[35], W_DATA[34], W_DATA[33], W_DATA[32], W_DATA[31], 
        W_DATA[30]}), .B_REN(VCC), .B_WEN({GND, VCC}), .B_DOUT_EN(VCC), 
        .B_DOUT_ARST_N(VCC), .B_DOUT_SRST_N(VCC), .ECC_EN(GND), 
        .BUSY_FB(GND), .A_WIDTH({VCC, GND, VCC}), .A_WMODE({GND, GND}), 
        .A_BYPASS(VCC), .B_WIDTH({GND, VCC, VCC}), .B_WMODE({GND, GND})
        , .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("polarfire_rx_dpram_0%8-32%160-40%SPEED%0%2%TWO-PORT%ECC_EN-0")
         )  polarfire_rx_dpram_polarfire_rx_dpram_0_PF_TPSRAM_R0C2 (
        .A_DOUT({R_DATA[149], R_DATA[148], R_DATA[147], R_DATA[146], 
        R_DATA[145], R_DATA[144], R_DATA[143], R_DATA[142], 
        R_DATA[141], R_DATA[140], R_DATA[109], R_DATA[108], 
        R_DATA[107], R_DATA[106], R_DATA[105], R_DATA[104], 
        R_DATA[103], R_DATA[102], R_DATA[101], R_DATA[100]}), .B_DOUT({
        R_DATA[69], R_DATA[68], R_DATA[67], R_DATA[66], R_DATA[65], 
        R_DATA[64], R_DATA[63], R_DATA[62], R_DATA[61], R_DATA[60], 
        R_DATA[29], R_DATA[28], R_DATA[27], R_DATA[26], R_DATA[25], 
        R_DATA[24], R_DATA[23], R_DATA[22], R_DATA[21], R_DATA[20]}), 
        .DB_DETECT(), .SB_CORRECT(), .ACCESS_BUSY(\ACCESS_BUSY[0][2] ), 
        .A_ADDR({GND, GND, GND, GND, GND, GND, R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND, GND, GND, GND}), .A_BLK_EN({VCC, VCC, VCC})
        , .A_CLK(R_CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND}), .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({GND, GND, 
        GND, GND, GND, GND, W_ADDR[4], W_ADDR[3], W_ADDR[2], W_ADDR[1], 
        W_ADDR[0], GND, GND, GND}), .B_BLK_EN({W_EN, VCC, VCC}), 
        .B_CLK(W_CLK), .B_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[29], W_DATA[28], W_DATA[27], W_DATA[26], 
        W_DATA[25], W_DATA[24], W_DATA[23], W_DATA[22], W_DATA[21], 
        W_DATA[20]}), .B_REN(VCC), .B_WEN({GND, VCC}), .B_DOUT_EN(VCC), 
        .B_DOUT_ARST_N(VCC), .B_DOUT_SRST_N(VCC), .ECC_EN(GND), 
        .BUSY_FB(GND), .A_WIDTH({VCC, GND, VCC}), .A_WMODE({GND, GND}), 
        .A_BYPASS(VCC), .B_WIDTH({GND, VCC, VCC}), .B_WMODE({GND, GND})
        , .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("polarfire_rx_dpram_0%8-32%160-40%SPEED%0%0%TWO-PORT%ECC_EN-0")
         )  polarfire_rx_dpram_polarfire_rx_dpram_0_PF_TPSRAM_R0C0 (
        .A_DOUT({R_DATA[129], R_DATA[128], R_DATA[127], R_DATA[126], 
        R_DATA[125], R_DATA[124], R_DATA[123], R_DATA[122], 
        R_DATA[121], R_DATA[120], R_DATA[89], R_DATA[88], R_DATA[87], 
        R_DATA[86], R_DATA[85], R_DATA[84], R_DATA[83], R_DATA[82], 
        R_DATA[81], R_DATA[80]}), .B_DOUT({R_DATA[49], R_DATA[48], 
        R_DATA[47], R_DATA[46], R_DATA[45], R_DATA[44], R_DATA[43], 
        R_DATA[42], R_DATA[41], R_DATA[40], R_DATA[9], R_DATA[8], 
        R_DATA[7], R_DATA[6], R_DATA[5], R_DATA[4], R_DATA[3], 
        R_DATA[2], R_DATA[1], R_DATA[0]}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][0] ), .A_ADDR({GND, GND, GND, GND, 
        GND, GND, R_ADDR[2], R_ADDR[1], R_ADDR[0], GND, GND, GND, GND, 
        GND}), .A_BLK_EN({VCC, VCC, VCC}), .A_CLK(R_CLK), .A_DIN({GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND}), .A_REN(VCC), .A_WEN({GND, 
        GND}), .A_DOUT_EN(VCC), .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(
        VCC), .B_ADDR({GND, GND, GND, GND, GND, GND, W_ADDR[4], 
        W_ADDR[3], W_ADDR[2], W_ADDR[1], W_ADDR[0], GND, GND, GND}), 
        .B_BLK_EN({W_EN, VCC, VCC}), .B_CLK(W_CLK), .B_DIN({GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, W_DATA[9], W_DATA[8], 
        W_DATA[7], W_DATA[6], W_DATA[5], W_DATA[4], W_DATA[3], 
        W_DATA[2], W_DATA[1], W_DATA[0]}), .B_REN(VCC), .B_WEN({GND, 
        VCC}), .B_DOUT_EN(VCC), .B_DOUT_ARST_N(VCC), .B_DOUT_SRST_N(
        VCC), .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({VCC, GND, VCC}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, VCC, VCC})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("polarfire_rx_dpram_0%8-32%160-40%SPEED%0%1%TWO-PORT%ECC_EN-0")
         )  polarfire_rx_dpram_polarfire_rx_dpram_0_PF_TPSRAM_R0C1 (
        .A_DOUT({R_DATA[139], R_DATA[138], R_DATA[137], R_DATA[136], 
        R_DATA[135], R_DATA[134], R_DATA[133], R_DATA[132], 
        R_DATA[131], R_DATA[130], R_DATA[99], R_DATA[98], R_DATA[97], 
        R_DATA[96], R_DATA[95], R_DATA[94], R_DATA[93], R_DATA[92], 
        R_DATA[91], R_DATA[90]}), .B_DOUT({R_DATA[59], R_DATA[58], 
        R_DATA[57], R_DATA[56], R_DATA[55], R_DATA[54], R_DATA[53], 
        R_DATA[52], R_DATA[51], R_DATA[50], R_DATA[19], R_DATA[18], 
        R_DATA[17], R_DATA[16], R_DATA[15], R_DATA[14], R_DATA[13], 
        R_DATA[12], R_DATA[11], R_DATA[10]}), .DB_DETECT(), 
        .SB_CORRECT(), .ACCESS_BUSY(\ACCESS_BUSY[0][1] ), .A_ADDR({GND, 
        GND, GND, GND, GND, GND, R_ADDR[2], R_ADDR[1], R_ADDR[0], GND, 
        GND, GND, GND, GND}), .A_BLK_EN({VCC, VCC, VCC}), .A_CLK(R_CLK)
        , .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), .A_REN(VCC)
        , .A_WEN({GND, GND}), .A_DOUT_EN(VCC), .A_DOUT_ARST_N(VCC), 
        .A_DOUT_SRST_N(VCC), .B_ADDR({GND, GND, GND, GND, GND, GND, 
        W_ADDR[4], W_ADDR[3], W_ADDR[2], W_ADDR[1], W_ADDR[0], GND, 
        GND, GND}), .B_BLK_EN({W_EN, VCC, VCC}), .B_CLK(W_CLK), .B_DIN({
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, W_DATA[19], 
        W_DATA[18], W_DATA[17], W_DATA[16], W_DATA[15], W_DATA[14], 
        W_DATA[13], W_DATA[12], W_DATA[11], W_DATA[10]}), .B_REN(VCC), 
        .B_WEN({GND, VCC}), .B_DOUT_EN(VCC), .B_DOUT_ARST_N(VCC), 
        .B_DOUT_SRST_N(VCC), .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({
        VCC, GND, VCC}), .A_WMODE({GND, GND}), .A_BYPASS(VCC), 
        .B_WIDTH({GND, VCC, VCC}), .B_WMODE({GND, GND}), .B_BYPASS(VCC)
        , .ECC_BYPASS(GND));
    GND GND_power_inst1 (.Y(GND_power_net1));
    VCC VCC_power_inst1 (.Y(VCC_power_net1));
    
endmodule
