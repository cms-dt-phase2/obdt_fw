//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Mon Sep 30 22:01:16 2019
// Version: v12.2 12.700.0.21
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// user_clock
module user_clock(
    // Outputs
    RCOSC_160MHZ_GL,
    RCOSC_2MHZ_GL
);

//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output RCOSC_160MHZ_GL;
output RCOSC_2MHZ_GL;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire   RCOSC_2MHZ_GL_net_0;
wire   RCOSC_160MHZ_GL_net_0;
wire   RCOSC_160MHZ_GL_net_1;
wire   RCOSC_2MHZ_GL_net_1;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign RCOSC_160MHZ_GL_net_1 = RCOSC_160MHZ_GL_net_0;
assign RCOSC_160MHZ_GL       = RCOSC_160MHZ_GL_net_1;
assign RCOSC_2MHZ_GL_net_1   = RCOSC_2MHZ_GL_net_0;
assign RCOSC_2MHZ_GL         = RCOSC_2MHZ_GL_net_1;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------user_clock_user_clock_0_PF_OSC   -   Actel:SgCore:PF_OSC:1.0.102
user_clock_user_clock_0_PF_OSC user_clock_0(
        // Outputs
        .RCOSC_160MHZ_GL ( RCOSC_160MHZ_GL_net_0 ),
        .RCOSC_2MHZ_GL   ( RCOSC_2MHZ_GL_net_0 ) 
        );


endmodule
