`timescale 1 ns/100 ps
// Version: v12.2 12.700.0.21


module user_clock_user_clock_0_PF_OSC(
       RCOSC_160MHZ_GL,
       RCOSC_2MHZ_GL
    );
output RCOSC_160MHZ_GL;
output RCOSC_2MHZ_GL;

    wire GND_net, VCC_net, I_OSC_160_CLK_c, I_OSC_2_CLK_c;
    
    CLKINT I_OSC_2_INT (.A(I_OSC_2_CLK_c), .Y(RCOSC_2MHZ_GL));
    VCC vcc_inst (.Y(VCC_net));
    OSC_RC2MHZ I_OSC_2 (.OSC_2MHZ_ON(VCC_net), .CLK(I_OSC_2_CLK_c));
    OSC_RC160MHZ I_OSC_160 (.OSC_160MHZ_ON(VCC_net), .CLK(
        I_OSC_160_CLK_c));
    GND gnd_inst (.Y(GND_net));
    CLKINT I_OSC_160_INT (.A(I_OSC_160_CLK_c), .Y(RCOSC_160MHZ_GL));
    
endmodule
