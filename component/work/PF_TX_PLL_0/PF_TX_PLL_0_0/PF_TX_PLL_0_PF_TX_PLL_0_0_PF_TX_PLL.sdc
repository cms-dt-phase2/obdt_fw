set_component PF_TX_PLL_0_PF_TX_PLL_0_0_PF_TX_PLL
# Microsemi Corp.
# Date: 2019-Sep-30 22:00:45
#

create_clock -period 8.33333 [ get_pins { txpll_isnt_0/FAB_REF_CLK } ]
create_clock -period 8 [ get_pins { txpll_isnt_0/DIV_CLK } ]
