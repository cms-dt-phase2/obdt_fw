//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Mon Sep 30 22:00:45 2019
// Version: v12.2 12.700.0.21
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// PF_TX_PLL_0
module PF_TX_PLL_0(
    // Inputs
    FAB_REF_CLK,
    // Outputs
    BIT_CLK,
    LOCK,
    PLL_LOCK,
    REF_CLK_TO_LANE
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input  FAB_REF_CLK;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output BIT_CLK;
output LOCK;
output PLL_LOCK;
output REF_CLK_TO_LANE;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire   CLKS_TO_XCVR_0_BIT_CLK;
wire   CLKS_TO_XCVR_0_LOCK;
wire   CLKS_TO_XCVR_0_REF_CLK_TO_LANE;
wire   FAB_REF_CLK;
wire   PLL_LOCK_net_0;
wire   PLL_LOCK_net_1;
wire   CLKS_TO_XCVR_0_LOCK_net_0;
wire   CLKS_TO_XCVR_0_BIT_CLK_net_0;
wire   CLKS_TO_XCVR_0_REF_CLK_TO_LANE_net_0;
//--------------------------------------------------------------------
// TiedOff Nets
//--------------------------------------------------------------------
wire   GND_net;
wire   [10:0]DRI_CTRL_const_net_0;
wire   [32:0]DRI_WDATA_const_net_0;
//--------------------------------------------------------------------
// Constant assignments
//--------------------------------------------------------------------
assign GND_net               = 1'b0;
assign DRI_CTRL_const_net_0  = 11'h000;
assign DRI_WDATA_const_net_0 = 33'h000000000;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign PLL_LOCK_net_1                       = PLL_LOCK_net_0;
assign PLL_LOCK                             = PLL_LOCK_net_1;
assign CLKS_TO_XCVR_0_LOCK_net_0            = CLKS_TO_XCVR_0_LOCK;
assign LOCK                                 = CLKS_TO_XCVR_0_LOCK_net_0;
assign CLKS_TO_XCVR_0_BIT_CLK_net_0         = CLKS_TO_XCVR_0_BIT_CLK;
assign BIT_CLK                              = CLKS_TO_XCVR_0_BIT_CLK_net_0;
assign CLKS_TO_XCVR_0_REF_CLK_TO_LANE_net_0 = CLKS_TO_XCVR_0_REF_CLK_TO_LANE;
assign REF_CLK_TO_LANE                      = CLKS_TO_XCVR_0_REF_CLK_TO_LANE_net_0;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------PF_TX_PLL_0_PF_TX_PLL_0_0_PF_TX_PLL   -   Actel:SgCore:PF_TX_PLL:2.0.006
PF_TX_PLL_0_PF_TX_PLL_0_0_PF_TX_PLL PF_TX_PLL_0_0(
        // Inputs
        .FAB_REF_CLK     ( FAB_REF_CLK ),
        // Outputs
        .LOCK            ( CLKS_TO_XCVR_0_LOCK ),
        .BIT_CLK         ( CLKS_TO_XCVR_0_BIT_CLK ),
        .REF_CLK_TO_LANE ( CLKS_TO_XCVR_0_REF_CLK_TO_LANE ),
        .PLL_LOCK        ( PLL_LOCK_net_0 ) 
        );


endmodule
